using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelProcessor : AssetPostprocessor
{
	static readonly string filePath = "Assets/Resources/Xls/horror.xls";
	static readonly string exportPath = "Assets/Resources/Xls/Data/";
	//private static readonly string exportPath = "Assets/Resources/Xml/Data/ExcelData.asset";



	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			Debug.Log (asset);

			if (!filePath.Equals (asset))
				continue;
			
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook (stream);

				for( int sheetIndex = 0;sheetIndex < book.NumberOfSheets;++sheetIndex ){
					ISheet sheet = book.GetSheetAt (sheetIndex);

					//Debug.Log ( "SheetName:"+sheet.SheetName+","+lastRowNum );

					if(sheet.SheetName.Equals("EVENT")){
						SetEVENTsheetData(sheet);
					} else if(sheet.SheetName.Equals("FLG")){
						SetFLGsheetData(sheet);
					} else if(sheet.SheetName.Equals("SCENE")){
						SetSCENEsheetData(sheet);
					} else if(sheet.SheetName.Equals("EVENTNAME")){
						SetEVENTNAMEsheetData(sheet);
					} else if(sheet.SheetName.Equals("DBG")){
						SetDBGsheetData(sheet);
					} else if(sheet.SheetName.Equals("ITEM")){
						SetITEMsheetData(sheet);
					} else if(sheet.SheetName.Equals("ARCHIVE")){
						SetARCHIVEsheetData(sheet);
					} else if(sheet.SheetName.Equals("PLACE")){
						SetPLACEsheetData(sheet);
					} else if(sheet.SheetName.Equals("HINT")){
						SetHINTsheetData(sheet);
					} else if(sheet.SheetName.Equals("ACHIEVEMENT")){
						SetACHIEVEMENTsheetData(sheet);
					}
				}
				/*
				ISheet sheet = book.GetSheetAt (1);
				Debug.Log (sheet.SheetName);
				
				for (int i=1; i< sheet.LastRowNum; i++) {
					
					IRow row = sheet.GetRow (i);

					ItemData.Param p = new ItemData.Param ();
					p.itemName = row.GetCell (0).StringCellValue;
					Debug.Log (p.itemName);
					p.itemContext = row.GetCell (1).StringCellValue;
					
					data.list.Add (p);
				}
				*/
			}
			
			// 4


		}
	}

	static void SetEVENTsheetData (ISheet sheet)
	{
		EventData data = (EventData)AssetDatabase.LoadAssetAtPath (exportPath + "EventData.asset", typeof(EventData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<EventData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "EventData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			EventData.Param p = new EventData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.event_key = cell.ToString();
					break;
				case 1:
					try {
						p.no = Int32.Parse(cell.ToString());
					} catch {
						p.no = 0;
					}
					break;
				case 2:
					p.event_content = cell.ToString();
					break;
				case 3:
					p.content = cell.ToString();
					break;
				case 4:
					p.other = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "EventData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetFLGsheetData (ISheet sheet)
	{
		FlgData data = (FlgData)AssetDatabase.LoadAssetAtPath (exportPath + "FlgData.asset", typeof(FlgData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<FlgData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "FlgData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			FlgData.Param p = new FlgData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				//Debug.Log( "Cell"+cellIndex+":"+cell.CellType+","+cell );

				switch(cellIndex)
				{
				case 0:
					p.flg_key = cell.ToString();
					break;
				case 1:
					try {
						p.flg_no = Int32.Parse(cell.ToString());
					} catch {
						p.flg_no = 0;
					}
					break;
				case 2:
					p.flg_content = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "FlgData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetSCENEsheetData (ISheet sheet)
	{
		SceneData data = (SceneData)AssetDatabase.LoadAssetAtPath (exportPath + "SceneData.asset", typeof(SceneData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<SceneData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "SceneData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			SceneData.Param p = new SceneData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.scene_key = cell.ToString();
					break;
				case 1:
					try {
						p.scene_no = Int32.Parse(cell.ToString());
					} catch {
						p.scene_no = 0;
					}
					break;
				case 2:
					p.sphereKind = cell.ToString();
					break;
				case 3:
					p.texture_name = cell.ToString();
					break;
				case 4:
					try {
						p.camera_rotate = Int32.Parse(cell.ToString());
					} catch {
						p.camera_rotate = 0;
					}
					break;
				case 5:
					p.scene_content = cell.ToString();
					break;
				case 6:
					p.touch_prefab = cell.ToString();
					break;
				case 7:
					p.prefab_pass = cell.ToString();
					break;
				case 8:
					p.room_name = cell.ToString();
					break;
				case 9:
					p.event_key = cell.ToString();
					break;
				case 10:
					p.scene_decisioin_flg = cell.ToString();
					break;
				case 11:
					p.bgm_name = cell.ToString();
					break;
				case 12:
					if(string.Empty != cell.ToString()){
						p.bgm_vol = float.Parse(cell.ToString());
					}
					break;
				}
			}
			if(null != p.scene_key && string.Empty != p.scene_key){
				p.scene_key = p.scene_key.Replace("¥r¥n","");
			}
			if(string.Empty != p.scene_key){
				data.list.Add (p);
			}
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "SceneData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetEVENTNAMEsheetData (ISheet sheet)
	{
		EventNameData data = (EventNameData)AssetDatabase.LoadAssetAtPath (exportPath + "EventNameData.asset", typeof(EventNameData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<EventNameData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "EventNameData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			EventNameData.Param p = new EventNameData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.event_name = cell.ToString();
					break;
				case 1:
					p.scene_array = cell.ToString().Split(',');
					break;
				case 2:
					p.limit_flg = cell.ToString();
					break;
				case 3:
					p.limit_flg_nothing = cell.ToString();
					break;
				case 4:
					p.context = cell.ToString();
					break;
				case 5:
					try {
						p.touch_count = Int32.Parse(cell.ToString());
					} catch {
						p.touch_count = 0;
					}
					break;
				}
			}
			if(string.Empty != p.event_name){
				data.list.Add (p);
			}
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "EventNameData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}


	static void SetDBGsheetData (ISheet sheet)
	{
		DBGData data = (DBGData)AssetDatabase.LoadAssetAtPath (exportPath + "DBGData.asset", typeof(DBGData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<DBGData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "DBGData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			DBGData.Param p = new DBGData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.event_key = cell.ToString();
					break;
				case 1:
					p.flg_array = cell.ToString().Split(',');
					break;
				case 2:
					p.item_flg_array = cell.ToString().Split(',');
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "DBGData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetITEMsheetData (ISheet sheet)
	{
		ItemData data = (ItemData)AssetDatabase.LoadAssetAtPath (exportPath + "ItemData.asset", typeof(ItemData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<ItemData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "ItemData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			ItemData.Param p = new ItemData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				//Debug.Log( "Cell"+cellIndex+":"+cell.CellType+","+cell );

				switch(cellIndex)
				{
				case 0:
					p.item_key = cell.ToString();
					break;
				case 1:
					p.item_content = cell.ToString();
					break;
				case 2:
					p.item_name = cell.ToString();
					break;	
				case 3:
					p.use_event_array = cell.ToString().Split(',');
					break;
				case 4:
					p.match_item = cell.ToString();
					break;
				case 5:
					p.matched_item_name = cell.ToString();
					break;
				case 6:
					if(null != cell.ToString() && string.Empty != cell.ToString()){
						p.isDeleteInMatch = true;
					} else {
						p.isDeleteInMatch = false;
					}
					break;
				case 7:
					p.match_event = cell.ToString();
					break;
				}
			}
			if(null != p.item_key && string.Empty != p.item_key){
				data.list.Add (p);
			}
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "ItemData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetARCHIVEsheetData (ISheet sheet)
	{
		ArchiveData data = (ArchiveData)AssetDatabase.LoadAssetAtPath (exportPath + "ArchiveData.asset", typeof(ArchiveData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<ArchiveData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "ArchiveData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			ArchiveData.Param p = new ArchiveData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );

				switch(cellIndex)
				{
				case 0:
					p.archive_key = cell.ToString();
					break;
				case 1:
					p.archive_title = cell.ToString();
					break;
				case 2:
					p.archive_content = cell.ToString();
					break;
				case 3:
					p.archive_format = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "ArchiveData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetPLACEsheetData (ISheet sheet)
	{
		PlaceData data = (PlaceData)AssetDatabase.LoadAssetAtPath (exportPath + "PlaceData.asset", typeof(PlaceData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<PlaceData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "PlaceData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			PlaceData.Param p = new PlaceData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.place_key = cell.ToString();
					break;
				case 1:
					p.place_name = cell.ToString();
					break;
				case 2:
					p.place_floor = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "PlaceData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetHINTsheetData (ISheet sheet)
	{
		HintData data = (HintData)AssetDatabase.LoadAssetAtPath (exportPath + "HintData.asset", typeof(HintData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<HintData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "HintData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			HintData.Param p = new HintData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.no = cell.ToString();
					break;
				case 1:
					p.title = cell.ToString();
					break;
				case 2:
					p.content = cell.ToString();
					break;
				case 3:
					p.place_name = cell.ToString();
					break;
				case 4:
					p.scene_name = cell.ToString().Split(',');
					break;
				case 5:
					p.no_flg_name = cell.ToString();
					break;
				case 6:
					p.exist_flg_name = cell.ToString();
					break;
				case 7:
					p.magatama_count = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "HintData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}

	static void SetACHIEVEMENTsheetData (ISheet sheet)
	{
		AchievementData data = (AchievementData)AssetDatabase.LoadAssetAtPath (exportPath + "AchievementData.asset", typeof(AchievementData));

		if (data == null) {
			data = ScriptableObject.CreateInstance<AchievementData> ();
			AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath + "AchievementData.asset");
			data.hideFlags = HideFlags.NotEditable;
		}

		data.list.Clear ();

		int lastRowNum = sheet.LastRowNum;

		for( int rowIndex = sheet.FirstRowNum;rowIndex <= lastRowNum;++rowIndex ){
			IRow row = sheet.GetRow(rowIndex);
			if( row==null || rowIndex == 0) continue;

			int lastCellNum = row.LastCellNum;
			AchievementData.Param p = new AchievementData.Param ();

			for( int cellIndex = row.FirstCellNum;cellIndex < lastCellNum;++cellIndex ){
				ICell cell = row.GetCell( cellIndex );
				if(cell == null)	continue;

				switch(cellIndex)
				{
				case 0:
					p.no = cell.ToString();
					break;
				case 2:
					p.point = cell.ToString();
					break;
				case 3:
					p.hint = cell.ToString();
					break;
				case 4:
					p.flgName = cell.ToString();
					break;
				}
			}
			data.list.Add (p);
		}
		ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath + "AchievementData.asset", typeof(ScriptableObject)) as ScriptableObject;
		EditorUtility.SetDirty (obj);
	}


}