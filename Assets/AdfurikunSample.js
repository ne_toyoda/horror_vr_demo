﻿#pragma strict

enum SCENE_STATE {
	MAIN,
	QUIT_WAIT,
	QUIT,
	END
}

private var sceneState : SCENE_STATE = SCENE_STATE.MAIN;
private var initialized : boolean = false;

#if UNITY_IPHONE
private var ADFURIKUN_APPID : String = "********************"; 
#elif UNITY_ANDROID
private var ADFURIKUN_APPID : String = "********************"; 
#else
private var ADFURIKUN_APPID : String = "********************";
#endif

function Update () {
	if (!initialized) {
		initialized = true;
		var object : GameObject = GameObject.Find("AdfurikunUtility");
		if (object != null) {
			var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
			if (au != null) {
				au.addCustomSizeAd(ADFURIKUN_APPID, 100, 400, AdfurikunUtility.AdSize.ICON, AdfurikunBannerAd.AndroidTransition.NOTHING, true);
			}
		}
	}
	
	switch (this.sceneState) {
		case SCENE_STATE.MAIN:
			break;
		case SCENE_STATE.QUIT_WAIT:
			this.sceneState = SCENE_STATE.QUIT;
			break;
		case SCENE_STATE.QUIT:
			this.sceneState = SCENE_STATE.END;
			Application.Quit();
			break;
		case SCENE_STATE.END:
			break;
	}
}

function OnGUI () {
	if (GUI.Button(Rect(10,10,200,80), "ShowBannerAd")) {
		ShowAdfurikunBannerAd(0, true);
	}
	if (GUI.Button(Rect(220,10,200,80), "HideBannerAd")) {
		ShowAdfurikunBannerAd(0, false);
	}
	if (GUI.Button(Rect(10,100,200,80), "ShowCustomSizeAd")) {
		ShowAdfurikunCustomSizeAd(0, true);
	}
	if (GUI.Button(Rect(220,100,200,80), "HideCustomSizeAd")) {
		ShowAdfurikunCustomSizeAd(0, false);
	}
	if (GUI.Button(Rect(10,190,200,80), "ShowIntersAd")) {
		ShowAdfurikunIntersAd(0);
	}
	if (GUI.Button(Rect(10,280,200,80), "ShowWallAd(Android)")) {
		ShowAdfurikunWallAd();
	}
	if (GUI.Button(Rect(220,190,200,80), "cancelIntersAd(Android)")) {
		CancelAdfurikunIntersAd();
	}
	
}

function ShowAdfurikunBannerAd (index : int, show : boolean) {
	var object : GameObject = GameObject.Find("AdfurikunUtility");
	if (object != null) {
		var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
		if (au != null) {
			if (show) {
				au.showBannerAd(index);
			} else {
				au.hideBannerAd(index);
			}
		}
	}
}

function ShowAdfurikunCustomSizeAd (index : int, show : boolean) {
	var object : GameObject = GameObject.Find("AdfurikunUtility");
	if (object != null) {
		var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
		if (au != null) {
			if (show) {
				au.showCustomSizeAd(index);
			} else {
				au.hideCustomSizeAd(index);
			}
		}
	}
}

function ShowAdfurikunIntersAd (index : int) {
	var object : GameObject = GameObject.Find("AdfurikunUtility");
	if (object != null) {
		var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
		if (au != null) {
			au.showIntersAd(this.gameObject, index);
		}
	}
}

function CancelAdfurikunIntersAd () {
	var object : GameObject = GameObject.Find("AdfurikunUtility");
	if (object != null) {
		var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
		if (au != null) {
			au.cancelIntersAd();
		}
	}
}

function ShowAdfurikunWallAd () {
	var object : GameObject = GameObject.Find("AdfurikunUtility");
	if (object != null) {
		var au : AdfurikunUtility = object.GetComponent.<AdfurikunUtility>();
		if (au != null) {
			au.showWallAd(this.gameObject);
		}
	}
}

// -------------------------------
// インタースティシャル広告CallBack
// -------------------------------
function AdfurikunIntersAdCustomClose (index : int) {
	// カスタムボタンが押されてインタースティシャル広告が閉じた時の処理
	if (index == 0) {
		this.sceneState = SCENE_STATE.QUIT_WAIT;
		Time.timeScale = 0;
	}
}

function AdfurikunIntersAdClose (index : int) {
	// インタースティシャル広告が閉じた時の処理
	Debug.LogWarning("AdfurikunIntersAdClose ");
}

function AdfurikunIntersAdNotDisplayed (index : int) {
	// インタースティシャル広告が表示せずに終了した時の処理（最大表示回数オーバー・表示頻度等）
}

// -------------------------------
// Wall広告CallBack
// -------------------------------
function AdfurikunWallAdClose () {
	// Wall広告が閉じた時の処理
}

function AdfurikunWallAdNotDisplayed () {
	// Wall広告が表示せずに終了した時の処理
}
