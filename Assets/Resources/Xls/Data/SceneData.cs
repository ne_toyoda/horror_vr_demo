﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string scene_key;
		public int scene_no;
		public string sphereKind;
		public string texture_name;
		public int camera_rotate;
		public string scene_content;
		public string touch_prefab;
		public string prefab_pass;
		public string room_name;
		public string event_key;
		public string scene_decisioin_flg;
		public string bgm_name;
		public float bgm_vol;
	}
}
