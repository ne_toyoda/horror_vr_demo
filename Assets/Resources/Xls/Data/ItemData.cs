﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string item_key;
		public string item_content;
		public string item_name;
		public string[] use_event_array;
		public string match_item;
		public string matched_item_name;
		public bool isDeleteInMatch;
		public string match_event;
	}
}
