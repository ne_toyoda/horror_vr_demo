﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AchievementData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string no;
		public string point;
		public string hint;
		public string flgName;
	}
}
