﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DBGData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string event_key;
		public string[] item_flg_array;
		public string[] flg_array;
	}
}
