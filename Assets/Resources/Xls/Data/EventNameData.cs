﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventNameData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string event_name;
		public string[] scene_array;
		public string limit_flg;
		public string limit_flg_nothing;
		public string context;
		public int touch_count;
	}
}
