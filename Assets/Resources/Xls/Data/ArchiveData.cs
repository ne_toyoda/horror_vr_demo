﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArchiveData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string archive_key;
		public string archive_title;
		public string archive_content;
		public string archive_format;
	}
}
