﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaceData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string place_key;
		public string place_name;
		public string place_floor;
	}
}
