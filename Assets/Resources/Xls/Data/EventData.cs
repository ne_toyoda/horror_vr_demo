﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string event_key;
		public int no;
		public string event_content;
		public string content;
		public string other;
	}
}
