﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HintData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string no;
		public string title;
		public string content;
		public string place_name;
		public string[] scene_name;
		public string no_flg_name;
		public string exist_flg_name;
		public string magatama_count;
	}
}
