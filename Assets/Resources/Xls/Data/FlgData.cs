﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlgData : ScriptableObject {

	public List<Param> list = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		public string flg_key;
		public int flg_no;
		public string flg_content;
	}
}
