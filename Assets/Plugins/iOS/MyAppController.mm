#import "UnityAppController.h"

@interface MyAppController : UnityAppController
@end

@implementation MyAppController
- (void) applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [super applicationDidReceiveMemoryWarning:application];
    printf_console("メモリやばいよ\n");
    UnitySendMessage("MemoryWarningReciever", "DidReceiveMemoryWarning","");
}
@end

IMPL_APP_CONTROLLER_SUBCLASS(MyAppController)
