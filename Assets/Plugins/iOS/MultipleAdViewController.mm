#import "MultipleAdViewController.h"

#define BANNER_REFRESH_RATE 30

// ※最前面じゃない箇所に配置したいという要望が多く頂きましたので、実際に描画しているViewに配置するように変更しています。
// 最前面に表示したい場合には、+(void)showBannerAd:(int)index 、+(void) showIntersAd:(int)index を変更するようにしてください。
//
// Unity出力の設定を呼び出しています。
// classes/iPhone_view.mm にて取得できるメソッドが確認できますので、適宜必要な情報を取得してください。
extern UIView* UnityGetGLView();

// こちらは、UnityのAdfurikunUnityに値を返す為の関数になります。
extern void UnitySendMessage(const char *, const char *, const char *);

// こちらは、Unityから呼び出せるようにするための記述です。
// Cでの関数を追加した際には追加するようにしましょう。
extern "C" {
    void addAdfurikunBannerAdIOS_(char *appID, int position, int test_mode, int transition, bool initvisible);
    void addAdfurikunIntersAdIOS_(char *appID, int test_mode, int frequency, int max, char *titleText);
    void hideBannerAdIOS_(int index);
    void showBannerAdIOS_(int index);
    void cancelIntersAdIOS_();
    void showIntersAdIOS_(int index);
    void closeIntersAdIOS_(int index);
    void addAdfurikunCustomSizeAdIOS_(char *appID, float x, float y , float width, float height, int transition, bool initvisible);
    void showCustomSizeAdIOS_(int index);
    void hideCustomSizeAdIOS_(int index);
    
}

@implementation MultipleAdViewController

@synthesize bannerView = _bannerView;
static NSMutableArray* adBannerList = nil; // バナー形式のAdfurikunView を格納する為の配列
static NSMutableArray* adIntersList = nil; // インタースティシャル型のAdfurikunを格納する為の配列
static NSMutableArray* adCustomList = nil; // カスタムサイズ広告のAdfurikunを格納する為の配列
static MultipleAdViewController *instance = nil; // クラスのインスタンス

+ (CGSize) determineAdSize{
    return ADFRJS_VIEW_SIZE_320x50;
}

// こちら、NSLogを残したままご利用になられる方が多かったので、
// Debug の場合のみLogを表示するようにしています。
#ifdef _ADF_DEBUG_ENABLE_
#define adfuri_debug_NSLog(format, ...) NSLog(format, ## __VA_ARGS__)

#else
#define adfuri_debug_NSLog(format, ...)
#endif

/**
 *
 * バナー広告の設定箇所です。
 * 配列にバナー広告のView を入れて広告の表示を行います。表示順についてはshowBannerAd をご覧ください。
 * 初期表示する場合にはinitVisible にチェックを入れてください。
 *
 */
+ (void) addAdfurikunBanner:(NSString *)appID position:(int)position test_mode:(int)test_mode
                 transition:(int)transition initVisible:(BOOL)initvisible {
    
    if(adBannerList == nil) {
        adBannerList = [[NSMutableArray alloc] init];
    }
    
    if ( instance == nil) {
        MultipleAdViewController *adViewController = [[MultipleAdViewController alloc] init];
        instance = adViewController;
    }
    
    // Init Adfurikun
    CGSize adSize = [MultipleAdViewController determineAdSize];
    AdfurikunView *bannerView = [[AdfurikunView alloc] initWithFrame:CGRectMake(0,0, adSize.width,adSize.height)];
    bannerView.appID = appID;
    bannerView.delegate = instance;
    
    // 表示位置の指定。表示位置を変更したい場合にはここを変更してください。
    // ※Unity の画面サイズは縦横 変更しても変わりませんので、
    // RootViewController.interfaceOrientationなどで画面方向に合わせるようにしましょう。
    CGRect frame;
    UIView* unityView = UnityGetGLView();
    if (position == AdPositionTop) {
        frame = CGRectMake((unityView.frame.size.width - adSize.width)/2,
                           0.0,
                           adSize.width,
                           adSize.height);
        
    }else if(position == AdPositionBottom){
        frame = CGRectMake((unityView.frame.size.width - adSize.width)/2,
                           unityView.frame.size.height -adSize.height,
                           adSize.width,
                           adSize.height);
    }
    
    bannerView.frame = frame;
    
    if ( test_mode == AdTest ) {
        [bannerView testModeEnable];
    }
    
    // transiton animation
    if ( transition == 0 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionNone;
    }
    if ( transition == 1 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionRandom;
    }
    if ( transition == 6 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionFadeIn;
    }
    if ( transition == 2 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromRight:oldAdView:newAdView:);
    }
    if ( transition == 3 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromLeft:oldAdView:newAdView:);
    }
    if ( transition == 4 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromTop:oldAdView:newAdView:);
    }
    if ( transition == 5 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromBottom:oldAdView:newAdView:);
    }
    
    // 配列に生成したビューを格納
    [adBannerList addObject:bannerView];
    [bannerView release];
    //[adBannerList setObject:bannerView forKey:[bannerView getAppId]];
    //[bannerView startShowAd];
    
    if ( initvisible == YES ) {
        [MultipleAdViewController showBannerAd:[adBannerList count]-1];
    }
    
}


/**
 * 基本的にはaddAdfurikunBannerと変わりませんが、表示位置の指定が入っています
 *
 *
 */
+ (void) addCustomSizeBanner:(NSString *)appID x:(float)x y:(float)y width:(float)width height:(float)height
                  transition:(int)transition initVisible:(BOOL)initvisible {
    
    if(adCustomList == nil) {
        adCustomList = [[NSMutableArray alloc] init];
    }
    
    if ( instance == nil) {
        MultipleAdViewController *adViewController = [[MultipleAdViewController alloc] init];
        instance = adViewController;
    }
    
    // Init Adfurikun
    CGSize adSize = [MultipleAdViewController determineAdSize];
    AdfurikunView *bannerView = [[AdfurikunView alloc] initWithFrame:CGRectMake(0,0, adSize.width,adSize.height)];
    bannerView.appID = appID;
    bannerView.delegate = instance;
    
    CGRect frame;
    frame = CGRectMake(x, y, width, height);
    
    
    bannerView.frame = frame;
    
    // transiton animation
    if ( transition == 0 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionNone;
    }
    if ( transition == 1 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionRandom;
    }
    if ( transition == 6 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionFadeIn;
    }
    if ( transition == 2 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromRight:oldAdView:newAdView:);
    }
    if ( transition == 3 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromLeft:oldAdView:newAdView:);
    }
    if ( transition == 4 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromTop:oldAdView:newAdView:);
    }
    if ( transition == 5 ) {
        bannerView.transitionType = AdfurikunViewAnimationTransitionCustom;
        bannerView.customAnimationClass = instance;
        bannerView.customAnimationMethod = @selector(customAanimationSlideFromBottom:oldAdView:newAdView:);
    }
    
    // 配列に生成したビューを格納
    [adCustomList addObject:bannerView];
    [bannerView release];
    
    if ( initvisible == YES ) {
        [MultipleAdViewController showCustomSizeAd:[adCustomList count]-1];
    }
    
}



/**
 * インタースティシャル広告の設定をしています。
 * この関数では表示を行わずに、設定と先読み飲みしているので御注意ください。
 *
 */
+ (void) addAdfurikunInters:(NSString *)appID test_mode:(int)test_mode
                  frequency:(int)frequency max:(int)max titleText:(NSString *)titleText{
    
    if(adIntersList == nil) {
        adIntersList = [[NSMutableArray alloc] init];
    }
    
    if ( instance == nil) {
        MultipleAdViewController *adViewController = [[MultipleAdViewController alloc] init];
        instance = adViewController;
    }
    
    AdfurikunPopupView *adPopupView = [[AdfurikunPopupView alloc] init];
    adPopupView.delegate = instance;
    adPopupView.appId = appID;
    
    if ( test_mode == AdTest ) {
        [adPopupView testModeEnable];
    }
    if(titleText != nil && [titleText length] > 0) {
        adPopupView.titleLabel.text = titleText;
    }
    [adPopupView setSchedule:frequency];
    [adPopupView setDisplayCount:max];
    [adPopupView preloadAd];
    //[adPopupView setBarGradient: [UIColor blueColor] endColor: [UIColor blueColor]];
    
    [adIntersList addObject:adPopupView];
    [adPopupView release];
}


- (id)init {
    self = [super init];
    return self;
}


/**
 * バナー広告の表示を行っています。
 *
 */
+(void)showBannerAd:(int)index {
    AdfurikunView *adView = (AdfurikunView *)[adBannerList objectAtIndex:index];
    if (adView == nil) {
        return;
    }
        
    [adView startShowAd];
    
    UIView* unityView = UnityGetGLView();
    // UnityView に広告のView を追加
    [unityView addSubview:adView];
    
    // どの画面よりも最前面に置きたい場合には
    // UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    // にAddSubView をしてください。
}

/**
 * バナー広告を非表示化し、更新を止めています。
 *
 */
+(void) hideBannerAd:(int)index {
    AdfurikunView *adView = (AdfurikunView *)[adBannerList objectAtIndex:index];
    if (adView == nil) {
        return;
    }
    [adView removeFromSuperview];
    [adView stopUpdating];
}


/**
 * カスタムサイズ広告の表示
 *
 */
+(void)showCustomSizeAd:(int)index {
    
    AdfurikunView *adView = (AdfurikunView *)[adCustomList objectAtIndex:index];
    if (adView == nil) {
        return;
    }
    [adView startShowAd];
    
    UIView* unityView = UnityGetGLView();
    // UnityView に広告のView を追加
    [unityView addSubview:adView];
    
    // どの画面よりも最前面に置きたい場合には
    // UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    // にAddSubView をしてください。
}

/**
 * カスタムサイズ広告の非表示化し、更新を止めています。
 *
 */
+(void) hideCustomSizeAd:(int)index {
    AdfurikunView *adView = (AdfurikunView *)[adCustomList objectAtIndex:index];
    if (adView == nil) {
        return;
    }
    [adView removeFromSuperview];
    [adView stopUpdating];
}


/**
 * インタースティシャル広告の表示を行っています。
 *
 */
+(void) showIntersAd:(int)index {
    
    AdfurikunPopupView *adView = (AdfurikunPopupView *)[adIntersList objectAtIndex:index];
    if (adView == nil) {
        return;
    }
    
    UIView* unityView = UnityGetGLView();
    // UnityView に広告のView を追加
    [unityView addSubview:adView];
    [adView preloadShowAd];
    
    // どの画面よりも最前面に置きたい場合には
    // UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    // にAddSubView をしてください。
}

+(void) closeIntersAd:(int)index {
    
}


- (void)dealloc {
    
    //
    if ( adBannerList != nil ) {
        for(int i=0; i<[adBannerList count]; i++) {
            AdfurikunView *adView = [adBannerList objectAtIndex:i];
            adView.delegate = nil;
            [adView release];
        }
        [adBannerList release];
        adBannerList = nil;
    }
    
    if (adIntersList != nil) {
        for(int i=0; i<[adIntersList count]; i++) {
            AdfurikunPopupView *adView = [adIntersList objectAtIndex:i];
            adView.delegate = nil;
            [adView release];
        }
        [adIntersList release];
        adIntersList = nil;
        
    }
    
    if ( adCustomList != nil ) {
        for(int i=0; i<[adCustomList count]; i++) {
            AdfurikunView *adView = [adCustomList objectAtIndex:i];
            adView.delegate = nil;
            [adView release];
        }
        [adCustomList release];
        adCustomList = nil;
    }
    
    instance = nil;
    [super dealloc];
}


-(void)adfurikunViewDidFinishLoad:(AdfurikunBaseView *)view{
    adfuri_debug_NSLog( @"adfurikun: ad loaded" );
}
-(void)adfurikunViewAdTapped:(AdfurikunBaseView *)view{
    adfuri_debug_NSLog( @"adfurikun: ad clicked" );
}

// 広告が閉じられた際に呼び出されます。
// スケジュール指定を行っている場合には、表示外の時もここが呼ばれます。
-(void)adfurikunViewAdClose:(AdfurikunPopupView *)view{
    
    adfuri_debug_NSLog( @"adfurikun: ad closeType %d", view.closeType );
    if (view.closeType == AdfurikunPopUpCloseTypeTouchUpButton || view.closeType==AdfurikunPopUpCloseTypeScheduleSkip) {
        [view preloadAd];
    }
    
    // 閉じられた内容をUnityに送っています。
    NSUInteger index = [adIntersList indexOfObject:view];
    char* indexStr = (char*)[[NSString stringWithFormat:@"%d", index] UTF8String];
    
    switch (view.closeType) {
        case AdfurikunPopUpCloseTypeScheduleSkip:
            UnitySendMessage("AdfurikunUtility", "IntersAdSkip", indexStr);
            break;
            
        case AdfurikunPopUpCloseTypeMaxDisplay:
            UnitySendMessage("AdfurikunUtility", "IntersAdMaxEnd", indexStr);
            break;
            
        case AdfurikunPopUpCloseTypeNetworkFailed:
            UnitySendMessage("AdfurikunUtility", "IntersAdError", indexStr);
            break;
            
        case AdfurikunPopUpCloseTypeAdInfoFailed:
            UnitySendMessage("AdfurikunUtility", "IntersAdError", indexStr);
            break;
            
        default:
            UnitySendMessage("AdfurikunUtility", "IntersAdClose", indexStr);
            break;
    }
}


// Custom transition anime

-(void) customAanimationSlideFromRight:(UIView*)baseView oldAdView:(UIView *)oldAdView newAdView:(UIView *)newAdView {
    
    // アニメーション処理
    newAdView.frame = CGRectMake(self.view.frame.size.width,
                                 0.0f,
                                 newAdView.frame.size.width,
                                 newAdView.frame.size.height);
    
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options: UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         newAdView.frame = CGRectMake(0.0f,
                                                      0.0f,
                                                      newAdView.frame.size.width,
                                                      newAdView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         // 過去画面はアニメーションが終了したら外すようにお願いします。
                         [oldAdView removeFromSuperview];
                     }
     ];
    
    
}

-(void) customAanimationSlideFromLeft:(UIView*)baseView oldAdView:(UIView *)oldAdView newAdView:(UIView *)newAdView {
    
    // アニメーション処理
    newAdView.frame = CGRectMake(self.view.frame.size.width * -1,
                                 0.0f,
                                 newAdView.frame.size.width,
                                 newAdView.frame.size.height);
    
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options: UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         newAdView.frame = CGRectMake(0.0f,
                                                      0.0f,
                                                      newAdView.frame.size.width,
                                                      newAdView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         // 過去画面はアニメーションが終了したら外すようにお願いします。
                         [oldAdView removeFromSuperview];
                     }
     ];
    
    
}

-(void) customAanimationSlideFromTop:(UIView*)baseView oldAdView:(UIView *)oldAdView newAdView:(UIView *)newAdView {
    
    // アニメーション処理
    newAdView.frame = CGRectMake(0.0f,
                                 newAdView.frame.size.height * -1,
                                 newAdView.frame.size.width,
                                 newAdView.frame.size.height);
    
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options: UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         newAdView.frame = CGRectMake(0.0f,
                                                      0.0f,
                                                      newAdView.frame.size.width,
                                                      newAdView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         // 過去画面はアニメーションが終了したら外すようにお願いします。
                         [oldAdView removeFromSuperview];
                     }
     ];
    
    
}

-(void) customAanimationSlideFromBottom:(UIView*)baseView oldAdView:(UIView *)oldAdView newAdView:(UIView *)newAdView {
    
    // アニメーション処理
    newAdView.frame = CGRectMake(0.0f,
                                 self.view.frame.size.height,
                                 newAdView.frame.size.width,
                                 newAdView.frame.size.height);
    
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options: UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         newAdView.frame = CGRectMake(0.0f,
                                                      0.0f,
                                                      newAdView.frame.size.width,
                                                      newAdView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         // 過去画面はアニメーションが終了したら外すようにお願いします。
                         [oldAdView removeFromSuperview];
                     }
     ];
    
    
}




@end

// ココからはUnity から呼び出すための関数を指定しています。
// 機能を追加して、Unityから呼び出す際にはココに書いてください。
void addAdfurikunBannerAdIOS_(char *appID, int position, int test_mode, int transition, bool initvisible)
{
    [MultipleAdViewController addAdfurikunBanner:
     [NSString stringWithCString:appID encoding:NSASCIIStringEncoding]
                                        position:position
                                       test_mode:test_mode
                                      transition:transition
                                     initVisible:initvisible];
    
    
}

void addAdfurikunCustomSizeAdIOS_(char *appID, float x, float y , float width, float height, int transition, bool initvisible)
{
    [MultipleAdViewController addCustomSizeBanner:
     [NSString stringWithCString:appID encoding:NSASCIIStringEncoding]
                                                x:x
                                                y:y
                                            width:width
                                           height:height
                                       transition:transition
                                      initVisible:initvisible];
    
    
}



void addAdfurikunIntersAdIOS_(char *appID, int test_mode, int frequency, int max, char *titleText)
{
    
    [MultipleAdViewController addAdfurikunInters:
     [NSString stringWithCString:appID encoding:NSASCIIStringEncoding]
                                       test_mode:test_mode
                                       frequency:frequency
                                             max:max
                                       titleText:[NSString stringWithCString:titleText encoding:NSUTF8StringEncoding]
     ];
    
}

void hideBannerAdIOS_(int index){
    
    if(adBannerList != nil){
        [MultipleAdViewController hideBannerAd:index];
    }
}

void showBannerAdIOS_(int index){
    if(adBannerList != nil){
        [MultipleAdViewController showBannerAd:index];
    }
    
}

void showCustomSizeAdIOS_(int index){
    if(adCustomList != nil){
        [MultipleAdViewController showCustomSizeAd:index];
    }
    
}
void hideCustomSizeAdIOS_(int index){
    
    if(adCustomList != nil){
        [MultipleAdViewController hideCustomSizeAd:index];
    }
}


void cancelIntersAdIOS_(){
    
}

void showIntersAdIOS_(int index) {
    if (adIntersList != nil){
        [MultipleAdViewController showIntersAd:index];
    }
}

void closeIntersAdIOS_(int index) {
    if (adIntersList != nil){
        [MultipleAdViewController closeIntersAd:index];
    }
    
}
