//
//  AdfurikunConversion.h
//
//  Copyright (c) Terajima Joho Kikaku Co., Ltd. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@interface AdfurikunConversion : NSObject
+ (void)doReportConversion:(NSString *)b_id;

@end
