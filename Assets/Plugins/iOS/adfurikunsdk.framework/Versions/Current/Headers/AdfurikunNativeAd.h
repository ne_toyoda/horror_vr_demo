//
//  AdfurikunNativeAd.h
//
//  Copyright (c) Terajima Joho Kikaku Co., Ltd. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "AdfurikunNativeAdInfo.h"

@protocol AdfurikunNativeAdDelegate <NSObject>
/** デリゲートの定義 */
@required
-(void)apiDidFinishLoading:(AdfurikunNativeAdInfo *)nativeAdInfo adnetworkKey:(NSString *)adnetworkKey;
-(void)apiDidFailWithError:(int)err adnetworkKey:(NSString *)adnetworkKey;
@end

@interface AdfurikunNativeAd : NSObject

@property (weak, nonatomic) id<AdfurikunNativeAdDelegate> delegate;
//@property
/**
 * 広告表示を開始する。
 */
-(id)init:(NSString *)withAppID;
-(void)getNativeAd;
-(void)apiDidFinishLoading:(AdfurikunNativeAdInfo *)nativeAdInfo adnetworkKey:(NSString *)adnetworkKey;
-(void)apiDidFailWithError:(int)err adnetworkKey:(NSString *)adnetworkKey;
@end
