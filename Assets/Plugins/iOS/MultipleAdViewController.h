#import <UIKit/UIKit.h>
#import <adfurikunsdk/AdfurikunPopupView.h>

@interface MultipleAdViewController : UIViewController  <AdfurikunViewDelegate,AdfurikunPopupViewDelegate>{
}

+ (void) addAdfurikunBanner:(NSString *)appID position:(int)position test_mode:(int)test_mode
                 transition:(int)transition initVisible:(BOOL)initvisible;
+ (void) addAdfurikunInters:(NSString *)appID test_mode:(int)test_mode
                  frequency:(int)frequency max:(int)max;
- (id)init;
+(void)showBannerAd:(int)index;
+(void) hideBannerAd:(int)index;
+(void) showIntersAd:(int)index;
+(void) closeIntersAd:(int)index;
- (void)dealloc;

@property(nonatomic, retain) AdfurikunView *bannerView;

enum _AdPosition{
    AdPositionTop ,
    AdPositionBottom
};

enum _AdTest{
    AdTest,
    AdProduct
};

@end

