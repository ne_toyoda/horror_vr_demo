using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

[System.Serializable]
public class AdfurikunBannerAd {
    public enum Position
    {
        TOP,
        BOTTOM
    }
	
	public enum AndroidTransition
	{
		NOTHING,
		RANDOM,
		SLIDE_FROM_RIGHT,
		SLIDE_FROM_LEFT,
		SLIDE_FROM_TOP,
		SLIDE_FROM_BOTTOM,
		FADEIN_FADEOUT
	}
	
	public Position position;
    public AndroidTransition transition;
	public bool visible;
	public string iPhoneAppID;
	public string androidAppID;
}

[System.Serializable]
public class AdfurikunIntersAd {
	public int frequency;
	public int max;
	public string iPhoneAppID;
	public string androidAppID;
    public string titleBarText;
    public string intersadButtonName;
    public string customButtonName;
}

[System.Serializable]
public class AdfurikunWallAd {
	public enum WallAdTheme
	{
		RANDOM,
		DARK,
		LIGHT
	}
	
	public WallAdTheme theme;
	public string iPhoneAppID;
	public string androidAppID;
}

public class AdfurikunUtility : MonoBehaviour {
	public enum TestMode
	{
	   TEST,
	   PRODUCT
	}
	
	public enum AdSize
	{
		BANNER,
		ICON
	}
	
	public TestMode testMode;
	public AdfurikunBannerAd[] bannerAd;
	public AdfurikunIntersAd[] intersAd;
	public AdfurikunWallAd wallAd;
	
	private static AdfurikunUtility mInstance = null;
	private GameObject intersSrcObject = null;
	private GameObject wallSrcObject = null;

#if UNITY_IPHONE
   	[DllImport("__Internal")]
   	private static extern void addAdfurikunBannerAdIOS_(string appID, int position,int test_mode, int transition, bool initVisible);
   	[DllImport("__Internal")]
   	private static extern void addAdfurikunIntersAdIOS_(string appID, int test_mode, int frequency, int max, string titleText);
   	[DllImport("__Internal")]
   	private static extern void showIntersAdIOS_(int index);
   	[DllImport("__Internal")]
   	private static extern void showBannerAdIOS_(int index);
   	[DllImport("__Internal")]
   	private static extern void hideBannerAdIOS_(int index);
	[DllImport("__Internal")]
	private static extern  void addAdfurikunCustomSizeAdIOS_(string appID, float x, float y , float width, float height, int transition, bool initvisible);
	[DllImport("__Internal")]
	private static extern void showCustomSizeAdIOS_(int index);
	[DllImport("__Internal")]
	private static extern void hideCustomSizeAdIOS_(int index);

#elif UNITY_ANDROID
	private float androidDensity = 1.0f;
#endif

	public static AdfurikunUtility instance
	{
		get
		{
			return mInstance;
		}
	}
	
	public void Awake ()
	{
		if (mInstance == null) {
			mInstance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
	public void OnDestroy ()
	{
		if (Application.isEditor) return;
		
#if UNITY_IPHONE
//        if (mInstance == this)
//        {
//            releaseAdfurikunIOS_();
//        }
#endif
	}
	
	public void OnApplicationPause (bool pause) {
        if (Application.isEditor) return;

#if UNITY_IPHONE
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("onPause", activity, pause);
        }
#endif
	}

    public void Start()
    {
        if (Application.isEditor) return;

#if UNITY_IPHONE
		 if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			foreach(AdfurikunBannerAd ad in bannerAd) {
				addAdfurikunBannerAdIOS_(ad.iPhoneAppID,  (int)ad.position, (int)testMode, (int)ad.transition, ad.visible);
			}

			foreach (AdfurikunIntersAd iad in intersAd) {
				addAdfurikunIntersAdIOS_(iad.iPhoneAppID,  (int)testMode, (int)iad.frequency, (int)iad.max, iad.titleBarText);
			}
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("adfurikunInitialize", activity, (int)testMode);
			foreach (AdfurikunBannerAd ad in bannerAd) {
				plugin.CallStatic("addBannerAd", activity, ad.androidAppID, (int)ad.position, (int)ad.transition, ad.visible);
			}
			foreach (AdfurikunIntersAd iad in intersAd) {
				plugin.CallStatic("addIntersAdSetting", activity, iad.androidAppID, iad.titleBarText, iad.frequency, iad.max, iad.intersadButtonName, iad.customButtonName);
			}
			if (wallAd.androidAppID.Length > 0) {
				plugin.CallStatic("initializeWallAdSetting", activity, wallAd.androidAppID, (int)wallAd.theme);
			}
			AndroidJavaObject wm = activity.Call<AndroidJavaObject>("getWindowManager");
			if (wm != null) {
				AndroidJavaObject display = wm.Call<AndroidJavaObject>("getDefaultDisplay");
				if (display != null) {
					AndroidJavaObject displayMetrics = new AndroidJavaObject("android.util.DisplayMetrics");
					display.Call("getMetrics", displayMetrics);
					androidDensity = displayMetrics.Get<float>("density");
				}
			}
        }
#endif
    }
	
	// ------------------------------------------------
	// バナー広告
	// ------------------------------------------------
    public void hideBannerAd(int index)
    {
		if (index < bannerAd.Length) {
#if UNITY_IPHONE
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				hideBannerAdIOS_(index);
			}
#elif UNITY_ANDROID
	        if (Application.platform == RuntimePlatform.Android)
			{
				AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
				AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
				plugin.CallStatic("hideBannerAd", activity, index);
			}
#endif
		}
    }

    public void showBannerAd(int index)
    {
		if (index < bannerAd.Length) {
#if UNITY_IPHONE
			 if (Application.platform == RuntimePlatform.IPhonePlayer)
			{			
				showBannerAdIOS_(index);
			}
#elif UNITY_ANDROID
	        if (Application.platform == RuntimePlatform.Android)
			{
				AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
				AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
				plugin.CallStatic("showBannerAd", activity, index);
			}
#endif
		}
    }
	
	// ------------------------------------------------
	// カスタムサイズ広告
	// ------------------------------------------------
	public void addCustomSizeAd(string appid, float x, float y, AdSize adsize, AdfurikunBannerAd.AndroidTransition transition, bool visible) {
#if UNITY_IPHONE
		 if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			switch(adsize) {
				case AdSize.BANNER:
					addAdfurikunCustomSizeAdIOS_(appid, x, y, 320.0f, 50.0f,(int) transition, visible);
					break;
				case AdSize.ICON:
					addAdfurikunCustomSizeAdIOS_(appid, x, y, 73.0f, 75.0f, (int) transition, visible);
					break;
			}
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
			switch (adsize) {
				case AdSize.BANNER:
					addCustomSizeAd(appid, x, y, 320.0f, 50.0f, transition, visible);
					break;
				case AdSize.ICON:
					addCustomSizeAd(appid, x, y, 73.0f, 75.0f, transition, visible);
					break;
			}
        }
#endif
	}
	
    public void addCustomSizeAd(string appid, float x, float y, float width, float height, AdfurikunBannerAd.AndroidTransition transition, bool visible)
    {
#if UNITY_IPHONE
		 if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			addAdfurikunCustomSizeAdIOS_(appid, x, y, width, height, (int)transition, visible);

		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("addCustomSizeAd", activity, appid, x, y, width * androidDensity + 0.5f, height * androidDensity + 0.5f, (int)transition, visible);
		}
#endif
    }

    public void hideCustomSizeAd(int index)
    {
#if UNITY_IPHONE
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			hideCustomSizeAdIOS_(index);
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("hideCustomSizeAd", activity, index);
		}
#endif
    }

    public void showCustomSizeAd(int index)
    {
#if UNITY_IPHONE
		 if (Application.platform == RuntimePlatform.IPhonePlayer)
		{			
			showCustomSizeAdIOS_(index);
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("showCustomSizeAd", activity, index);
		}
#endif
    }
	
	// ------------------------------------------------
	// インタースティシャル広告
	// ------------------------------------------------
    public void cancelIntersAd()
    {
#if UNITY_IPHONE
#elif UNITY_ANDROID

        if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
			AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			plugin.CallStatic("cancelIntersAd", activity);
		}
#endif
    }

    public void showIntersAd(GameObject intersSrcObject, int index)
    {
		if (index < intersAd.Length && this.intersSrcObject == null) {
			this.intersSrcObject = intersSrcObject;
#if UNITY_IPHONE
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				showIntersAdIOS_(index);
			}			
#elif UNITY_ANDROID
	        if (Application.platform == RuntimePlatform.Android)
			{
				AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
				AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
				plugin.CallStatic("showIntersAd", activity, index);
			}
#endif
		}
    }

	public void IntersAdClose(string str_index)
	{
		if (this.intersSrcObject != null) {
			int index = -1;
			try {
				index = int.Parse(str_index);
			} catch {
			}
			if (index != -1) {
				this.intersSrcObject.SendMessage("AdfurikunIntersAdClose", index);
			}
			this.intersSrcObject = null;
		}
	}

	public void IntersAdCustomClose(string str_index)
	{
		if (this.intersSrcObject != null) {
			int index = -1;
			try {
				index = int.Parse(str_index);
			} catch {
			}
			if (index != -1) {
#if UNITY_IPHONE
#elif UNITY_ANDROID
				this.intersSrcObject.SendMessage("AdfurikunIntersAdCustomClose", index);
#endif
			}
			this.intersSrcObject = null;
		}
	}
	
	public void IntersAdError(string str_index)
	{
		if (this.intersSrcObject != null) {
			int index = -1;
			try {
				index = int.Parse(str_index);
			} catch {
			}
			if (index != -1) {
				this.intersSrcObject.SendMessage("AdfurikunIntersAdNotDisplayed", index);
			}
			this.intersSrcObject = null;
		}
	}
	
	public void IntersAdMaxEnd(string str_index)
	{
		if (this.intersSrcObject != null) {
			int index = -1;
			try {
				index = int.Parse(str_index);
			} catch {
			}
			if (index != -1) {
				this.intersSrcObject.SendMessage("AdfurikunIntersAdNotDisplayed", index);
			}
			this.intersSrcObject = null;
		}
	}
	
	public void IntersAdSkip(string str_index) {
		if (this.intersSrcObject != null) {
			int index = -1;
			try {
				index = int.Parse(str_index);
			} catch {
			}
			if (index != -1) {
				this.intersSrcObject.SendMessage("AdfurikunIntersAdNotDisplayed", index);
			}
			this.intersSrcObject = null;
		}
	}
	
	// ------------------------------------------------
	// Wall広告
	// ------------------------------------------------
    public void showWallAd(GameObject wallSrcObject)
    {
		if (this.wallSrcObject == null) {
			this.wallSrcObject = wallSrcObject;
#if UNITY_IPHONE
#elif UNITY_ANDROID
	        if (Application.platform == RuntimePlatform.Android)
			{
				AndroidJavaClass plugin = new AndroidJavaClass("jp.tjkapp.adfurikunsdk.AdfurikunController");
				AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
				plugin.CallStatic("showWallAd", activity);
			}
#endif
		}
    }

	public void WallAdClose()
	{
		if (this.wallSrcObject != null) {
			this.wallSrcObject.SendMessage("AdfurikunWallAdClose");
			this.wallSrcObject = null;
		}
	}
	
	public void WallAdError()
	{
		if (this.wallSrcObject != null) {
			this.wallSrcObject.SendMessage("AdfurikunWallAdNotDisplayed");
			this.wallSrcObject = null;
		}
	}


	/*--------------------------------------------*/
	// コンバージョン報告
	/*--------------------------------------------*/

	// 報告
	public static void doConversionReport( int no=1, int sleepsec=1 )
	{	
#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass( "jp.tjkapp.adfurikunsdk.AdfurikunController" );
			AndroidJavaClass player = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");

			plugin.CallStatic( "doConversionReport", activity, no, sleepsec );
		}
#endif
	}

	// レシーブ状況取得
	public static bool isReceivedRef()
	{
#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass plugin = new AndroidJavaClass( "jp.tjkapp.adfurikunsdk.AdfurikunController" );
			AndroidJavaClass player = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
			AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
			return plugin.CallStatic<bool>( "isReceivedRef", activity );
		}
		return false;
#else
		return false;
#endif
	}


}
