﻿using UnityEngine;
using System.Collections;

public class VibrateScript : MonoBehaviour {

	public static void vibrate(int msec) {

		#if UNITY_EDITOR
		Debug.Log ( "vibrate(1):");
		#elif UNITY_WEBPLAYER

		#elif UNITY_ANDROID
		AndroidJavaObject unityPlayer = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" );
		AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>( "currentActivity" );
		AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");

		vibrator.Call("vibrate", msec);

		vibrator.Dispose();
		currentActivity.Dispose();
		unityPlayer.Dispose();
		#elif UNITY_IOS
		Handheld.Vibrate();
		#endif

	}
}
