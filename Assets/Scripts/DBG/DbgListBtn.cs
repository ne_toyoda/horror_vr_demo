﻿using UnityEngine;
using System.Collections;

public class DbgListBtn : MonoBehaviour {

	public string scene_key;

	public void OnButton ()
	{
		GameData.Instance.AllInit();

		GameData.Instance.SceneString = scene_key;

		// フラグ初期化
		GameData.Instance.InitFlgData();
		GameData.Instance.InitItemData();
		GameData.Instance.SetItem = "";


		// フラグをたてる
		// データをセットする
		DBGData data = Resources.Load<DBGData>("Xls/Data/DBGData");
		foreach (DBGData.Param p in data.list) {
			if(scene_key.Equals(p.event_key)){

				for(int i=0; i<p.flg_array.Length; i++){
					GameData.Instance.SetEventFlg(p.flg_array[i]);
				}
				// アイテム
				for(int i=0; i<p.item_flg_array.Length; i++){

					if(p.item_flg_array[i].StartsWith("archive")){
						GameData.Instance.SetArchiveGet(p.item_flg_array[i]);
					} else {
						if(string.Empty != p.item_flg_array[i]){
							GameData.Instance.SetItemGet(p.item_flg_array[i]);
						}
					}
				}
			}
		}

		if(scene_key.Equals("SCENE_OP")){
			GameObject prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/OP/PREFAB_OP_NOT_VR"), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
			prefab.name = "PREFAB_OP_NOT_VR";
			prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);

			GameObject.Find("Canvas").GetComponent<DbgScript>().OnBtn();

		} else {
			GlobalObject.LoadLevelWithString("Setting", "");
		}
	}
}
