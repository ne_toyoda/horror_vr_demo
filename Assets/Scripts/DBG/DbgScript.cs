﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DbgScript : MonoBehaviour {

	[SerializeField]
	GameObject DBGObject;

	[SerializeField]
	RectTransform prefab;

	void Start () {
		SceneData sceneData = Resources.Load<SceneData>("Xls/Data/SceneData");
		Dictionary<string, SceneData.Param> sceneDic = new Dictionary<string, SceneData.Param>();
		foreach (SceneData.Param p in sceneData.list) {
			if(! sceneDic.ContainsKey(p.scene_key)){
				sceneDic.Add(p.scene_key, p);
			}
		}

		foreach (string key in sceneDic.Keys) {
			RectTransform item = GameObject.Instantiate(prefab) as RectTransform;
			item.SetParent(transform.FindChild("DBG/Scroll/Content").transform, false);
			item.GetComponent<DbgListBtn>().scene_key = key;

			Text text = item.GetComponentInChildren<Text>();
			text.text = sceneDic[key].scene_content;
		}
	}

	public void OnContinueBtn ()
	{
		GlobalObject.LoadLevelWithString("Setting", "");
	}

	public void OnBtn ()
	{
		DBGObject.SetActive(false);
	}
}
