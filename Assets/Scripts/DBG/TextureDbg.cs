﻿using UnityEngine;
using System.Collections;

public class TextureDbg : MonoBehaviour {

	public void Dbg1 ()
	{
		GameObject.Find("360View/360").BroadcastMessage ("SetSceneTextureDbg1", SendMessageOptions.DontRequireReceiver);
	}

	public void Dbg2 ()
	{
		GameObject.Find("360View/360").BroadcastMessage ("SetSceneTextureDbg2", SendMessageOptions.DontRequireReceiver);
	}

	public void Dbg3 ()
	{
		GameObject.Find("360View/360").BroadcastMessage ("SetSceneTextureDbg3", SendMessageOptions.DontRequireReceiver);
	}
}
