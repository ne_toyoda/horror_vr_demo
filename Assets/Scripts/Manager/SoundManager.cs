﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {
	static SoundManager instance = null;

	[SerializeField]
	AudioSource roomAudioSource;
	[SerializeField]
	AudioSource seAudioSource;
	[SerializeField]
	AudioSource bgmAudioSource;
	[SerializeField]
	AudioSource randomAudioSource;

	Dictionary<string, AudioClip> audioClips = new Dictionary<string, AudioClip>();

	public static SoundManager Instance {
		get { return SoundManager.instance; }
	}

	void Awake() 
	{
		if ( instance == null ) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		} else {
			Destroy(gameObject);
		}

		//リソースフォルダから全SE&BGMのファイルを読み込みセット
		audioClips  = new Dictionary<string, AudioClip> ();

		object[] seList  = Resources.LoadAll ("Audio");

		foreach (AudioClip se in seList) {
			audioClips [se.name] = se;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// 部屋ごとのBGMを再生
	/// </summary>
	public void PlayRoomBGM  (string bgmName, float vol)
	{
		if(string.Empty != bgmName){
			if (!audioClips.ContainsKey (bgmName)) {
				Debug.Log (bgmName + "という名前のBGMがありません");
				return;
			}
		}

		if(0 != vol){
			roomAudioSource.volume  = vol;
		}

		if(string.Empty != bgmName){
			roomAudioSource.clip = audioClips [bgmName] as AudioClip;
			if(GameData.Instance.IsBGM) {
				roomAudioSource.Play ();
			}
		} else {
			roomAudioSource.clip = null;
		}

		/*
		//現在BGMが流れていない時はそのまま流す
		if (!AttachBGMSource2.isPlaying) {
			_nextBGMName2 = "";
			if(string.Empty != bgmName){
				AttachBGMSource2.clip = audioClips [bgmName] as AudioClip;
			}
			if(GameData.GetInstance().IsBGM){
				AttachBGMSource2.Play ();
			}
		}
		//違うBGMが流れている時は、流れているBGMをフェードアウトさせてから次を流す。同じBGMが流れている時はスルー
		else if (AttachBGMSource2.clip.name != bgmName) {
			if(string.Empty != bgmName){
				_nextBGMName2 = bgmName;
			}
			if(string.Empty != bgmName){
				AttachBGMSource2.clip = audioClips [bgmName] as AudioClip;
			}
			if(GameData.GetInstance().IsBGM){
				AttachBGMSource2.Play ();
			}
		}
		*/
	}

	public void StopRoomBGM ()
	{
		roomAudioSource.Stop();
	}

	/// <summary>
	/// SE再生
	/// </summary>
	public void PlaySE (string seName, float vol)
	{
		// SEの設定をチェック
		if(! GameData.Instance.IsSE) return;

		seAudioSource.volume  = vol;

		if (!audioClips.ContainsKey (seName)) {
			Debug.Log (seName + "という名前のSEがありません");
			return;
		}

		seAudioSource.PlayOneShot (audioClips [seName] as AudioClip);
	}

	/// <summary>
	/// BGM再生
	/// </summary>
	public void PlayBGM (string bgmName, float vol)
	{
		if(string.Empty != bgmName){
			if (!audioClips.ContainsKey (bgmName)) {
				Debug.Log (bgmName + "という名前のBGMがありません");
				return;
			}
		}

		if(0 != vol){
			bgmAudioSource.volume  = vol;
		}

		if (!bgmAudioSource.isPlaying) {
			if(string.Empty != bgmName){
				bgmAudioSource.clip = audioClips [bgmName] as AudioClip;
			}
			if(GameData.Instance.IsBGM) {
				bgmAudioSource.Play ();
			}
		} else if (bgmAudioSource.clip.name != bgmName) {
			//違うBGMが流れている時のみを流す。同じBGMが流れている時はスルー
			bgmAudioSource.clip = audioClips [bgmName] as AudioClip;
			if(GameData.Instance.IsBGM) {
				bgmAudioSource.Play ();
			}
		}
	}

	public void StopBGM ()
	{
		bgmAudioSource.Stop();
	}

	/// <summary>
	/// ランダムサウンド
	/// </summary>
	public void PlayRandomSE (string seName, float vol)
	{
		// SEの設定をチェック
		if(! GameData.Instance.IsSE) return;

		randomAudioSource.volume  = vol;

		if (!audioClips.ContainsKey (seName)) {
			Debug.Log (seName + "という名前のSEがありません");
			return;
		}

		randomAudioSource.PlayOneShot (audioClips [seName] as AudioClip);
	}
}
