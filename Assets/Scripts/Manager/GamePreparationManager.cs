﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using UnityEngine.UI;

public class GamePreparationManager : MonoBehaviour {

	[SerializeField]
	GameObject GameStartBtnObj;
	[SerializeField]
	GameObject GameStartTextObj;
	[SerializeField]
	GameObject CountTextObj;
	[SerializeField]
	GameObject ProfileTextObj;
	[SerializeField]
	GameObject ProfileSettingObj;

	int timeRemaining = 5;

	void Start ()
	{
		GameObject.Find("AdfurikunUtility").GetComponent<AdfrikunScript>().HideAdfurikunBannerAd();

		GvrViewer.Instance.Recenter();

		foreach(Transform child in GameObject.Find("Limit360View/Limit360").transform) {
			child.gameObject.GetComponent<SphareTextureView>().SetSettingTexture();
		}

		//タイマの残り時間を描画する
		CountTextObj.GetComponent<Text>().text = timeRemaining.ToString();
		InvokeRepeating("DecreaseTimeRemaining", 1.0f, 1.0f);

		// プロファイルの設定
		/*
		BaseVRDevice device;
		device = BaseVRDevice.GetDevice();
		device.Init();
		Uri DefaultDeviceProfile = new Uri("your URL here");
		device.SetDefaultDeviceProfile(DefaultDeviceProfile);
		*/
	}

	void DecreaseTimeRemaining()
	{
		timeRemaining --;
		if(0 < timeRemaining){
			CountTextObj.GetComponent<Text>().text = timeRemaining.ToString();
		} else {
			CancelInvoke("DecreaseTimeRemaining");
			CountTextObj.SetActive(false);
			GameStartBtnObj.SetActive(true);
			GameStartTextObj.SetActive(true);
		}
	}

	public void OnStartGame ()
	{
		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/FADE_OUT_NOT_DELETE"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_OUT_AND_FADE_IN";
		prefab.transform.SetParent(GameObject.Find("GvrMain/Head/Canvas").transform, false);

		GameObject.Find("UICanvas/Button").GetComponent<Button>().interactable = false;

		Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => Application.LoadLevel("Game"));
	}

	/// <summary>
	/// プロファイルの設定ボタンを押す
	/// </summary>
	public void OnProfileSettimg ()
	{
		ProfileTextObj.SetActive(false);
		ProfileSettingObj.SetActive(true);
	}

}
