﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UniRx;

public class NotVREventManager : MonoBehaviour {

	[SerializeField]
	GameObject FullTextObj;
	[SerializeField]
	GameObject BackgroundObj;

	EventData data;
	Dictionary<int, EventData.Param>eventDic = new Dictionary<int, EventData.Param>();
	int eventNo = 0;

	void Start () {

		data = Resources.Load<EventData>("Xls/Data/EventData");

		// イベント情報をセットする
		string scene = "OP_NO_VR";
		foreach (EventData.Param p in data.list) {
			if(p.event_key.Equals(scene)){
				if(! eventDic.ContainsKey(p.no)){
					eventDic.Add(p.no, p);
				}
			}
		}

		SoundManager.Instance.PlayBGM("120", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_IN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_IN";
		prefab.transform.SetParent(transform, false);

		PlayRoomSound();
		EventController();
	}

	public void EventController ()
	{
		if(eventDic.ContainsKey(eventNo+1)){
			eventNo++;

			string eventContent = eventDic[eventNo].event_content;
			if(eventContent.Equals("FULL_TEXT")){
				// フル画面のテキスト表示
				FullTextObj.SetActive(true);

				Animator anim = FullTextObj.GetComponent<Animator>();
				anim.Play("Anim_event_text_close", 0, 0.0f);

				Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => DisplayText());
			} else if(eventContent.Equals("CHAMGE_BACKGROUND")){
				Sprite[] sprites = Resources.LoadAll<Sprite> ("Event/" + eventDic[eventNo].other);
				BackgroundObj.GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (eventDic[eventNo].content));
				EventController();
			} else if(eventContent.Equals("CHAMGE_BACKGROUND_BLACK")){
				BackgroundObj.GetComponent<Image>().sprite = null;
				BackgroundObj.GetComponent<Image>().color = new Color(0f, 0f, 0f);
				EventController();
			} else if(eventContent.Equals("FADE_OUT_AND_FADE_IN")){
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT_AND_FADE_IN"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.name = "PREFAB_FADE_OUT_AND_FADE_IN";
				prefab.transform.SetParent(transform.transform, false);

				Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => EventController());
			} else if(eventContent.Equals("LOAD_VR")){
				FullTextObj.SetActive(true);

				Animator anim = FullTextObj.GetComponent<Animator>();
				anim.Play("Anim_event_text_close", 0, 0.0f);

				Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => DisplayText());

				Observable.Timer(TimeSpan.FromSeconds(3.0f)).Subscribe( _ => {
					TrasmitScene();
				});
			}
		}
	}

	void TrasmitScene ()
	{
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/FADE_OUT_NOT_DELETE"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "FADE_OUT_NOT_DELETE";
		prefab.transform.SetParent(transform, false);

		Observable.Timer(TimeSpan.FromSeconds(3.0f)).Subscribe( _ =>
			{
				GameData.Instance.SceneString = "SCENE_OP_2";
				GlobalObject.LoadLevelWithString("Setting", "");
			});
	}

	/// <summary>
	/// フル画面のテキストを表示する
	/// </summary>
	void DisplayText ()
	{
		Animator anim = FullTextObj.GetComponent<Animator>();
		anim.Play("Anim_event_text_open", 0, 0.0f);

		FullTextObj.GetComponent<Text>().text = eventDic[eventNo].content;
	}

	/// <summary>
	/// イベントナンバーを進める
	/// </summary>
	public void UpdateEventNo ()
	{
		if(eventDic.ContainsKey(eventNo)){
			if(eventDic[eventNo].event_content.Equals("TEXT") ||
				eventDic[eventNo].event_content.Equals("FULL_TEXT")){
				EventController();
			}
		}
	}

	void PlayRoomSound ()
	{
		Dictionary<string, SceneData.Param> sceneDic = GameData.Instance.GetSceneDic();

		// 環境音設定
		if(string.Empty != sceneDic[GameData.Instance.SceneString].bgm_name){
			// 環境音がある場合は再生
			SoundManager.Instance.PlayRoomBGM (
				sceneDic[GameData.Instance.SceneString].bgm_name,
				sceneDic[GameData.Instance.SceneString].bgm_vol
			);
		} else {
			// 環境音がない場合はストップする
			/*
			SoundController.GetInstance().StopEnvironmentalBGM();
			SoundController.GetInstance().ResetEnvironmentalBGM();
			*/
		}
	}
}
