﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using System.Collections.Generic;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SetBackground();

		SoundManager.Instance.PlayRoomBGM("", 1.0f);
		SoundManager.Instance.StopBGM();
		SoundManager.Instance.StopRoomBGM();


		UpdateMagatamaDisplay();

		string arg = GlobalObject.getInstance().StringParam;
		if(arg.Equals("Purchase")){
			// 共有のポップアップを出す
			GameObject.Find("Canvas/Guide").SetActive(false);
			Animator _anim = GameObject.Find("Canvas").GetComponent<Animator>();
			_anim.Stop();
		} else {
			Observable.Timer(TimeSpan.FromSeconds(4f)).Subscribe( _ => EndGuide());
		}


	}

	/// <summary>
	/// ご案内を非表示にする
	/// </summary>
	void EndGuide ()
	{
		GameObject fade = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT_AND_FADE_IN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		fade.name = "PREFAB_FADE_OUT_AND_FADE_IN";
		fade.transform.SetParent(GameObject.Find("Canvas").transform, false);

		Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => 
			{
				SoundManager.Instance.PlayBGM("83", 1);

				GameObject.Find("Canvas/Guide").SetActive(false);
				Animator _anim = GameObject.Find("Canvas").GetComponent<Animator>();
				_anim.Play("AnimC_title_main", 0, 0.0f);

				string arg = GlobalObject.getInstance().StringParam;
				if(arg.Equals("Ending")){
					// 共有のポップアップを出す
					DisplayAchievementSharePopup();
				}

				// 広告表示
				GameObject.Find("AdfurikunUtility").GetComponent<AdfrikunScript>().ShowAdfurikunBannerAd();
				GameObject.Find("AdfurikunUtility").GetComponent<AdfrikunScript>().ShowAdfurikunIntersAd();
			});
	}

	/// <summary>
	/// タイトル360度テクスチャの設定
	/// </summary>
	public void SetBackground ()
	{
		Dictionary<string, SceneData.Param> scene_dic = GameData.Instance.GetSceneDic();

		if(! GameData.Instance.GetNowPlaceKey().Equals("Lobby") && ! GameData.Instance.GetNowPlaceKey().Equals("StaffRoom") &&
			! GameData.Instance.GetNowPlaceKey().Equals("HospitalRoom") && ! GameData.Instance.GetNowPlaceKey().Equals("ExaminationRoom") &&
			! GameData.Instance.GetNowPlaceKey().Equals("OpeRoom") && ! GameData.Instance.GetNowPlaceKey().Equals("Mortuary") &&
			! GameData.Instance.GetNowPlaceKey().Equals("WC") ){

			// 2D
			GameObject.Find("360View").SetActive(false);
		} else {
			// テクスチャ設定
			GameObject.Find("Canvas/Background").SetActive(false);
			GameObject.Find("360View/360").BroadcastMessage ("SetTitleTexture", SendMessageOptions.DontRequireReceiver);
		}
	}

	/// <summary>
	/// ゲーム開始ボタン
	/// </summary>
	public void OnStartGame ()
	{
		SoundManager.Instance.StopBGM();
		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject fade = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/FADE_OUT_NOT_DELETE"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		fade.name = "FADE_OUT_NOT_DELETE";
		fade.transform.SetParent(GameObject.Find("Canvas").transform, false);

		Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe( _ => 
		{
				if(GameData.Instance.SceneString.Equals("SCENE_OP")){
					GlobalObject.LoadLevelWithString("OP", "");
				} else {
					GlobalObject.LoadLevelWithString("Setting", "");
				}
		});
	}

	/// <summary>
	/// 新しく始めるボタン
	/// </summary>
	public void OnNewGame ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Title/NewGamePop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "NewGamePop";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}

	/// <summary>
	/// 共有ボタン
	/// </summary>
	public void OnShare ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Title/SharePop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "SharePop";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}

	/// <summary>
	/// ポータルサイト
	/// </summary>
	public void OnPotalSite ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		Application.OpenURL("https://zzyzx.jp/portal/public/index/?from=horrorVR");
	}

	/// <summary>
	/// 達成率の共有
	/// </summary>
	void DisplayAchievementSharePopup ()
	{
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Title/AchievementSharePop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "AchievementSharePop";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}

	/// <summary>
	/// コンフィグボタン
	/// </summary>
	public void OnConfing ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Title/TitleConfingPop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "TitleConfingPop";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}

	/// <summary>
	/// コンフィグボタン
	/// </summary>
	public void OnMovie ()
	{
		GameObject.Find("UnityAds").GetComponent<UnityAdsScript>().ShowRewardVideo();
	}

	/// <summary>
	/// マガタマの個数を更新
	/// </summary>
	public void UpdateMagatamaDisplay() 
	{
		// マガタマの個数をセット
		GameObject.Find("Canvas/Magatama/Text").GetComponent<Text>().text = GameData.Instance.GetAllMagatama().ToString();
	}

	/// <summary>
	/// マガタマボタンを押す
	/// </summary>
	public void OnShop ()
	{
		GlobalObject.LoadLevelWithString("Purchase", "Top");
	}
}
