﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UniRx;
using UnityEngine.UI;

public class EventManager : MonoBehaviour {

	[SerializeField]
	GameObject BackgroundSphareObj;
	[SerializeField]
	GameObject LimitBackgroundSphareObj;
	[SerializeField]
	GameObject MsgBoxObj;
	[SerializeField]
	GameObject ItemGetObj;
	[SerializeField]
	GameObject FixingImgCanvas;
	[SerializeField]
	GameObject TextCanvas;
	[SerializeField]
	GameObject FrontUICanvas;
	[SerializeField]
	GameObject BackBtn;

	[SerializeField]
	GameObject ReticleObj;
	[SerializeField]
	GameObject ViewReticleObj;

	[SerializeField]
	GameObject BlindFoldObj;

	// イベント詳細データ
	EventData data;

	// 現在再生されているイベントの情報
	string eventKey = ""; 
	int eventNo = 1;
	Dictionary<int, EventData.Param>eventDic = new Dictionary<int, EventData.Param>();

	IDisposable disposer = null;

	public string backBtnEvent = "";

	float randumEventSec = -1;

	void Start () {
		DeployGateSDK.Install ();

		data = Resources.Load<EventData>("Xls/Data/EventData");
		if(null == data){
			Debug.Log ( "イベント詳細データでエラー中" );
			return;
		}

		GameData.Instance.CanPushUIFlg = true;
		GameData.Instance.CanTouchFlg = true;

		BackgroundSphareObj.SetActive(true);
		LimitBackgroundSphareObj.SetActive(false);

		Set360Background();
		PlayRoomSound();
		SceneStatEventController();
		if(! GameData.Instance.SceneString.Equals("SCENE_MORTUARY_DOOR_PUZZLE")){
			DisplayRoomName();
		}
		RandomEvent();

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_IN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_IN";
		prefab.transform.SetParent(FrontUICanvas.transform, false);
	}

	void Update () {
		if(0 < randumEventSec){
			randumEventSec -= Time.deltaTime;
			if (randumEventSec <= 0.0) {
				randumEventSec = -1;

				StartBlindfold();
			}
		}
	}

	/// <summary>
	/// だーれだイベント
	/// </summary>
	void StartBlindfold ()
	{
		// イベント中じゃないときだけ
		if(! GameData.Instance.CanPushUIFlg) return;
		if(! GameData.Instance.CanTouchFlg) return;

		// アイテム画面も開いてないとき
		if(GameObject.Find("UICanvas").GetComponent<MenuObject>().IsOpenMenu()) return;

		GameData.Instance.CanTouchFlg = false;
		GameData.Instance.CanPushUIFlg = false;

		MsgBoxObj.SetActive(false);

		GameObject prefab = Instantiate (
			BlindFoldObj, 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_BLINDFOLD";
		prefab.transform.SetParent(GameObject.Find("GvrMain/Head/EventCanvas").transform, false);
		GameData.Instance.SetEventFlg("FLG_BLINDFOLD");

	}

	/// <summary>
	/// 部屋球体の背景画像を設定
	/// </summary>
	public void Set360Background (bool isTouchObj=true)
	{
		Dictionary<string, SceneData.Param> sceneDic = GameData.Instance.GetSceneDic();

		if(string.Empty != GameData.Instance.SceneString &&
			! sceneDic.ContainsKey(GameData.Instance.SceneString)){
			Debug.Log ( "シーンがありません：" +  GameData.Instance.SceneString);
			return;
		}

		Debug.Log ( "シーン：" +  GameData.Instance.SceneString);

		// カメラ固定か360度viewか制限360度か
		if(sceneDic[GameData.Instance.SceneString].sphereKind.Equals("limit360")){
			// 制限360度
			BackgroundSphareObj.SetActive(false);
			LimitBackgroundSphareObj.SetActive(true);
			SetLimit360(sceneDic[GameData.Instance.SceneString].texture_name, "Reset", false);
		} else {
			BackgroundSphareObj.SetActive(true);
			LimitBackgroundSphareObj.SetActive(false);

			// テクスチャ設定
			BackgroundSphareObj.BroadcastMessage ("SetSceneTexture", SendMessageOptions.DontRequireReceiver);

			// タッチオブジェクト作成
			if(string.Empty != sceneDic[GameData.Instance.SceneString].touch_prefab && isTouchObj){
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/Touch/" + 
						sceneDic[GameData.Instance.SceneString].touch_prefab), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.transform.SetParent(GameObject.Find("360").transform, false);
				prefab.name = sceneDic[GameData.Instance.SceneString].touch_prefab;
			}

			// スタッフルームとトイレは電気がちかちかする
			if(GameData.Instance.GetNowPlaceKey().Equals("StaffRoom") || GameData.Instance.GetNowPlaceKey().Equals("WC")){
				// 扉はなしで
				//if(! GameData.GetInstance().SceneString.Equals("SCENE_WC_DOOR_OPEN") &&
				//! GameData.GetInstance().SceneString.Equals("SCENE_WC_DOOR")){
				GameObject light = Instantiate (
					Resources.Load<GameObject> ("Prefab/Common/PREFAB_LIGHT_FRASH"), 
						Vector3.zero, 
						Quaternion.identity
					) as GameObject;
				light.name = "PREFAB_LIGHT_FRASH";
				light.transform.SetParent(FixingImgCanvas.transform, false);
				//}
			}
		}
	}

	/// <summary>
	/// 部屋名をだす
	/// </summary>
	void DisplayRoomName ()
	{
		string scene = GameData.Instance.GetNowPlaceKey();
		if(scene.Equals("OP") || scene.Equals("ED_BEST") || scene.Equals("ED_BAD"))	return;

		// アナリティクス
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendScene(scene);

		Dictionary<string, SceneData.Param> sceneDic = GameData.Instance.GetSceneDic();

		GameObject nameObj = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_ROOM_NAME"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		nameObj.transform.SetParent(TextCanvas.transform, false);
		nameObj.transform.FindChild("Text").GetComponent<Text>().text = 
			sceneDic[GameData.Instance.SceneString].room_name;
	}

	/// <summary>
	/// 部屋のBGMをならす
	/// </summary>
	void PlayRoomSound ()
	{
		Dictionary<string, SceneData.Param> sceneDic = GameData.Instance.GetSceneDic();

		// 環境音設定
		if(string.Empty != sceneDic[GameData.Instance.SceneString].bgm_name){
			// 環境音がある場合は再生
			SoundManager.Instance.PlayRoomBGM (
				sceneDic[GameData.Instance.SceneString].bgm_name,
				sceneDic[GameData.Instance.SceneString].bgm_vol
			);
		} else {
			// 環境音がない場合はストップする
			SoundManager.Instance.StopRoomBGM();
		}
	}

	void RandomEvent ()
	{
		// 秒数を決める
		if(GameData.Instance.GetNowPlaceKey().Equals("WC") || GameData.Instance.GetNowPlaceKey().Equals("OpeRoom")){
			randumEventSec = UnityEngine.Random.Range(60f, 120f);
		}
	}

	/// <summary>
	/// シーン開始直後にイベントを開始するものをチェック
	/// </summary>
	void SceneStatEventController() 
	{
		Dictionary<string, SceneData.Param> scene_dic = GameData.Instance.GetSceneDic();
		if(null != scene_dic[GameData.Instance.SceneString].event_key &&
			string.Empty != scene_dic[GameData.Instance.SceneString].event_key){
			Debug.Log ( "シーンイベント:" +  scene_dic[GameData.Instance.SceneString].event_key);

			SetEvent(scene_dic[GameData.Instance.SceneString].event_key);
			StartEvent();

			/*
			if(GameData.Instance.SceneString.Equals("SCENE_MORTUARY_DOOR_PUZZLE")){
				SetEvent(scene_dic[GameData.Instance.SceneString].event_key);
				StartEvent();
			} else {
				Observable.Timer(TimeSpan.FromSeconds(2f)).Subscribe( _ => {
					SetEvent(scene_dic[GameData.Instance.SceneString].event_key);
					StartEvent();
				});
			}
			*/
		}
	}

	/// <summary>
	/// スタートするイベント情報をセットする
	/// </summary>
	/// /// <remarks>
	/// </remarks>
	public void SetEvent (string key, bool touch=false)
	{
		// イベントを設定
		eventKey  = key;

		if (key.StartsWith("TEXT_")) {
			// ただのテキストの場合
			string msg = GameData.Instance.GetTextEventContext(key);
			if (msg != string.Empty){
				DisplayMessage(msg, "");
			} else {
				Debug.LogError ("テキストメッセージが表示されていないようです");
			}
			return;
		} else if (key.StartsWith("MAGATAMA_")) {
			// マガタマの場合
			//GetMagatama();
			return;
		} else if (key.Equals("EVENT_MAP") || key.Equals("EVENT_MORTUARY_MAP")){
			// マップを表示する
			GameObject.Find("UICanvas").GetComponent<MenuObject>().OnMap();
			return;
		} else if (key.Equals("EVENT_MORTUARY_MAP")){
			// マップを表示する
			GameObject.Find("UICanvas").GetComponent<MenuObject>().OnMap();
			return;
		}

		// タッチ不可能の場合はタッチ不可能にする
		GameData.Instance.CanTouchFlg = false;

		// フォーカスOFF
		SetReticle(false);

		/*
		if(! touch){
			GameData.Instance.CanTouchFlg = false;
		}
		*/
		GameData.Instance.CanPushUIFlg = false;

		//////////////
		// イベント前の初期化
		////////////
		// メッセージ表示時間を初期化
		//msg_remaining_sec = -1;
		// メッセージを一度非表示に
		//Message.SetActive(false);
		// バックボタンも消す
		//FullImageBackBtn.SetActive(false);
		// イベント配列初期化
		eventDic.Clear();
		eventNo = 1;

		// イベント詳細を配列にセットする
		Debug.Log("eventKey:" +eventKey);
		foreach (EventData.Param p in data.list) {
			if(p.event_key.Equals(key)){
				if(! eventDic.ContainsKey(p.no)){
					eventDic.Add(p.no, p);
				} else {
					Debug.LogError ( "イベントエクセルエラー:" + key + ":" + p.no);
				}
			}
		}
	}

	/// <summary>
	/// イベントが終了するまで各イベントを実行する
	/// </summary>
	public void StartEvent ()
	{
		// event_keyがない時はreturn
		if(string.Empty == eventKey)	return;

		if(! eventDic.ContainsKey(eventNo)){
			Debug.LogError ( "イベントナンバーエラー:" + eventNo + ":" + eventKey);
			return;
		}
		string eventKind = eventDic[eventNo].event_content;

		if (eventKind.StartsWith("TEXT_"))	return;
		else if (eventKind.Equals("EVENT_MAP") || eventKind.Equals("EVENT_MORTUARY_MAP"))	return;

		// メッセージボックスは一度非表示にする
		MsgBoxObj.SetActive(false);
			
		if(define.EventSetLimit360.Equals(eventKind)){
			// 制限360度画面を表示
			SetLimit360(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventSetLimit360Part.Equals(eventKind)){
			// 制限360度画面を一部表示
			SetLimit360Part(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventPlaySE.Equals(eventKind)){
			// SEをならす
			PlaySE(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventCreatePrefab.Equals(eventKind)){
			// Prefabを作成する
			CreatePrefab(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventDestroyPrefab.Equals(eventKind)){
			// Prefabを削除する
			DestroyPrefab(eventDic[eventNo].content, eventDic[eventNo].other );
		} else if(define.EventJumpEventNoWhenHasEventFlg.Equals(eventKind)){
			// フラグがあればイベントジャンプ
			JumpEventNOWhenHasEventFlg(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventActiveEventFlg.Equals(eventKind)){
			// イベントフラグON
			ActiveEventFlg(eventDic[eventNo].content);
		} else if(define.EventEndEvent.Equals(eventKind)){
			// イベントフラグON
			EndEvent(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventFadeOut.Equals(eventKind)){
			// Fadeout
			FadeOut();
		} else if(define.EventFadeIn.Equals(eventKind)){
			// FadeIn
			FadeIn();
		} else if(define.EventJumpEvent.Equals(eventKind)){
			// イベントジャンプ
			JumpEvent(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventJumpEventWhenUseItem.Equals(eventKind)){
			// アイテムを使ってる場合はイベントジャンプ
			JumpEventWhenUseItem(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventDisplayMessage.Equals(eventKind)){
			// テキスト表示
			DisplayMessage(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventGetItem.Equals(eventKind)){
			// アイテム取得
			GetItem(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventPlayBGM.Equals(eventKind)){
			// BGMをならす
			PlayBGM(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventStopBGM.Equals(eventKind)){
			// BGMをとめる
			StopBGM();
		} else if(define.EventLoadScene.Equals(eventKind)){
			// Unityのシーンをロードする
			LoadScene(eventDic[eventNo].content);
		} else if(define.EventChange360Immediate.Equals(eventKind)){
			// 360度画像を即時変更
			Change360Immediate(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventChangeScene.Equals(eventKind)){
			// シーンをかえる
			ChangeScene(eventDic[eventNo].content);
		} else if(define.EventDisabledUIButton.Equals(eventKind)){
			// UIボタンを無効にする
			DisabledUIButton();
		} else if(define.EventEnabledUIButton.Equals(eventKind)){
			// UIボタンを有効にする
			EnabledUIButton();
		} else if(define.EventDelay.Equals(eventKind)){
			// ディレイ
			PutDelay(eventDic[eventNo].content);
		} else if(define.EventDeleteItem.Equals(eventKind)){
			// アイテムを削除する
			DeleteItem(eventDic[eventNo].content);
		} else if(define.EventCreateSpatitalSE.Equals(eventKind)){
			// 3DサウンドPrefabを作成する
			CreateSpatitalSE(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventVibrate.Equals(eventKind)){
			// 振動
			StartVibrate(eventDic[eventNo].content);
		} else if(define.EventBackBtnEvent.Equals(eventKind)){
			// バックボタンにイベントを登録する
			SetBackBtnEvent(eventDic[eventNo].content);
		} else if(define.EventChange360ViewFade.Equals(eventKind)){
			// 360度画像をフェードでかえる
			Change360ViewFade(eventDic[eventNo].content, eventDic[eventNo].other);
		} else if(define.EventDisabledReticle.Equals(eventKind)){
			// レティクルをOFFにする
			DisabledReticle();
		} else if(define.EventDisplayMapPop.Equals(eventKind)){
			// マップを表示する
			DisplayMapPop();
		} else {
			Debug.Log("EVE_NAME nothing:" + eventKind);
		}
			
	}

	/// <summary>
	/// BGMを再生
	/// </summary>
	void PlaySERepeat (string bgm_name, string volume)
	{
		/*
		float vol = 0f;
		if (float.TryParse(volume, out vol)){
			if(0 == vol) vol = 1f;
			SoundController.GetInstance().PlayBGMWithVolume (bgm_name, vol);
		} else {
			SoundController.GetInstance().PlayBGMWithVolume (bgm_name, 1);
		}
		*/
		UpdateEventNo();
	}

	/// <summary>
	/// イベント終了
	/// </summary>
	/// /// <remarks>
	/// //isEndPrefab:	プレハブを残す
	/// //isEndFullImg:	フルサイズ画像をそのまま表示する
	/// isNotBackBtn:	バックボタンを表示しない
	/// isNotTouch:		タッチを解禁しない
	/// isNotUI:		UIタッチを解除しない
	/// isDisabledMap:		マップを非表示にする
	/// isNotCamera:	カメラを元に戻さない
	/// isNext:			次のイベントに進む
	/// </remarks>
	public void EndEvent (string all_pram, string event_name="")
	{
		// メッセージ終了
		//Message.SetActive(false);
		//msg_remaining_sec = -1;	

		// ゲットしたアイテム表示終了
		//Item.SetActive(false);

		// フォーカスON
		SetReticle(true);

		// 特定条件付加するものをリストにする
		string[] pramArray = all_pram.ToString().Split(',');
		List<string> pramList = new List<string>();
		pramList.AddRange(pramArray);


		// 制限360度から戻る
		if(pramList.Contains("BackBtnEvent")){
			GameData.Instance.CanTouchFlg = true;
			OnBackBtn();
		}

		// 作ったプレハブを残すか
		if(! pramList.Contains("isNotEndPrefab")){
			FixingImgCanvas.gameObject.BroadcastMessage ("DestroyPrefab", SendMessageOptions.DontRequireReceiver);
			LimitBackgroundSphareObj.transform.FindChild("EventPrefab").gameObject.BroadcastMessage ("DestroyPrefab", SendMessageOptions.DontRequireReceiver);
		}

		// バックボタンを表示しないか
		if(pramList.Contains("isNotBackBtn")){
			BackBtn.SetActive(false);
		} else {
			if(null != GameObject.Find("Limit360View")){
				BackBtn.SetActive(true);
			} else {
				BackBtn.SetActive(false);
			}

			Dictionary<string, SceneData.Param> sceneDic = GameData.Instance.GetSceneDic();
			if(sceneDic[GameData.Instance.SceneString].sphereKind.Equals("limit360")){
				// 制限360度のシーン
				BackBtn.SetActive(false);
			}
		}

		// UIを表示できるか
		if(pramList.Contains("isDisabledMap")){
			GameData.Instance.CanPushUIFlg = false;
		} else {
			GameData.Instance.CanPushUIFlg = true;
		}

		// タッチON
		if(pramList.Contains("isNotTouch")){
			GameData.Instance.CanTouchFlg = false;

			if(null != GameObject.Find("360View/360")){
				GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
			}
			if(null != GameObject.Find("Limit360View/EventPrefab")){
				GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
			}

		} else {
			if (eventKey.Equals("EVENT_MAP") || eventKey.Equals("EVENT_MORTUARY_MAP"))	return;

			GameData.Instance.CanTouchFlg = true;
			if(null != GameObject.Find("360View/360")){
				GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
			}
			if(null != GameObject.Find("Limit360View/EventPrefab")){
				GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
			}

		}


		// 次のイベントに進む
		if(pramList.Contains("isNext")){
			UpdateEventNo();
		} else {
		
		}

		/*
		// 一枚絵を終了させる
		if(pram_list.Contains("isNotEndFullImg")){
		} else {
			// 消す
			FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = false;
		}
		*/
		/*
		Dictionary<string, SceneData.Param> scene_dic = GameData.GetInstance().GetSceneDic();

		// バックボタンをだす
		if(pram_list.Contains("isDspBackBtn")){

			// 2D画像が表示されていない場合はバックボタンをださない
			// 扉などの2Dマップでもださない
			if(FullImgObj.transform.FindChild("Image").GetComponent<Image>().isActiveAndEnabled &&
				0 != scene_dic[GameData.GetInstance().SceneString].camera_fov){
				// 戻るボタン表示
				FullImageBackBtn.SetActive(true);
				FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
			} else if(! FullImgObj.transform.FindChild("Image").GetComponent<Image>().isActiveAndEnabled &&
				0 == scene_dic[GameData.GetInstance().SceneString].camera_fov){
				// 戻るボタン表示
				FullImageBackBtn.SetActive(true);
			} else if(0 == scene_dic[GameData.GetInstance().SceneString].camera_fov){
				FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
			} else {
				if(pram_list.Contains("isNotEndFullImg")){
					FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
				} else {
					FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = false;
				}
				//FullImgObj.gameObject.BroadcastMessage ("EndEventInDestoryPrefabOnly", SendMessageOptions.DontRequireReceiver);
			}
		} else {
			FullImageBackBtn.SetActive(false);

			if(0 == scene_dic[GameData.GetInstance().SceneString].camera_fov){
				FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
			} else if(pram_list.Contains("isDspImg")){
				FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
			} else {
				if(pram_list.Contains("isNotEndFullImg")){
					FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = true;
				} else {
					FullImgObj.transform.FindChild("Image").GetComponent<Image>().enabled = false;
				}
			}
			//FullImgObj.gameObject.BroadcastMessage ("EndEventInDestoryPrefabOnly", SendMessageOptions.DontRequireReceiver);
		}

		// タッチ解禁
		if(pram_list.Contains("isNotTouch")){
			GameData.GetInstance().CanTouchFlg = false;
		} else {
			GameData.GetInstance().CanTouchFlg = true;

		}



		// マップ移動禁止
		if(pram_list.Contains("isNotMap")){
			GameObject.Find("Canvas").GetComponent<UIController>().ChangeMapBtn(false);
		}

		// マップ移動復活
		if(pram_list.Contains("isMap")){
			GameObject.Find("Canvas").GetComponent<UIController>().ChangeMapBtn(true);
		}

		// カメラを元に戻す
		if(! pram_list.Contains("isNotCamera")){
			// 2Dの場合があるのでチェック
			if(null != GameObject.Find("CameraObject/Main Camera")){
				GameObject.Find("CameraObject/Main Camera").GetComponent<Camera>().fieldOfView = 
					scene_dic[GameData.GetInstance().SceneString].camera_fov;
				GameObject obj = GameObject.Find("CameraObject");
				obj.transform.eulerAngles = new Vector3(
					0,
					obj.transform.eulerAngles.y,
					obj.transform.eulerAngles.z);
			}
		}

		// イベント発生させる
		if(null != event_name && string.Empty != event_name){
			GameObject.Find("Manager").GetComponent<EventController>().SetEvent(event_name);
			GameObject.Find("Manager").GetComponent<EventController>().StartEvent();
			return;
		}

		// 次のイベントに進む
		if(pram_list.Contains("isNext")){
			SetEventNo();
		} else {
			event_key  = "";
		}
		*/
	}

	/// <summary>
	/// 制限360度球体に画像を設定
	/// </summary>
	public void SetLimit360 (string folderName, string reset, bool isEvent=true)
	{


		GvrHead head = GameObject.Find("GvrMain/Head/Main Camera").GetComponent<StereoController>().Head;


		BackgroundSphareObj.SetActive(false);
		LimitBackgroundSphareObj.SetActive(true);

		// カメラをリセットするかチェック
		if(string.Empty == reset){
			// しない場合はカメラの正面に画像がくるようにする
			LimitBackgroundSphareObj.transform.SetLocalEulerAnglesY(
				head.transform.eulerAngles.y);
		} else {
			GvrViewer.Instance.Recenter();
		}

		foreach(Transform child in LimitBackgroundSphareObj.transform.FindChild("Limit360").transform) {
			child.gameObject.GetComponent<SphareTextureView>().SetLimitSceneTexture(folderName);
		}

		if(isEvent)	UpdateEventNo();
	}

	/// <summary>
	/// 制限360度球体に画像を一部設定
	/// </summary>
	public void SetLimit360Part (string folderName, string prefabNo)
	{
		GvrHead head = GameObject.Find("GvrMain/Head/Main Camera").GetComponent<StereoController>().Head;


		BackgroundSphareObj.SetActive(false);
		LimitBackgroundSphareObj.SetActive(true);

		// prefab配列を作成
		string[] prefabArray = prefabNo.ToString().Split(',');
		for(int i=0; i<prefabArray.Length; i++){
			LimitBackgroundSphareObj.transform.FindChild("Limit360/" + prefabArray[i]).gameObject.GetComponent<SphareTextureView>().SetLimitSceneTexture(folderName);
		}

		UpdateEventNo();
	}

	/// <summary>
	/// イベントプレハブ作成
	/// </summary>
	void CreatePrefab (string prefabName, string parentInfo)
	{
		Debug.Log("create:" + prefabName);

		// プレハブの場所を取得
		string placeName = GameData.Instance.GetNowPlaceKey();

		GameObject prefab = null;
		if(null != Resources.Load<GameObject> ("Prefab/" + placeName + "/" + prefabName)){
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/" + placeName + "/" + prefabName), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
		} else if(null != Resources.Load<GameObject> ("Prefab/Common/" + prefabName)){
			// 場所依存ではないものはCommon直下におく
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/Common/" + prefabName), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
		} /*else {
			Debug.LogError("作成するプレハブがありません:"+ "Prefab/" + place_name + "/" + prefab_name +
				" EVENT:" + event_key + " NO:" + event_context_no);
			SetEventNo();
			return;
		}
		*/

		// Prefab名を設定
		prefab.name = prefabName;

		// 親オブジェクトを決める
		if(parentInfo == string.Empty){
			// 指定場所がない場合は制限球体配下におく
			prefab.transform.SetParent(LimitBackgroundSphareObj.transform.FindChild("EventPrefab").transform, false);
		} else if(parentInfo.Equals("Fix")){
			// Fixの場合はcanvas配下におく
			prefab.transform.SetParent(FixingImgCanvas.transform, false);
			prefab.transform.SetSiblingIndex(0);
		} else if(parentInfo.Equals("FixReticle")){
			// Fixの場合はcanvas配下におく
			prefab.transform.SetParent(FixingImgCanvas.transform, false);
			prefab.transform.SetSiblingIndex(0);
		} else if(parentInfo.Equals("Text")){
			// Fixの場合はcanvas配下におく
			prefab.transform.SetParent(TextCanvas.transform, false);
			prefab.transform.SetSiblingIndex(0);
		} else if(parentInfo.Equals("First")){
			// せんとうにおく
		} else if(parentInfo.Equals("Canvas")){
			// Canvas配下におく
			prefab.transform.SetParent(LimitBackgroundSphareObj.transform.FindChild("Canvas").transform, false);
		} else if(parentInfo.Equals("UI")){
			// Canvas配下におく
			prefab.transform.SetParent(GameObject.Find("UIGvrMain/Head/UIFirstCanvas").transform, false);
		} else if(parentInfo.Equals("Menu")){
			// メニューCanvasにおく
			prefab.transform.SetParent(GameObject.Find("UICanvas").transform, false);
		} else if(parentInfo.Equals("Room")){
			// メニューCanvasにおく
			prefab.transform.SetParent(BackgroundSphareObj.transform.FindChild("EventPrefab").transform, false);
		} else if(parentInfo.Equals("MenuText")){
			// メニューCanvasにおく
			prefab.transform.SetParent(GameObject.Find("UICanvas").transform, false);
			GameObject.Find("UICanvas").GetComponent<MenuObject>().SetTutorialPopupTransform(prefab);
		} else if(parentInfo.Equals("MenuTextLimit")){
			// メニューCanvasにおく
			prefab.transform.SetParent(GameObject.Find("UICanvas").transform, false);
			GameObject.Find("UICanvas").GetComponent<MenuObject>().SetTutorialLimitPopupTransform(prefab);
		}
	}

	/// <summary>
	/// フラグがあれば指定のイベントにすすむ
	/// なければ次へ
	/// </summary>
	void JumpEventNOWhenHasEventFlg(string flgName, string strJumpEventNo)
	{
		if(GameData.Instance.GetEventFlg(flgName)){
			// 該当フラグがある場合はジャンプ
			int jumpEventNo = 0;
			if (Int32.TryParse(strJumpEventNo, out jumpEventNo)){
				// SetEventNoで+1するのでひいておく
				eventNo = jumpEventNo-1;
			} else {
				Debug.LogError("JumpEventWhenHasEventFlg のジャンプする値が変:"+ 
					" EVENT:" + eventKey + " NO:" + eventNo);
				return;
			}
		}
		UpdateEventNo();
	}

	/// <summary>
	/// 指定したPrefabを削除
	/// </summary>
	void DestroyPrefab(string prefabName, string parentInfo)
	{
		if(parentInfo.Equals("Canvas")){
			// Canvas配下のオブジェクトを消す
			LimitBackgroundSphareObj.transform.FindChild("Canvas/" +prefabName);
		} else if(parentInfo.Equals("Fix")){
			if(null != FixingImgCanvas.transform.FindChild(prefabName)){
				Destroy(FixingImgCanvas.transform.FindChild(prefabName).gameObject);
			}
		} else if (null != LimitBackgroundSphareObj.transform.FindChild("EventPrefab/" + prefabName)){
			Destroy(LimitBackgroundSphareObj.transform.FindChild("EventPrefab/" + prefabName).gameObject);
		} else if(parentInfo.Equals("Room")){
			// メニューCanvasにおく
			if(null != BackgroundSphareObj.transform.FindChild("EventPrefab/" + prefabName)){
				Destroy(BackgroundSphareObj.transform.FindChild("EventPrefab/" + prefabName).gameObject);
			}
		}
		UpdateEventNo();
	}

	/// <summary>
	/// イベントフラグをONにする
	/// </summary>
	void ActiveEventFlg(string eventName)
	{
		// イベントフラグセット
		GameData.Instance.SetEventFlg(eventName);

		// フラグによってタッチイベントON/OFF切り替わるものを切り替える
		BackgroundSphareObj.transform.FindChild("360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);

		UpdateEventNo();
	}

	/// <summary>
	/// イベントジャンプする
	/// </summary>
	void JumpEvent(string eventName, string strtEventNo)
	{
		int jumpEventNo = 0;
		if (Int32.TryParse(strtEventNo, out jumpEventNo)){
			SetEvent(eventName);
			eventNo = jumpEventNo;
			StartEvent();
		} else {
			Debug.LogError("JumpEvent の値が変:"+ 
				" EVENT:" + eventKey + " NO:" + eventNo);
		}
	}

	/// <summary>
	/// メッセージを表示
	/// </summary>
	void DisplayMessage (string msg, string prm)
	{
		if(prm.Equals("not")){
			// prm:notの場合はメッセージを消さない
			//msg_remaining_sec = 100;
		} else {
			// メッセージの表示秒数を設定
			//msg_remaining_sec = define.SHOW_TEXT_SECOND;
		}

		// メッセージボックスをONにしてテキストを設定する
		MsgBoxObj.SetActive(true);
		MsgBoxObj.transform.FindChild("Text").GetComponent<Text>().text = msg;

		// 一度キャンセルしてからメッセージ表示
		if(null != disposer){
			disposer.Dispose();
		}
		bool isNextEvent = true;
		if (eventKey.StartsWith("TEXT_")) {
			isNextEvent = false;
		}

		disposer = Observable.Timer(TimeSpan.FromSeconds(2f)).Subscribe( _ => {
			MsgBoxObj.SetActive(false);
			if (isNextEvent) {
				UpdateEventNo();
			}
		});
	}

	public void DisplayMessageSpecialEvent (string msg)
	{
		// メッセージボックスをONにしてテキストを設定する
		MsgBoxObj.SetActive(true);
		MsgBoxObj.transform.FindChild("Text").GetComponent<Text>().text = msg;
		disposer = Observable.Timer(TimeSpan.FromSeconds(2f)).Subscribe( _ => {
			MsgBoxObj.SetActive(false);
		});
	}

	/// <summary>
	/// 黒へのフェードイン
	/// </summary>
	void FadeOut ()
	{
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_OUT";
		prefab.transform.SetParent(FrontUICanvas.transform, false);

		Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => UpdateEventNo());
	}

	/// <summary>
	/// 黒からのフェード合うと
	/// </summary>
	void FadeIn ()
	{
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_IN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_IN";
		prefab.transform.SetParent(FrontUICanvas.transform, false);

		UpdateEventNo();
		//Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => UpdateEventNo());
	}

	/// <summary>
	/// アイテムを使ってればイベントジャンプ
	/// </summary>
	void JumpEventWhenUseItem (string itemName, string jumpEvent)
	{
		if(GameData.Instance.SetItem.Equals(itemName)){
			int jumpEventNo = 0;
			SetEvent(jumpEvent);
			eventNo = 1;
			StartEvent();
		} else {
			UpdateEventNo();
		}
	}

	/// <summary>
	/// アイテムゲット & アイテム画像を表示
	/// </summary>
	void GetItem (string itemName, string text)
	{
		ItemGetObj.SetActive(true);
		Sprite[] sprites = {};

		if(itemName.StartsWith("item_")){
			// アイテムを取得
			GameData.Instance.SetItemGet(itemName);
			SoundManager.Instance.PlaySE ("113", 1.0f);

			sprites = Resources.LoadAll<Sprite> ("item/ItemAtlas");
		} else if(itemName.StartsWith("archive_")){
			// ファイル系を取得
			GameData.Instance.SetArchiveGet(itemName);
			SoundManager.Instance.PlaySE ("25", 1f);
			sprites = Resources.LoadAll<Sprite> ("item/ArchiveAtlas");

		} else {
			// たぶんマップ
			SoundManager.Instance.PlaySE ("25", 1f);
			sprites = Resources.LoadAll<Sprite> ("item/ArchiveAtlas");
		}

		// メッセージボックスをONにしてテキストを設定する
		//MsgBoxObj.SetActive(true);
		ItemGetObj.transform.FindChild("MsgBox/Text").GetComponent<Text>().text = text;

		Debug.Log("itemName:" +itemName);
		ItemGetObj.transform.FindChild("Item").GetComponent<Image>().sprite = 
			System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (itemName));

		// 終わったら消す
		Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe( _ => 
			{
				ItemGetObj.SetActive(false);
				MsgBoxObj.SetActive(false);
				UpdateEventNo();
			}
		);

		/*
		bool isAll = true;
		Dictionary<string, bool> dic = GameData.GetInstance().GetHasArchiveDic();
		foreach(string key in dic.Keys){
			if(! dic[key]){
				if(key.Equals("archive_magazine_clip_2") || key.Equals("archive_magazine_clip_3"))
				{
					continue;
				} else {
					isAll = false;
					break;
				}
			}
		}
		if(isAll){
			SubmitAchievementInNotEvent("18");
		}
		*/
	}

	/// <summary>
	/// SEをならす
	/// </summary>
	void PlaySE(string seName, string strVolume)
	{
		float volume = 0f;
		if (float.TryParse(strVolume, out volume)){
			if(0 == volume) volume = 1f;
			SoundManager.Instance.PlaySE (seName, volume);
		} else {
			SoundManager.Instance.PlaySE (seName, 1f);
		}

		UpdateEventNo();
	}

	/// <summary>
	/// BGMをならす
	/// </summary>
	void PlayBGM (string bgmName, string strVolume)
	{
		Debug.Log("bgmName:" + bgmName);

		float volume = 0f;
		if (float.TryParse(strVolume, out volume)){
			if(0 == volume) volume = 1f;
			SoundManager.Instance.PlayBGM (bgmName, volume);
		} else {
			SoundManager.Instance.PlayBGM (bgmName, 1);
		}

		UpdateEventNo();
	}

	/// <summary>
	/// BGMをとめる
	/// </summary>
	void StopBGM ()
	{
		SoundManager.Instance.StopBGM ();
		UpdateEventNo();
	}

	/// <summary>
	/// Unityのシーンをロードする
	/// </summary>
	void LoadScene (string sceneName)
	{
		GameData.Instance.SceneString = sceneName;
		GlobalObject.LoadLevelWithString("Dummy", "");
		//GlobalObject.LoadLevelWithString(define.SceneNameGame, define.SceneOptGame);
	}

	/// <summary>
	/// 360度Viewをすぐに切り替える
	/// </summary>
	void Change360Immediate (string changePrefabNo, string str_scene_no)
	{
		/*
		int scene_no = 0;
		if(! Int32.TryParse(str_scene_no, out scene_no)){
			Debug.LogError("360度viewチェンジのシーン設定おかしい:"+ str_scene_no +
				" EVENT:" + event_key + " NO:" + event_context_no);
			SetEventNo();
			return;
		}

		string[] change_prefab_array = all_chabge_prefab_no.ToString().Split(',');
		List<string> change_prefab_list = new List<string>();
		change_prefab_list.AddRange(change_prefab_array);

		// 設定されている変更prefabナンバーのテクスチャを変更する
		foreach(string prefab_no in change_prefab_list){
			if(null != GameObject.Find("View/Sphere/polygon_" + prefab_no)){
				GameObject.Find("View/Sphere/polygon_" + prefab_no).GetComponent<SphareTextureView>().ChangeSceneTextureImmediate(scene_no);
			}
		}
		SetEventNo();
		*/
	}

	/// <summary>
	/// シーン変更
	/// </summary>
	void ChangeScene(string scene)
	{
		GameData.Instance.SceneString = scene;
		// タッチオブジェクトの切り替え
		// フラグによってタッチイベントON/OFF切り替わるものを切り替える
		BackgroundSphareObj.transform.FindChild("360").BroadcastMessage ("SetTouchEventInScene", SendMessageOptions.DontRequireReceiver);

		// 360度画像変更
		BackgroundSphareObj.transform.FindChild("360").BroadcastMessage ("SetSceneTexture", SendMessageOptions.DontRequireReceiver);

		UpdateEventNo();
	}

	/// <summary>
	/// UIボタンを無効にする
	/// </summary>
	void DisabledUIButton ()
	{
		GameData.Instance.CanPushUIFlg = false;
		UpdateEventNo();
	}

	/// <summary>
	/// UIボタンを有効にする
	/// </summary>
	void EnabledUIButton ()
	{
		GameData.Instance.CanPushUIFlg = true;
		UpdateEventNo();
	}

	/// <summary>
	/// ディレイ
	/// </summary>
	void PutDelay (string strDelaySec)
	{
		float delaySec;
		if(float.TryParse(strDelaySec, out delaySec)){
			Observable.Timer(TimeSpan.FromSeconds(delaySec)).Subscribe( _ => UpdateEventNo());
		} else {
			Debug.LogError("ディレイの時間がfloatにできません:"+ delaySec + " EVENT:" + eventKey + " NO:" + eventNo);
			UpdateEventNo();
		}
	}

	/// <summary>
	/// アイテムを削除する
	/// </summary>
	void DeleteItem (string item_name)
	{
		GameData.Instance.DeleteItem(item_name);

		// 使用中アイテム更新
		GameData.Instance.SetItem = "";
		GameObject.Find("UICanvas").GetComponent<MenuObject>().SetUseItemIcon();

		UpdateEventNo();
	}

	/// <summary>
	/// 3Dサウンドプレハブ作成
	/// </summary>
	void CreateSpatitalSE (string prefabName, string parentInfo)
	{
		// プレハブの場所を取得
		string placeName = GameData.Instance.GetNowPlaceKey();

		GameObject prefab = null;

		Debug.Log("Prefab/" + placeName + "/" + prefabName);
		if(null != Resources.Load<GameObject> ("Prefab/" + placeName + "/" + prefabName)){
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/" + placeName + "/" + prefabName), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
		} /*else if(null != Resources.Load<GameObject> ("Prefab/Common/" + prefab_name)){
			// 場所依存ではないものはCommon直下におく
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/Common/" + prefab_name), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
		} else {
			Debug.LogError("作成するプレハブがありません:"+ "Prefab/" + place_name + "/" + prefab_name +
				" EVENT:" + event_key + " NO:" + event_context_no);
			SetEventNo();
			return;
		}
		*/

		// Prefab名を設定
		prefab.name = prefabName;

		// 親オブジェクトを決める
		if(parentInfo == string.Empty){
			// 指定場所がない場合は制限球体配下におく
			prefab.transform.SetParent(LimitBackgroundSphareObj.transform.FindChild("EventPrefab").transform, false);
		} else if(parentInfo.Equals("Limit360View")){
			prefab.transform.SetParent(LimitBackgroundSphareObj.transform.FindChild("Limit360").transform, false);
		}

		UpdateEventNo();
	}

	/// <summary>
	/// 振動
	/// </summary>
	void StartVibrate(string strSec)
	{
		if(! GameData.Instance.IsVIBRATE){
			UpdateEventNo();
			return;
		}

		int sec = 0;
		if (Int32.TryParse(strSec, out sec)){
			#if UNITY_EDITOR || UNITY_WEBPLAYER
			UpdateEventNo();
			#else
			UpdateEventNo();
			Handheld.Vibrate();
			VibrateScript.vibrate(sec);
			#endif
		} else {
			Debug.LogError("StartVibrate の値が変:"+ 
				" EVENT:" + eventKey + " NO:" + eventNo);
		}
	}

	/// <summary>
	/// 360度ViewをFadeできりかえる
	/// </summary>
	void Change360ViewFade (string changePrefabNos, string strSceneNo)
	{
		int sceneNo = 0;
		if(! Int32.TryParse(strSceneNo, out sceneNo)){
			UpdateEventNo();
			return;
		}

		string[] change_prefab_array = changePrefabNos.ToString().Split(',');
		List<string> change_prefab_list = new List<string>();
		change_prefab_list.AddRange(change_prefab_array);

		// 設定されている変更prefabナンバーのテクスチャを変更する
		foreach(string prefab_no in change_prefab_list){
			if(null != GameObject.Find("360View/360/" + prefab_no)){
				GameObject.Find("360View/360/" + prefab_no).GetComponent<SphareTextureView>().ChangeSceneTextureFade(sceneNo);
			}
		}
		UpdateEventNo();
	}

	/// <summary>
	/// バックボタンにイベントを登録する
	/// </summary>
	void SetBackBtnEvent (string eventName)
	{
		backBtnEvent = eventName;
		UpdateEventNo();
	}

	/// <summary>
	/// マップポップアップを表示
	/// </summary>
	void DisplayMapPop ()
	{
		GameObject.Find("UICanvas").GetComponent<MenuObject>().OnMap();
		UpdateEventNo();
	}

	/// <summary>
	/// レティクルをOFFにする
	/// </summary>
	void DisabledReticle ()
	{
		SetReticle(false);
		UpdateEventNo();
	}

	/// <summary>
	/// バックボタンを押下
	/// </summary>
	public void SetReticle(bool isReticle)
	{
		if(isReticle){
			ReticleObj.GetComponent<MeshRenderer>().enabled = true;
			ViewReticleObj.SetActive(true);
		} else {
			ReticleObj.GetComponent<MeshRenderer>().enabled = false;
			ViewReticleObj.SetActive(false);
		}
	}

	/// <summary>
	/// ヒントポップを出す
	/// </summary>
	public void DisplayHintPop()
	{
		GameObject popup = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_HINT_POPUP"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		popup.transform.SetParent(GameObject.Find("GvrMain/Head/TextCanvas").transform, false);
	}

	public void SetEDLimit360 ()
	{
		// 制限360度
		BackgroundSphareObj.SetActive(false);
		LimitBackgroundSphareObj.SetActive(true);
		GvrHead head = GameObject.Find("GvrMain/Head/Main Camera").GetComponent<StereoController>().Head;
		GvrViewer.Instance.Recenter();
		foreach(Transform child in LimitBackgroundSphareObj.transform.FindChild("Limit360").transform) {
			child.gameObject.GetComponent<SphareTextureView>().SetLimitEDSceneTexture();
		}
	}

	/// <summary>
	/// バックボタンを押下
	/// </summary>
	public void OnBackBtn ()
	{
		if(! GameData.Instance.CanTouchFlg) return;

		FixingImgCanvas.gameObject.BroadcastMessage ("DestroyPrefab", SendMessageOptions.DontRequireReceiver);
		LimitBackgroundSphareObj.transform.FindChild("EventPrefab").gameObject.BroadcastMessage ("DestroyPrefab", SendMessageOptions.DontRequireReceiver);

		BackgroundSphareObj.SetActive(true);
		LimitBackgroundSphareObj.SetActive(false);

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}

		// 360度画像変更
		BackgroundSphareObj.transform.FindChild("360").BroadcastMessage ("SetSceneTexture", SendMessageOptions.DontRequireReceiver);


		// バックボタンでイベントが発生するもの
		if(string.Empty !=  backBtnEvent){
			SetEvent(backBtnEvent);
			StartEvent();
			backBtnEvent = "";
		}
	}

	/// <summary>
	/// イベントナンバーを進める
	/// </summary>
	public void UpdateEventNo ()
	{
		eventNo++;
		StartEvent();
	}
}
