﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UniRx;
using System;

public class PurchaseManager : MonoBehaviour {

	string signature = "";
	string arg;

	// Use this for initialization
	void Start () {
		arg = GlobalObject.getInstance().StringParam;

		#if UNITY_IPHONE
		IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
		IOSInAppPurchaseManager.OnTransactionComplete += OnTransactionComplete;

		IOSInAppPurchaseManager.Instance.LoadStore();
		#elif UNITY_ANDROID
		AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
		AndroidInAppPurchaseManager.ActionProductConsumed += OnProductConsumed;
		AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;

		AndroidInAppPurchaseManager.Client.Connect();

		//Debug.Log ("key:" + AndroidNativeSettings.Instance.base64EncodedPublicKey);
		//AndroidInAppPurchaseManager.Instance.LoadStore(base64EncodedPublicKey);
		#endif

		// マガタマの個数表示
		GameObject.Find("Canvas/Magatama/Text").GetComponent<Text>().text = "×" + GameData.Instance.GetAllMagatama();

		// 広告
		GameObject.Find("AdfurikunUtility").GetComponent<AdfrikunScript>().ShowAdfurikunBannerAd();
	}


#if UNITY_ANDROID
	private void OnProductPurchased(BillingResult result) {
		if(result.IsSuccess) {
			OnProcessingPurchasedProduct (result.Purchase);

			if(signature.Equals(result.Purchase.Signature))	return;

			signature = result.Purchase.Signature;

			/// サーバーに送信する
			Client.Instance.CreateGooglePurchaseData(
				result.Purchase.SKU,
				result.Purchase.OrderId,
				result.Purchase.Signature,
				result.Purchase.OriginalJson,
				CreatePurchaseDataSuccess);
		} 
	}

	private static void OnProcessingPurchasedProduct(GooglePurchaseTemplate purchase) {
		AndroidInAppPurchaseManager.Client.Consume(purchase.SKU);
	}

	private static void OnProcessingConsumeProduct(GooglePurchaseTemplate purchase) {

	}

	static private void OnProductConsumed(BillingResult result){
		/*
		if(result.isSuccess) {
			Debug.Log("OnProductConsumed");
		} else {
			Debug.Log("OnProductConsumed:");
			//AndroidMessage.Create("Product Cousume Failed", result.response.ToString() + " " + result.message);
		}
		*/
	}

	static private void OnBillingConnected (BillingResult result)
	{
		AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;

		if(result.IsSuccess) {
			//Store connection is Successful. Next we loading product and customer purchasing details
			AndroidInAppPurchaseManager.Client.RetrieveProducDetails();
			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProductsFinised;
		} 
			
	}

	static private void OnRetrieveProductsFinised(BillingResult result) {
		AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProductsFinised;

		if(result.IsSuccess) {
			foreach(GoogleProductTemplate tpl in AndroidInAppPurchaseManager.Client.Inventory.Products) {
				//AndroidInAppPurchaseManager.Instance.Consume (p.SKU);
				SetItemPriceAndroid(tpl.SKU, tpl.Price);
			}
		} else {
			AndroidMessage.Create("Connection Response", result.Response.ToString() + " " + result.Message);
		}
	}

	static void SetItemPriceAndroid (string tpl, float price)
	{
		Debug.Log ("Loaded product id: " + tpl);
		Debug.Log ("Loaded price: " + price);

		// _区切りで分割して配列に格納する
		if ("mueitou_vr_magatama_5" == tpl) {
			GameObject.Find("Canvas/Shop/1/Price").GetComponent<Text>().text = "¥ " + price.ToString();
		} else if ("mueitou_vr_magatama_11" == tpl) {
			GameObject.Find("Canvas/Shop/2/Price").GetComponent<Text>().text = "¥ " + price.ToString();
		} else if ("mueitou_vr_magatama_18" == tpl) {
			GameObject.Find("Canvas/Shop/3/Price").GetComponent<Text>().text = "¥ " + price.ToString();
		} else if ("mueitou_vr_magatama_28" == tpl) {
			GameObject.Find("Canvas/Shop/4/Price").GetComponent<Text>().text = "¥ " + price.ToString();
		} else if ("mueitou_vr_magatama_40" == tpl) {
			GameObject.Find("Canvas/Shop/5/Price").GetComponent<Text>().text = "¥ " + price.ToString();
		}
	}
#endif

#if UNITY_IPHONE
	static void OnStoreKitInitComplete (SA.Common.Models.Result result)
	{
		IOSInAppPurchaseManager.OnStoreKitInitComplete -= OnStoreKitInitComplete;

		if (result.IsSucceeded) {
			foreach(IOSProductTemplate tpl in IOSInAppPurchaseManager.Instance.Products) {
				SetItemPrice(tpl);
			}
		} else {
			Debug.Log("Failed in init Stoke Kit :(");
		}
	}

	static void SetItemPrice (IOSProductTemplate tpl)
	{
		// 直指定…
		// _区切りで分割して配列に格納する
		if ("mueitou_vr_magatama_5" == tpl.Id) {
			GameObject.Find("Canvas/Shop/1/Price").GetComponent<Text>().text = tpl.LocalizedPrice;
		} else if ("mueitou_vr_magatama_11" == tpl.Id) {
			GameObject.Find("Canvas/Shop/2/Price").GetComponent<Text>().text = tpl.LocalizedPrice;
		} else if ("mueitou_vr_magatama_18" == tpl.Id) {
			GameObject.Find("Canvas/Shop/3/Price").GetComponent<Text>().text = tpl.LocalizedPrice;
		} else if ("mueitou_vr_magatama_28" == tpl.Id) {
			GameObject.Find("Canvas/Shop/4/Price").GetComponent<Text>().text = tpl.LocalizedPrice;
		} else if ("mueitou_vr_magatama_40" == tpl.Id) {
			GameObject.Find("Canvas/Shop/5/Price").GetComponent<Text>().text = tpl.LocalizedPrice;
		}
	}

	void OnTransactionComplete (IOSStoreKitResult response)
	{
		switch (response.State) {
		case InAppPurchaseState.Purchased:

			if(signature.Equals(response.Receipt))	return;

			signature = response.Receipt;

			Client.Instance.CreateAppPurchaseData(response.ProductIdentifier,
				response.Receipt, 
				CreatePurchaseDataSuccess);

			break;
		case InAppPurchaseState.Restored:
			break;
		case InAppPurchaseState.Deferred:
			break;
		case InAppPurchaseState.Failed:
			break;
		default:
			break;
		}
	}
#endif

	/// <summary>
	/// マガタマ購入ボタン押下
	/// </summary>
	public void StartPurchase (int no)
	{
		string product_id = "";
		if (1 == no) {
			product_id = "mueitou_vr_magatama_5";
		} else if (2 == no) {
			product_id = "mueitou_vr_magatama_11";
		} else if (3 == no) {
			product_id = "mueitou_vr_magatama_18";
		} else if (4 == no) {
			product_id = "mueitou_vr_magatama_28";
		} else if (5 == no) {
			product_id = "mueitou_vr_magatama_40";
		}

		if (!PlayerPrefs.HasKey ("user_id")) {
			// 初めての場合はアカウント作成
			Client.Instance.CreateAccount (Client.Instance.CreateAccountSuccess);
			// エラーポップ
			return;
		}

		// 課金処理開始
		#if UNITY_IPHONE
		IOSInAppPurchaseManager.Instance.BuyProduct(product_id);
		#elif UNITY_ANDROID
		AndroidInAppPurchaseManager.Instance.Purchase(product_id);
		#endif
	}

	/// <summary>
	/// マガタマ購入後のサーバー通信OK
	/// </summary>
	void CreatePurchaseDataSuccess (IDictionary response)
	{
		if ((string)response ["status"] == "OK") {
			string item_type = (string)response ["item_type"];

			if ("mueitou_vr_magatama_5" == item_type) {
				GameData.Instance.AddPayMagatama(5);
			} else if ("mueitou_vr_magatama_11" == item_type) {
				GameData.Instance.AddPayMagatama(11);
			} else if ("mueitou_vr_magatama_18" == item_type) {
				GameData.Instance.AddPayMagatama(18);
			} else if ("mueitou_vr_magatama_28" == item_type) {
				GameData.Instance.AddPayMagatama(28);
			} else if ("mueitou_vr_magatama_40" == item_type) {
				GameData.Instance.AddPayMagatama(40);
			}

			#if UNITY_ANDROID
			//OnProcessingConsumeProduct (item_type);
			#endif

			if(GameData.Instance.GetPayMagatama() != int.Parse ((string)response["magatama"])){
				// サーバーと数が違うときはサーバーに合わせる
				GameData.Instance.SetPayMagatama(int.Parse ((string)response["magatama"]));
			}

			// マガタマ個数更新
			GameObject.Find("Canvas/Magatama/Text").GetComponent<Text>().text = "×" + GameData.Instance.GetAllMagatama();
		} else {
			//Debug.Log("NGNGNGNGN!!!");
		}
	}

	/// <summary>
	/// 戻るボタン
	/// </summary>
	public void OnBack ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		if(arg.Equals("Top")){
			GlobalObject.LoadLevelWithString("Top", "Purchase");
		} else {
			GlobalObject.LoadLevelWithString("Setting", "");
		}

	}
}
