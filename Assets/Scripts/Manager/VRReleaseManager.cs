﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using UnityEngine.UI;

public class VRReleaseManager : MonoBehaviour {

	[SerializeField]
	GameObject CountTextObj;

	int timeRemaining = 10;

	void Start ()
	{
		GvrViewer.Instance.Recenter();

		foreach(Transform child in GameObject.Find("Limit360View/Limit360").transform) {
			child.gameObject.GetComponent<SphareTextureView>().SetSettingTexture();
		}

		//タイマの残り時間を描画する
		CountTextObj.GetComponent<Text>().text = timeRemaining.ToString();
		InvokeRepeating("DecreaseTimeRemaining", 1.0f, 1.0f);

		//Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => Application.LoadLevel("Game"));
	}

	void DecreaseTimeRemaining()
	{
		timeRemaining --;
		if(0 < timeRemaining){
			CountTextObj.GetComponent<Text>().text = timeRemaining.ToString();
		} else {
			CancelInvoke("DecreaseTimeRemaining");
			string arg = GlobalObject.getInstance().StringParam;
			if(arg.Equals("Purchase")){
				Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => GlobalObject.LoadLevelWithString("Purchase", ""));
			} else {
				Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => GlobalObject.LoadLevelWithString("Top", "Ending"));
			}
		}
	}
}
