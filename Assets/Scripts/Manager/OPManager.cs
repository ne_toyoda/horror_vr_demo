﻿using UnityEngine;
using System.Collections;

public class OPManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// 広告非表示
		GameObject.Find("AdfurikunUtility").GetComponent<AdfrikunScript>().HideAdfurikunBannerAd();

		// オブジェクトを作成する
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/OP/PREFAB_OP_NOT_VR"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_OP_NOT_VR";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}
}
