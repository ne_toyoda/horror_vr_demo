﻿using UnityEngine;
using System.Collections;

public class AnalyticsManager : MonoBehaviour {
	GoogleAnalyticsV3 analytics;


	void Start()
	{
		analytics = GetComponent<GoogleAnalyticsV3>();
	}

	public void SendAdsPlay (string kind)
	{
		if(null == analytics)	return;

		string eventName = "";
		if(kind.Equals("Hint")){
			eventName = "Hint Ads";
		} else if(kind.Equals("Popup")){
			eventName = "Popup Ads";
		} else if(kind.Equals("Header")){
			eventName = "Header Ads";
		}

		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("MovieAds")
			.SetEventAction("PlayAds")
			.SetEventLabel(eventName)
			.SetEventValue(1));
	}

	public void SendMagatamaEvent (string select_no)
	{
		if(null == analytics)	return;

		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("Hint")
			.SetEventAction("Release")
			.SetEventLabel("No:" + select_no)
			.SetEventValue(1));
	}

	public void SendEventFlg (int no)
	{
		if(null == analytics)	return;

		string eventName = "";

		if(0==no){	eventName = "0_EVENT_OP";
		}else if(1==no){	eventName = "1 EVENT_GET_STAFFROOM_KEY";
		}else if(2==no){	eventName = "2 EVENT_OPEN_STAFFROOM_DOOR";
		}else if(3==no){	eventName = "3 EVENT_GET_MAP";
		}else if(4==no){	eventName = "4 EVENT_OPEN_EXAMINATIONROOM_DOOR";
		}else if(5==no){	eventName = "5 EVENT_GET_BLOODPACK";
		}else if(6==no){	eventName = "6 EVENT_OPEN_HOSPITALROOM_DOOR";
		}else if(7==no){	eventName = "7 EVENT_DESTROY_HOSPITALROOM_VASE";
		}else if(8==no){	eventName = "8 EVENT_LISTEN_NURSE_CALL";
		}else if(9==no){	eventName = "9 EVENT_GET_BATTERY";
		}else if(10==no){	eventName = "10 EVENT_LIGHT_ON";
		}else if(11==no){	eventName = "11 EVENT_GET_NEEDLE";
		}else if(12==no){	eventName = "12 EVENT_SUTURE_ARM";
		}else if(13==no){	eventName = "13 EVENT_GET_CAR_KEY";
		}else if(14==no){	eventName = "14 EVENT_CLEAR_BOX_KEY";
		}else if(15==no){	eventName = "15 EVENT_CLEAR_BLOCK_PUZZLE";
		}else if(16==no){	eventName = "16 EVENT_HELP_FRIEND";
		}else if(17==no){	eventName = "17 EVENT_END_BAD";
		}else if(18==no){	eventName = "18 EVENT_END_BEST";
		}
		
		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("Event")
			.SetEventAction("Clear")
			.SetEventLabel(eventName)
			.SetEventValue(1));
	}

	public void SendGameCenterAchievement (string no)
	{
		if(null == analytics)	return;
		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("GameCenterAchievement")
			.SetEventAction("Unlocked")
			.SetEventLabel("No:" +no)
			.SetEventValue(1));
	}

	public void SendAchievementRate (string point)
	{
		if(null == analytics)	return;
		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("AchievementRate")
			.SetEventAction("Rate")
			.SetEventLabel(point + "%")
			.SetEventValue(1));
	}

	public void SendAchievement (string no)
	{
		if(null == analytics)	return;
		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("Achievement")
			.SetEventAction("Clear")
			.SetEventLabel("No:" + no)
			.SetEventValue(1));
	}

	public void SendGameClear ()
	{
		if(null == analytics)	return;
		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("GameClear")
			.SetEventAction("ClearCount")
			.SetEventLabel(GameData.Instance.ClearCount.ToString())
			.SetEventValue(1));
	}

	public void SendShare (string sns)
	{
		if(null == analytics)	return;

		analytics.LogEvent(new EventHitBuilder()
			.SetEventCategory("SNS")
			.SetEventAction("Share")
			.SetEventLabel(sns)
			.SetEventValue(1));
	}

	public void SendScene (string scene)
	{
		if(null == analytics)	return;

		analytics.LogScreen (new AppViewHitBuilder ().SetScreenName (scene));
	}
}
