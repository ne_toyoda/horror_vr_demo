﻿using UnityEngine;
using System.Collections;
using System;

public class TimerManager : MonoBehaviour {

	[SerializeField]
	GameObject MovieBtn;

	float second = 0;
	float start_time = 0;

	float RECOVERY_TIME = 900;

	bool isCalculate = false;

	// Use this for initialization
	void Start () {
		NotActiveMovieBtn();

		start_time = Time.time;
		CalculateRocoveryTime();

		InvokeRepeating("UpdatePlayTime", 60.0f, 60.0f);
	}

	// Update is called once per frame
	void Update () {
		if(!isCalculate) return;

		if (second != -1) {
			second = (Time.time - start_time);
			if(RECOVERY_TIME < second){
				// 回復
				GameData.Instance.IsDisplayAds = true;
				GameData.Instance.IsDisplayAdsPop = true;
				second = -1;
				MovieBtn.SetActive(true);
			}
		}
	}

	public void SetRecoveryTime ()
	{
		start_time = Time.time;
		second = (Time.time - start_time);
	}

	void CalculateRocoveryTime ()
	{
		second = (float)((DateTime.Now - GameData.Instance.ADsTime).TotalSeconds);

		if(RECOVERY_TIME < second){
			// 回復
			GameData.Instance.IsDisplayAds = true;
			GameData.Instance.IsDisplayAdsPop = true;
			second = -1;
			MovieBtn.SetActive(true);
		}
		isCalculate = true;
	}

	public void NotActiveMovieBtn ()
	{
		MovieBtn.SetActive(false);
	}

	void OnApplicationPause (bool pauseStatus) {
		if (!pauseStatus) {
			isCalculate = false;
			CalculateRocoveryTime ();
		}
	}

}
