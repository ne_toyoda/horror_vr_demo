﻿using UnityEngine;
using System.Collections;

public class LoadSceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		System.GC.Collect ();
		Resources.UnloadUnusedAssets ();
		GlobalObject.LoadLevelWithString(define.SceneNameGame, define.SceneOptGame);
	}
	

}
