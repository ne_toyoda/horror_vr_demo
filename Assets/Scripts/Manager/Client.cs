﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using UniRx;
//using UnityEngine.iOS;

public enum ServerState {
	Start,
	Success,
	Error,
}
	
	

public class Client : MonoBehaviour {
	static Client instance = null;
	//static string apiUrl = "http://sapporo.zzyzx.jp/mueitou_vr/public/";
	static string apiUrl = "https://zzyzx.jp/mueitou_vr/public/";
	
    static string loginPath = "login/handshake";
	static string createAccountPath = "account/create";
	static string getUserInfoPath = "account/getUserInfo";

	static string createAppPurchaseDataPath = "account/createAppPurchaseData";
	static string createGooglePurchaseDataPath = "account/createGooglePurchaseData";

	static string setMagatamaCountPath = "account/setMagatamaCount";

	ServerState state;
	static string token;
	static byte[] key;
	static string iv = "ZXNxdWFkcmFlc3F1YWRyYQ==";
	
	RijndaelManaged rm;
	public delegate void Receiver(IDictionary response);
	
	void Awake() 
	{
		if ( instance == null ) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	public static Client Instance {
		get { return Client.instance; }
	}

	// Use this for initialization
	void Start () {

		//PlayerPrefs.DeleteAll();

		Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe( _ => 
															RetryHandShake ());
		state = ServerState.Start;
		
        Dictionary<string, string> data = new Dictionary<string, string>();
		string url = apiUrl + loginPath;
		
        HandShake(url, data);
    }
	
	public void RetryHandShake ()
	{
		state = ServerState.Start;
		
        Dictionary<string, string> data = new Dictionary<string, string>();
		string url = apiUrl + loginPath;
		
        HandShake(url, data);
	}
	
	/// <summary>
    /// サーバーコネクション状態確認
    /// </summary>
    /// <remarks>
    /// </remarks>
	public ServerState IsReady() 
	{
		return state;
	}

	/// <summary>
	/// 初回アクセスユーザー作成
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool CreateAccount(Receiver receiver) {
		int rand = UnityEngine.Random.Range(0, int.MaxValue);
		byte[] random_bytes = Encoding.UTF8.GetBytes(SystemInfo.deviceUniqueIdentifier + rand.ToString());
		MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
		byte[] login_bytes = md5.ComputeHash(random_bytes);
		string hash = "";
		for (int i = 0; i < login_bytes.Length; i++) {
			hash += System.Convert.ToString(login_bytes[i], 16).PadLeft(2, '0');
		}
		string login_hash = hash.PadLeft(32, '0');

		Dictionary<string, string> data = new Dictionary<string, string>();
		data.Add("login_hash", login_hash);

		string url = apiUrl + createAccountPath;
		Send(url, data, receiver);

		return true;
	}

	/// <summary>
	/// ユーザー情報取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool GetUserInfo(Receiver receiver) {
		Dictionary<string, string> data = new Dictionary<string, string>();

		string url = apiUrl + getUserInfoPath;
		Send(url, data, receiver);

		return true;
	}

	/// <summary>
    /// AppPurchase課金
    /// </summary>
    /// <remarks>
    /// </remarks>
	public bool CreateAppPurchaseData(string product_id, string receipt, Receiver receiver) {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("product_id", product_id);
        data.Add("receipt", receipt);
        
        string url = apiUrl + createAppPurchaseDataPath;
        Send(url, data, receiver);
		
		return true;
    }
	
	/// <summary>
    /// Google Play課金
    /// </summary>
    /// <remarks>
    /// </remarks>
	public bool CreateGooglePurchaseData(string product_id, string order_id, string signature, string signedData, 
											Receiver receiver) {
        Dictionary<string, string> data = new Dictionary<string, string>();
		data.Add("product_id", product_id);
        data.Add("order_id", order_id);
        data.Add("signature", signature);
        data.Add("signedData", signedData);
        
        string url = apiUrl + createGooglePurchaseDataPath;
        Send(url, data, receiver);
		
		return true;
    }

	/// <summary>
	/// マガタマ個数変換
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool SetMagatamaCount(string magatama, Receiver receiver) {
		Dictionary<string, string> data = new Dictionary<string, string>();
		Debug.Log("magatama:" +magatama);
		data.Add("magatama", magatama);

		string url = apiUrl + setMagatamaCountPath;
		Send(url, data, receiver);

		return true;
	}
    

	/// <summary>
    /// サーバーコネクション成功時
    /// </summary>
    /// <remarks>
    /// </remarks>
	void SuccessHandShake (RSACryptoServiceProvider rsa, string x)
	{
		IDictionary dict = Json.Deserialize(x) as Dictionary<string, object>;

		if ( dict.Contains("token") ) {
			token = (string)dict["token"];
			Debug.Log("token:"+token);
		}
		
		if ( dict.Contains("data") ) {
			byte[] encryptedData = Convert.FromBase64String((string)dict["data"]);
			byte[] decryptedData = rsa.Decrypt(encryptedData, false);
			// 共通鍵
			key = Convert.FromBase64String(Encoding.UTF8.GetString(decryptedData));

			// AES128の準備
			rm = new RijndaelManaged();
			rm.Padding = PaddingMode.PKCS7;
			rm.Mode = CipherMode.CBC;
			rm.KeySize = 128;
			rm.BlockSize = 128;
			rm.Key = key;
			rm.IV = Convert.FromBase64String(iv);

			// 準備完了
			state = ServerState.Success;
			Debug.Log("Login Success");

			// サーバーコネクションできてから画面遷移できるように
			if (!PlayerPrefs.HasKey ("user_id")) {
				// 初めての場合はアカウント作成
				CreateAccount (CreateAccountSuccess);
			} else {
				// サーバーから情報をとる
				GetUserInfo(GetUserInfoSuccess);
			}
		}
	}
	
	/// <summary>
    /// サーバーコネクション失敗時
    /// </summary>
    /// <remarks>
    /// </remarks>
	void ErrorHandShake (RSACryptoServiceProvider rsa, System.Exception ex)
	{
		rsa.PersistKeyInCsp = false;
		rsa.Clear();
		
		state = ServerState.Error;
		Debug.Log("Login Error:" + ex);
		
		if (null != GameObject.Find ("Loading")) {
			Destroy(GameObject.Find ("Loading"));
		}
	}

	/// <summary>
    /// サーバーコネクション
    /// </summary>
    /// <remarks>
    /// </remarks>
	void HandShake (string url, Dictionary<string, string> data)
	{
		// 公開鍵の用意
		try {
			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider ();
		                                       
			WWWForm form = new WWWForm ();
			form.AddField ("encrypt", "no");
			form.AddField ("public_key", ConvertPublicKey (rsa.ExportParameters (false)));
				
			Debug.Log (url + "?" + Encoding.UTF8.GetString (form.data));	
				
			ObservableWWW.Post (url, form)
				.Timeout(TimeSpan.FromSeconds(30))
        		.Subscribe (
				x => SuccessHandShake (rsa, x), // onSuccess
				ex => ErrorHandShake (rsa, ex)); // onError
		} catch (ArgumentNullException) {
			state = ServerState.Error;
		}
	}
	
	/// <summary>
    /// サーバー通信成功
    /// </summary>
    /// <remarks>
    /// </remarks>
	void SuccessSendServer (string x, Receiver receiver)
	{
		Debug.Log ("" + x);
		IDictionary dict = Json.Deserialize (x) as Dictionary<string, object>;
		IDictionary result = new Dictionary<string, object> ();
		
		if (dict.Contains ("status")) {
			// status
			result.Add ("status", (string)dict ["status"]);
			// data
			if (dict.Contains ("data")) {
				Debug.Log ("data: " + Decrypt ((string)dict ["data"]));
				IDictionary data_dict = Json.Deserialize (Decrypt ((string)dict ["data"])) as IDictionary;
				if (data_dict != null) {
					foreach (string key in data_dict.Keys) {
						result.Add (key, data_dict [key]);
					}
				}
			}
		} else {
			result.Add ("status", "NG");
		}
		
		if (null != GameObject.Find ("Loading")) {
			Destroy(GameObject.Find ("Loading"));
		}
		receiver(result);
	}
	
	/// <summary>
    /// サーバー通信失敗
    /// </summary>
    /// <remarks>
    /// </remarks>
	void ErrorSendServer (System.Exception ex, Receiver receiver)
	{
		Debug.LogError(ex);
		if (null != GameObject.Find ("Loading")) {
			Destroy(GameObject.Find ("Loading"));
		}
		
		IDictionary result = new Dictionary<string, object> ();
		result.Add ("status", "NG");
		receiver(result);
	}
	
	/// <summary>
    /// サーバー通信
    /// </summary>
    /// <remarks>
    /// </remarks>
	void Send (string url, Dictionary<string, string> data, Receiver receiver)
	{
		if (!data.ContainsKey ("login_hash") && PlayerPrefs.HasKey ("login_hash")) {
			data.Add ("login_hash", PlayerPrefs.GetString ("login_hash"));
		}
		if (!data.ContainsKey ("user_id") && PlayerPrefs.HasKey("user_id")) {
			data.Add ("user_id", PlayerPrefs.GetString ("user_id"));
		}
		
		string json = Json.Serialize(data);
		WWWForm form = new WWWForm();
		form.AddField("encrypt", "yes");
		form.AddField("token", token);
		form.AddField("data", Encrypt(json));
		Debug.Log("Send");
		Debug.Log(url + "?" + Encoding.UTF8.GetString(form.data));

		ObservableWWW.Post (url, form)
			.Timeout(TimeSpan.FromSeconds(30))
			.Subscribe (
				x => SuccessSendServer (x, receiver), // onSuccess
				ex => ErrorSendServer (ex, receiver)); // onError
	}
   
	
    public string Encrypt(string plain)
	{
		if (plain == string.Empty) {
			return string.Empty;
		}
		byte[] encrypted;
		using ( MemoryStream ms = new MemoryStream() ) {
			ICryptoTransform encoder = rm.CreateEncryptor(rm.Key, rm.IV);
			using ( CryptoStream cs = new CryptoStream(ms, encoder, CryptoStreamMode.Write) ) {
				byte[] b_plain = Encoding.UTF8.GetBytes(plain);
				cs.Write(b_plain, 0, b_plain.Length);
				cs.FlushFinalBlock();
				encrypted = ms.ToArray();
			}
		}
		return Convert.ToBase64String(encrypted);
	}
	
	public string Decrypt(string encrypted) 
	{
		if (encrypted == string.Empty) {
			return string.Empty;
		}
		using ( MemoryStream ms = new MemoryStream(Convert.FromBase64String(encrypted)) ) {
			ICryptoTransform decoder = rm.CreateDecryptor(rm.Key, rm.IV);
			using ( CryptoStream cs = new CryptoStream(ms, decoder, CryptoStreamMode.Read) ) {
				using (StreamReader sr = new StreamReader(cs) ) {
					return sr.ReadToEnd();
				}
			}
		}
	}
	
	private static string ToHexString(byte[] data) 
	{
		var buf = new StringBuilder();
		foreach (byte d in data)
		{
			buf.Append(d.ToString("x2"));
		}
		return buf.ToString();
	}
	 
	private static byte[] RSA_OID = 
	{ 0x30, 0xD, 0x6, 0x9, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x1, 0x1, 0x5, 0x0 }; // Object ID for RSA
	
	// Corresponding ASN identification bytes
	const byte INTEGER = 0x2;
	const byte SEQUENCE = 0x30;
	const byte BIT_STRING = 0x3;
	const byte OCTET_STRING = 0x4;
	
	private static string ConvertPublicKey(RSAParameters param)
	{
		List<byte> arrBinaryPublicKey = new List<byte>();
		
		arrBinaryPublicKey.InsertRange(0, param.Exponent);
		arrBinaryPublicKey.Insert(0, (byte)arrBinaryPublicKey.Count);
		arrBinaryPublicKey.Insert(0, INTEGER);
		
		arrBinaryPublicKey.InsertRange(0, param.Modulus);
		AppendLength(ref arrBinaryPublicKey, param.Modulus.Length);
		arrBinaryPublicKey.Insert(0, INTEGER);
		
		AppendLength(ref arrBinaryPublicKey, arrBinaryPublicKey.Count);
		arrBinaryPublicKey.Insert(0, SEQUENCE);
		
		arrBinaryPublicKey.Insert(0, 0x0); // Add NULL value
		
		AppendLength(ref arrBinaryPublicKey, arrBinaryPublicKey.Count);
		
		arrBinaryPublicKey.Insert(0, BIT_STRING);
		arrBinaryPublicKey.InsertRange(0, RSA_OID);
		
		AppendLength(ref arrBinaryPublicKey, arrBinaryPublicKey.Count);
		
		arrBinaryPublicKey.Insert(0, SEQUENCE);
		
		return System.Convert.ToBase64String(arrBinaryPublicKey.ToArray());
	}
	
	private static string ConvertPrivateKey(RSAParameters param)
	{
		List<byte> arrBinaryPrivateKey = new List<byte>();
		
		arrBinaryPrivateKey.InsertRange(0, param.InverseQ);
		AppendLength(ref arrBinaryPrivateKey, param.InverseQ.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.DQ);
		AppendLength(ref arrBinaryPrivateKey, param.DQ.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.DP);
		AppendLength(ref arrBinaryPrivateKey, param.DP.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.Q);
		AppendLength(ref arrBinaryPrivateKey, param.Q.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.P);
		AppendLength(ref arrBinaryPrivateKey, param.P.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.D);
		AppendLength(ref arrBinaryPrivateKey, param.D.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.Exponent);
		AppendLength(ref arrBinaryPrivateKey, param.Exponent.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.InsertRange(0, param.Modulus);
		AppendLength(ref arrBinaryPrivateKey, param.Modulus.Length);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		arrBinaryPrivateKey.Insert(0, 0x00);
		AppendLength(ref arrBinaryPrivateKey, 1);
		arrBinaryPrivateKey.Insert(0, INTEGER);
		
		AppendLength(ref arrBinaryPrivateKey, arrBinaryPrivateKey.Count);
		arrBinaryPrivateKey.Insert(0, SEQUENCE);
		
		return System.Convert.ToBase64String(arrBinaryPrivateKey.ToArray());
	}
	
	private static void AppendLength(ref List<byte> arrBinaryData, int nLen)
	{
		if (nLen <= byte.MaxValue)
		{
			arrBinaryData.Insert(0, Convert.ToByte(nLen));
			arrBinaryData.Insert(0, 0x81); //This byte means that the length fits in one byte
		}
		else
		{
			arrBinaryData.Insert(0, Convert.ToByte(nLen % (byte.MaxValue + 1)));
			arrBinaryData.Insert(0, Convert.ToByte(nLen / (byte.MaxValue + 1)));
			arrBinaryData.Insert(0, 0x82); //This byte means that the length fits in two byte
		}
		
	}

	public void CreateAccountSuccess (IDictionary response)
	{
		if ((string)response ["status"] == "NG") {

		} else {
			Debug.Log("user_id:" + (string)response["user_id"]);
			PlayerPrefs.SetString("login_hash", (string)response["login_hash"]);
			PlayerPrefs.SetString("user_id", (string)response["user_id"]);
		}
	}

	void GetUserInfoSuccess  (IDictionary response)
	{
		if ((string)response ["status"] == "NG") {
			Debug.Log("ng");
		} else {
			// アイテム情報を入れる
			if(GameData.Instance.GetPayMagatama() != int.Parse ((string)response["magatama"])){
				// サーバーと数が違うときはサーバーに合わせる
				GameData.Instance.SetPayMagatama(int.Parse ((string)response["magatama"]));
			}
		}
	}
}
