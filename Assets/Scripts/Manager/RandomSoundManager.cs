﻿using UnityEngine;
using System.Collections;

public class RandomSoundManager : MonoBehaviour {

	// 秒数を決める
	public float sec = 0;

	// Use this for initialization
	void Start () {
		SetRundomSec();
	}
	
	// Update is called once per frame
	void Update () {
		if(0<= sec){
			sec -= Time.deltaTime;
			if (sec <= 0.0) {
				sec = -1;

				if(GameData.Instance.GetNowPlaceKey().Equals("Lobby")){
					sec = 60;
				} else {
					SetRundomSec();
				}

				PlaySound();
			}
		}
	}

	void SetRundomSec ()
	{
		if(GameData.Instance.GetNowPlaceKey().Equals("Lobby")){
			sec = 30f;
			//sec = 5f;
		} else {
			sec = UnityEngine.Random.Range(20f, 40f);
		}
	}

	void PlaySound ()
	{
		if(GameData.Instance.GetNowPlaceKey().Equals("Lobby")){
			// プレハブの場所を取得
			int num = UnityEngine.Random.Range(0, 3);
			if(0==num){
				string placeName = GameData.Instance.GetNowPlaceKey();
				GameObject prefab = null;
				if(null != Resources.Load<GameObject> ("Prefab/" + placeName + "/PREFAB_SPEAKING_VOICE")){
					prefab = Instantiate (
						Resources.Load<GameObject> ("Prefab/" + placeName + "/PREFAB_SPEAKING_VOICE"), 
						Vector3.zero, 
						Quaternion.identity
					) as GameObject;
				}
			} else if(1 == num){
				SoundManager.Instance.PlayRandomSE ("217", 0.5f);
			} else {
				SoundManager.Instance.PlayRandomSE ("14", 0.3f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("StaffRoom")){
			int num = UnityEngine.Random.Range(0, 2);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("17", 0.1f);
			} else {
				SoundManager.Instance.PlayRandomSE ("225", 0.5f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("ExaminationRoom")){
			int num = UnityEngine.Random.Range(0, 3);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("145", 0.3f);
			} else if(1 == num){
				SoundManager.Instance.PlayRandomSE ("90", 0.3f);
			} else {
				SoundManager.Instance.PlayRandomSE ("146", 0.3f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("HospitalRoom")){
			int num = UnityEngine.Random.Range(0, 3);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("144", 0.3f);
			} else if(1 == num){
				SoundManager.Instance.PlayRandomSE ("37", 0.3f);
			} else {
				SoundManager.Instance.PlayRandomSE ("13", 0.3f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("WC")){
			int num = UnityEngine.Random.Range(0, 3);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("226", 0.3f);
			} else if(1 == num){
				SoundManager.Instance.PlayRandomSE ("251", 0.3f);
			} else {
				SoundManager.Instance.PlayRandomSE ("235", 0.3f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("Mortuary")){
			int num = UnityEngine.Random.Range(0, 2);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("229", 1f);
			} else {
				SoundManager.Instance.PlayRandomSE ("148", 0.3f);
			}
		} else if(GameData.Instance.GetNowPlaceKey().Equals("OpeRoom")){
			int num = UnityEngine.Random.Range(0, 2);
			if(0==num){
				SoundManager.Instance.PlayRandomSE ("234", 0.3f);
			} else {
				SoundManager.Instance.PlayRandomSE ("9", 0.3f);
			}
		}
	}
}
