﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UniRx;
using System;
using UnityEngine.UI;

public class MotuaryPuzzleScript : MonoBehaviour {

	[SerializeField]
	GameObject UpObj;
	[SerializeField]
	GameObject DownObj;
	[SerializeField]
	GameObject EndObj;
	[SerializeField]
	GameObject GameOverObj;
	[SerializeField]
	GameObject MagatamaPopObj;
	[SerializeField]
	GameObject MagatamaUsePop;
	[SerializeField]
	GameObject MagatamaLackPop;
	[SerializeField]
	GameObject DiscriptionDetailObj;

	bool isDestroy = false;

	// Use this for initialization
	void Start () {
		// レティクルの色変更
		Color redColor = new Color(1.0f,0.0f,0.0f,1.0f);
		GameObject.Find("FrontGvrMain/Head/Main Camera/GvrReticle").GetComponent<Renderer>().material.color = redColor;

		/*
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_IN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_IN";
		prefab.transform.SetParent(GameObject.Find("FrontGvrMain/Head/Canvas").transform, false);
		*/
	}
	
	public void OnStart ()
	{
		Debug.Log("OnStart");
		SoundManager.Instance.PlaySE ("300", 1.0f);

		UpObj.SetActive(true);
		DownObj.SetActive(true);
		EndObj.SetActive(true);
	}

	public void OnEnd ()
	{
		Debug.Log("OnEnd");
		SoundManager.Instance.PlaySE ("54", 1.0f);
		GameData.Instance.SetEventFlg("FLG_MORTUARY_CLEAR_PUZZLE");

		UpObj.SetActive(false);
		DownObj.SetActive(false);
		EndObj.SetActive(false);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_OUT";
		prefab.transform.SetParent(GameObject.Find("FrontGvrMain/Head/Canvas").transform, false);

		Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => {
			GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_MORTUARY_CLEAR_PUZZLE");
			GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
			Destroy(prefab);
			Destroy(gameObject);
		});
	}

	public void OnOut ()
	{
		Debug.Log("*** GAME OVER ****");
		GameOverObj.SetActive(true);

		if(!isDestroy){
			isDestroy = true;
			SoundManager.Instance.PlaySE ("301", 1.0f);

			Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => {
				GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_MORTUARY_PUZZLE_START");
				GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
				Destroy(gameObject);
			});
		}
	}

	public void OnStop ()
	{
		Debug.Log("OnStop");
		GameObject.Find("UICanvas").GetComponent<MenuObject>().OnMap();
		//GameData.Instance.SceneString = "SCENE_MORTUARY_DOOR";
		//GlobalObject.LoadLevelWithString(define.SceneNameGame, define.SceneOptGame);
	}

	public void OnMagatama ()
	{
		Debug.Log("OnMagatama");
		MagatamaPopObj.SetActive(true);

		if(GameData.Instance.GetAllMagatama() < 5 ){
			// マガタマ足りない
			MagatamaLackPop.SetActive(true);
			MagatamaUsePop.SetActive(false);	
		} else {
			MagatamaLackPop.SetActive(false);
			MagatamaUsePop.SetActive(true);	
		}
	}

	public void OnUseMagatama ()
	{
		GameData.Instance.ReduceMagatama(5);
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendMagatamaEvent("33");
		OnEnd();
	}

	public void OnPopupNo ()
	{
		MagatamaPopObj.SetActive(false);
	}

	/// <summary>
	/// 遊び方ボタンを押す
	/// </summary>
	public void OnHowToPlay ()
	{
		DiscriptionDetailObj.SetActive(true);
	}

	/// <summary>
	/// 遊び方を閉じるボタンを押す
	/// </summary>
	public void OnHowToPlayClose ()
	{
		DiscriptionDetailObj.SetActive(false);
	}
}
