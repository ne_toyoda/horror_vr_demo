﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArchiveNameScript : MonoBehaviour {

	public void InteractableBtn ()
	{
		GetComponent<Button>().interactable = false;
		transform.FindChild("New").GetComponent<Image>().enabled = false;
	}

	public void NotDisplayNewIcon()
	{
		transform.FindChild("New").GetComponent<Image>().enabled = false;
	}

	public void OnArchiveName ()
	{
		NotDisplayNewIcon();
		transform.parent.gameObject.SetActive(false);
		GameObject.Find("UICanvas/ArchivePop").GetComponent<ArchiveScript>().DisplayArchiveContent(transform.name);
	}
}
