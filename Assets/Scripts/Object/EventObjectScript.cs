﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EventObjectScript : MonoBehaviour {

	private void OnEnable()
	{
		// シーン条件でタッチON/OFFがあるもの設定
		SetTouchEventInScene();

		// フラグ条件でタッチON/OFFがあるもの設定
		SetTouchEvent();
	}

	public void OnEvent ()
	{
		Debug.Log ( "START EVENT:"+transform.name);
		if(! GameData.Instance.CanTouchFlg) return;

		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(transform.name);
		if (!(transform.name).StartsWith("TEXT_")) {
			GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
		}
	}

	public void OnEventAndParetnDestroy ()
	{
		Debug.Log ( "START EVENT:"+transform.name);
		if(! GameData.Instance.CanTouchFlg) return;

		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(transform.name);
		if (!(transform.name).StartsWith("TEXT_")) {
			GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
			Destroy(transform.parent.gameObject);
		}
	}

	public void OnEventAndDestroy ()
	{
		if(! GameData.Instance.CanTouchFlg) return;

		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(transform.name);
		if (!(transform.name).StartsWith("TEXT_")) {
			GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
			Destroy(gameObject);
		}
	}

	/// <summary>
	/// BroadcastMessageでよばれる
	/// シーンが変わったときが立ったときにタッチのON/OFFを切り替える
	/// </summary>
	bool SetTouchEventInScene ()
	{
		string[] sceneArray = GameData.Instance.GetEventScene(transform.name);

		if(null != sceneArray && string.Empty != sceneArray[0]){
			// 該当シーンかチェック
			bool isOccurScene = false;

			for(int i=0; i<sceneArray.Length; i++){
				if(GameData.Instance.SceneString.Equals(sceneArray[i])) {
					isOccurScene = true;
				}
			}

			if(isOccurScene){
				// タッチONのシーン
				GetComponent<BoxCollider>().enabled = true;
				return true;
			} else {
				Debug.Log("transform.name:" + transform.name);
				GetComponent<BoxCollider>().enabled = false;
				return false;
			}
		}
		return true;
	}

	/// <summary>
	/// BroadcastMessageでよばれる
	/// フラグが立ったときにタッチのON/OFFを切り替える
	/// </summary>
	void SetTouchEvent ()
	{
		if(!SetTouchEventInScene()){
			GetComponent<BoxCollider>().enabled = false;
			return;
		}

		// フラグがあるときのみ発生するイベント
		string flgName = GameData.Instance.GetStartEventFlg(transform.name);

		if(null != flgName && string.Empty != flgName){
			if(GameData.Instance.GetEventFlg(flgName)){
				if(null != GetComponent<BoxCollider>()){
					GetComponent<BoxCollider>().enabled = true;
				}
				if(null != GetComponent<Button>()){
					GetComponent<Button>().enabled = true;
				}
			} else {
				if(null != GetComponent<BoxCollider>()){
					GetComponent<BoxCollider>().enabled = false;
				}
				if(null != GetComponent<Button>()){
					GetComponent<Button>().enabled = false;
				}
				return;
			}
		}

		// フラグがないときのみ発生するイベント
		string noFlgName = GameData.Instance.GetStartEventNotFlg(transform.name);

		if(null != noFlgName && string.Empty != noFlgName){
			// フラグがないときのみタッチという指定がある場合
			if(GameData.Instance.GetEventFlg(noFlgName)){
				// フラグがある場合はタッチできないように
				if(null != GetComponent<BoxCollider>()){
					GetComponent<BoxCollider>().enabled = false;
				}
				if(null != GetComponent<Button>()){
					GetComponent<Button>().enabled = false;
				}
			} else {
				// フラグがない場合はタッチできるように
				if(null != GetComponent<BoxCollider>()){
					GetComponent<BoxCollider>().enabled = true;
				}
				if(null != GetComponent<Button>()){
					GetComponent<Button>().enabled = true;
				}
			}
		} else {
			if(null != GetComponent<BoxCollider>()){
				GetComponent<BoxCollider>().enabled = true;
			}
			if(null != GetComponent<Button>()){
				GetComponent<Button>().enabled = true;
			}
		}
	}

	void SetAllNonActiveTouchEvent ()
	{
		if(null != GetComponent<BoxCollider>()){
			GetComponent<BoxCollider>().enabled = false;
		}

		if(null != GetComponent<Button>()){
			GetComponent<Button>().enabled = false;
		}
	}

	public void OnMagatama ()
	{
		if(GameData.Instance.GetEventFlg(transform.name)) return;
		GetComponent<BoxCollider>().enabled = false;

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_MAGATAMA"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;

		prefab.transform.SetParent(transform, false);
		GameData.Instance.SetEventFlg(transform.name);

		SoundManager.Instance.PlaySE ("150", 0.3f);
		GameData.Instance.AddFreeMagatama(1);

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
	}
}
