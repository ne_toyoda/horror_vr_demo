﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UniRx;

public class HintScript : MonoBehaviour {
	[SerializeField]
	GameObject ListObj;
	[SerializeField]
	GameObject MagatamaPopObj;
	[SerializeField]
	GameObject MagatamaPopSelectObj;
	[SerializeField]
	GameObject ShortageMagatamaObj;
	[SerializeField]
	GameObject MagatamaPopShopObj;
	[SerializeField]
	GameObject ContentObj;
	[SerializeField]
	GameObject NextBtn;
	[SerializeField]
	GameObject PrevBtn;
	[SerializeField]
	GameObject BackGroundObj;

	string strSelectNo;
	int hintPage = 1;

	List<string> hintList = new List<string>();

	// Use this for initialization
	void Start () {
		SetHintList();
		DisplayHintList();
		SetPageBtn();
	}

	/// <summary>
	/// 表示するヒントを
	/// </summary>
	void SetHintList ()
	{
		Dictionary<string, HintData.Param> dic = GameData.Instance.GetHintDic();

		// 場所同じヒントを表示
		foreach(string key in dic.Keys){
			if(GameData.Instance.GetNowPlaceKey().Equals(dic[key].place_name) || 
				dic[key].place_name.Equals("")){

				if(! dic[key].scene_name.Equals("")){
					// 場所指定がない場合
					List<string> scene_array = new List<string>();
					scene_array.AddRange(dic[key].scene_name);

					if(! scene_array.Contains("")){
						// シーン条件があれば判定
						if(! scene_array.Contains(GameData.Instance.SceneString)){
							continue;
						}
					}
				}

				// フラグがない場合のみ表示
				if( string.Empty != dic[key].no_flg_name){
					if(GameData.Instance.GetEventFlg(dic[key].no_flg_name)) continue;
				}

				// フラグがある場合のみ表示
				if( string.Empty != dic[key].exist_flg_name){
					// アイテム使用中のみ表示
					if(dic[key].exist_flg_name.Contains("item_")){
						if(! GameData.Instance.SetItem.Equals(dic[key].exist_flg_name)) continue;
					} else {
						// フラグがある場合のみ表示
						if(! GameData.Instance.GetEventFlg(dic[key].exist_flg_name)) continue;
					}
				}

				hintList.Add(key);
			}
		}
	}

	/// <summary>
	/// ヒントを表示する
	/// </summary>
	void DisplayHintList ()
	{
		int count = 0;

		/*
		GameData.GetInstance()
			.ObserveEveryValueChanged(x => x.IsDisplayAds)
			.Subscribe(_ => 
				{
					if(GameData.GetInstance().IsDisplayAds){
						AdsObject.SetActive(true);
					} else {
						AdsObject.SetActive(false);
					}
				}).AddTo(this);
		*/

		// 場所同じヒントを表示
		for(int i=((hintPage-1)*3); i<hintPage*3; i++){
			if(hintList.Count<= i)	break;

			// ヒントのタイトル作成
			GameObject prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/UI/HintTitle"), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
			prefab.transform.SetParent(transform.FindChild("List/Content"), false);
			prefab.name = hintList[i];
			prefab.transform.SetLocalPosition(0, 0-(185*count));
			prefab.GetComponent<HintTitleScript>().SetInfo(this.gameObject, hintList[i]);

			count++;
		}

		/*
		foreach(string key in hintList){
			if( count < (hitPage-1)*3 ){
				continue;
			}

			if( hitPage*3 <= count ){
				break;
			}

			if(GameData.Instance.GetNowPlaceKey().Equals(dic[key].place_name) || 
				dic[key].place_name.Equals("")){

				if(! dic[key].scene_name.Equals("")){
					// 場所指定がない場合
					List<string> scene_array = new List<string>();
					scene_array.AddRange(dic[key].scene_name);

					if(! scene_array.Contains("")){
						// シーン条件があれば判定
						if(! scene_array.Contains(GameData.Instance.SceneString)){
							continue;
						}
					}
				}

				// フラグがない場合のみ表示
				if( string.Empty != dic[key].no_flg_name){
					if(GameData.Instance.GetEventFlg(dic[key].no_flg_name)) continue;
				}

				// フラグがある場合のみ表示
				if( string.Empty != dic[key].exist_flg_name){
					// アイテム使用中のみ表示
					if(dic[key].exist_flg_name.Contains("item_")){
						if(! GameData.Instance.SetItem.Equals(dic[key].exist_flg_name)) continue;
					} else {
						// フラグがある場合のみ表示
						if(! GameData.Instance.GetEventFlg(dic[key].exist_flg_name)) continue;
					}
				}

				// ヒントのタイトル作成
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/UI/HintTitle"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.transform.SetParent(transform.FindChild("List/Content"), false);
				prefab.name = key;
				prefab.transform.SetLocalPosition(0, 0-(185*count));
				prefab.GetComponent<HintTitleScript>().SetInfo(this.gameObject, key);

				count++;
			}
		}
		*/
	}

	/// <summary>
	/// ヒントを表示する
	/// </summary>
	public void DisplayContent (string no)
	{
		strSelectNo = no;

		ListObj.SetActive(false);
		BackGroundObj.SetActive(false);
		ContentObj.SetActive(true);

		ContentObj.transform.FindChild("Text").GetComponent<Text>().text = GameData.Instance.GetHintContent(no);
	}

	/// <summary>
	/// マガタマを使用するかポップアップを出す
	/// </summary>
	public void DisplayMagatamaPop(string no)
	{
		strSelectNo = no;

		// ポップアップを表示
		MagatamaPopObj.SetActive(true);
		ListObj.SetActive(false);

		int selectNo = 0;
		if (int.TryParse(strSelectNo, out selectNo)){
			int reduce = int.Parse(GameData.Instance.GetHintMagatama(no));

			if(GameData.Instance.GetAllMagatama() < reduce){
				MagatamaPopSelectObj.SetActive(false);
				ShortageMagatamaObj.SetActive(true);
			} else {
				MagatamaPopSelectObj.transform.FindChild("Count").GetComponent<Text>().text = 
					"×" + GameData.Instance.GetHintMagatama(no);


				MagatamaPopSelectObj.SetActive(true);
				MagatamaPopShopObj.SetActive(false);
			}
		}
	}

	public void OnReleaseHintSelect(bool flg)
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		MagatamaPopObj.SetActive(false);
		ListObj.SetActive(true);

		if(flg){
			// マガタマ減らす
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendMagatamaEvent(strSelectNo);

			// ヒント解放
			int reduceMagatama;
			if(int.TryParse(GameData.Instance.GetHintMagatama(strSelectNo), out reduceMagatama)){
				GameData.Instance.ReduceMagatama(reduceMagatama);
				GameData.Instance.SetHintRelease(strSelectNo);

				// 内容表示
				DisplayContent(strSelectNo);

				// 実績確認
				Dictionary<string, bool> dic = GameData.Instance.SetHintReleaseDic();
				foreach(bool value in dic.Values){
					if(! value) return;
				}
			}
		} else {
			MagatamaPopObj.SetActive(false);
			ListObj.SetActive(true);
		}
	}

	/// <summary>
	/// ページ送りボタンの設定
	/// </summary>
	void SetPageBtn ()
	{
		if(hintPage*3 < hintList.Count){
			NextBtn.SetActive(true);
		} else {
			NextBtn.SetActive(false);
		}

		if(1 != hintPage){
			PrevBtn.SetActive(true);
		} else {
			PrevBtn.SetActive(false);
		}

	}

	/// <summary>
	/// ページ送りボタンの設定
	/// </summary>
	public void OnNextBtn ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		hintPage++;
		foreach ( Transform n in transform.FindChild("List/Content") ){
			if(! n.gameObject.name.Equals("Close")){
				GameObject.Destroy(n.gameObject);
			}
		}
		DisplayHintList();
		SetPageBtn();
	}

	/// <summary>
	/// ページ戻りボタンの設定
	/// </summary>
	public void OnPrevBtn ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		hintPage--;
		foreach ( Transform n in transform.FindChild("List/Content") ){
			if(! n.gameObject.name.Equals("Close")){
				GameObject.Destroy(n.gameObject);
			}
		}
		DisplayHintList();
		SetPageBtn();
	}

	/// <summary>
	/// ヒントをとじる
	/// </summary>
	public void CloseHint ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		ListObj.SetActive(true);
		BackGroundObj.SetActive(true);
		ContentObj.SetActive(false);
	}

	/// <summary>
	/// アーカイブポップアップを消す
	/// </summary>
	public void ClosePopup ()
	{
		GameObject.Find("UICanvas").GetComponent<MenuObject>().ClosePopup();
	}

	/// <summary>
	/// ヒント詳細ポップアップを消す
	/// </summary>
	public void CloseHintDetailPopup ()
	{
		transform.FindChild("Content/Close").gameObject.SetActive(false);
		Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => GameObject.Find("UICanvas").GetComponent<MenuObject>().ClosePopup());
	}

	/// <summary>
	/// 購入ボタンを押す
	/// </summary>
	public void OnPurchase ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);
		GlobalObject.LoadLevelWithString("VRRelease", "Purchase");
	}
}
