﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class DestroyObject : MonoBehaviour {

	[SerializeField]
	float destroyTime;

	// Use this for initialization
	void Start () {
		Observable.Timer(TimeSpan.FromSeconds(destroyTime)).Subscribe( _ => Destroy(gameObject));
	}
}
