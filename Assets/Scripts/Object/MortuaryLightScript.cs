﻿using UnityEngine;
using System.Collections;

public class MortuaryLightScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameData.Instance.CanTouchFlg = true;
		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
	}
}
