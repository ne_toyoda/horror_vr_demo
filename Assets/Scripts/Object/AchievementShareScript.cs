﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
public class AchievementShareScript : MonoBehaviour {

	string msgTxt = "";

	void Start ()
	{
		#if UNITY_ANDROID
		Texture2D tex = Resources.Load("Twitter/image") as Texture2D;
		var bytes = tex.EncodeToJPG();
		File.WriteAllBytes(Application.persistentDataPath + "/image.jpg", bytes);
		#endif
	}

	/// <summary>
	/// 閉じる
	/// </summary>
	public void OnClose ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		Destroy(gameObject);
	}

	/// <summary>
	/// ツイッター
	/// </summary>
	public void OnTwitter ()
	{
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendShare("Twitter");
		SoundManager.Instance.PlaySE ("140", 1.0f);

		msgTxt = "【VR恐怖脱出】達成率 " + GameData.Instance.AchievementPoint +"% で脱出！\nVRゴーグルをお持ちのお連れ様、ご来院お待ちしております。\n" +
			"「改・恐怖！廃病院からの脱出：無影灯」\n" +
			"https://zzyzx.jp/mueitou_vr/hp/store.php\n" +
			"#無影灯 #ホラゲ #VR";
		#if UNITY_IPHONE
		string imagePath = Application.streamingAssetsPath + "/image.jpg";
		SWorker.SocialWorker.PostTwitter(msgTxt, null, imagePath, OnResultTwitter);
		#elif UNITY_ANDROID
		string imagePath = Application.persistentDataPath + "/image.jpg";
		SWorker.SocialWorker.PostTwitter(msgTxt, null, imagePath, OnResultTwitter);
		#endif
	}

	public void OnResultTwitter(SWorker.SocialWorkerResult res)
	{
		switch(res)
		{
		case SWorker.SocialWorkerResult.Success:
			break;
		case SWorker.SocialWorkerResult.NotAvailable:
			Application.OpenURL("http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(msgTxt));
			break;
		case SWorker.SocialWorkerResult.Error:
			break;
		}
	}

	public void PushLine ()
	{
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendShare("Line");
		SoundManager.Instance.PlaySE ("140", 1.0f);

		msgTxt = "【VR恐怖脱出】達成率 " + GameData.Instance.AchievementPoint +"% で脱出！\nVRゴーグルをお持ちのお連れ様、ご来院お待ちしております。\n" +
			"「改・恐怖！廃病院からの脱出：無影灯」\n" +
			"https://zzyzx.jp/mueitou_vr/hp/store.php";

		#if UNITY_IPHONE
		Application.OpenURL("http://line.naver.jp/R/msg/text/?" + WWW.EscapeURL(msgTxt, System.Text.Encoding.UTF8));
		#elif UNITY_ANDROID
		SWorker.SocialWorker.PostLine(msgTxt, null, null);
		#endif
	}
}
