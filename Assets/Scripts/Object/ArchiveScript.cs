﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ArchiveScript : MonoBehaviour {

	[SerializeField]
	GameObject ArchiveListObj;
	[SerializeField]
	GameObject ArchiveList;

	[SerializeField]
	GameObject NextBtn;
	[SerializeField]
	GameObject PrevBtn;

	[SerializeField]
	GameObject PageObj;

	GameObject target;
	int page = 1;

	Dictionary<string, ArchiveData.Param> dic;

	void Start () 
	{
		target = GameObject.Find("Target");
		DisplayArchiveList();
		SetPageBtn();
	}

	void Update ()
	{
		//transform.LookAt(-target.transform.forward);
		//transform.forward = target.transform.forward;
	} 

	/// <summary>
	/// アーカイブの詳細内容を表示する
	/// </summary>
	public void DisplayArchiveContent (string archiveName)
	{
		ArchiveListObj.SetActive(false);
		GameData.Instance.SetReadFile(archiveName);

		GameObject prefab = null;
		if(dic[archiveName].archive_format.Equals("TEXT")){
			// テキスト用prefab作成
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/UI/ArchiveTextDetail"), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;
		} else if(dic[archiveName].archive_format.Equals("IMAGE")){
			// 画像用prefab作成
			prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/UI/ArchiveImageDetail"), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;

			// セット
			Sprite[] sprites = Resources.LoadAll<Sprite> ("Item/ArchiveAtlas");

			prefab.transform.FindChild("Archive").GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (archiveName));

			prefab.transform.FindChild("ArchiveTextTitle").GetComponent<Text>().text = 
				"【" + dic[archiveName].archive_title + "】\n";
		}

		prefab.transform.SetParent(transform, false);
		prefab.GetComponent<ArchiveTextDetailScript>().SetText(dic[archiveName].archive_content);
	}

	/// <summary>
	/// アーカイブポップアップを消す
	/// </summary>
	public void ClosePopup ()
	{
		GameObject.Find("UICanvas").GetComponent<MenuObject>().ClosePopup();
	}

	/// <summary>
	/// アーカイブリストを表示する
	/// </summary>
	public void DisplayArchiveList ()
	{
		dic =  GameData.Instance.GetArchiveDic();

		Dictionary<int, string> numArchiveDic = new Dictionary<int, string>();
		int count = 0;
		// リスト作る
		foreach (string key  in dic.Keys) {
			numArchiveDic.Add(count, key);
			count++;
		}

		count = 0;
		// リストを5個ずつ表示
		for(int i=((page-1)*5); i<page*5; i++){
			if(dic.Count<= i)	break;

			// ヒントのタイトル作成
			GameObject prefab = Instantiate (
				Resources.Load<GameObject> ("Prefab/UI/ArchiveName"), 
				Vector3.zero, 
				Quaternion.identity
			) as GameObject;

			prefab.transform.SetParent(ArchiveListObj.transform.FindChild("List").transform, false);

			if(GameData.Instance.GetArchive(numArchiveDic[i])){
				// 持っているならタイトル表示
				prefab.GetComponentInChildren<Text>().text = dic[numArchiveDic[i]].archive_title;
				prefab.name = numArchiveDic[i];

				// 既読ならNEWアイコンをださない
				if(GameData.Instance.ReadFile(numArchiveDic[i])){
					prefab.GetComponent<ArchiveNameScript>().NotDisplayNewIcon();
				}

			} else {
				prefab.GetComponentInChildren<Text>().text = "????"; 
				prefab.GetComponent<ArchiveNameScript>().InteractableBtn();
			}

			count++;
		}
	}

	/// <summary>
	/// ページ送りボタンの設定
	/// </summary>
	public void OnNextBtn ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		page++;
		PageObj.GetComponent<Text>().text = page + "/2";

		foreach ( Transform n in ArchiveListObj.transform.FindChild("List") ){
			GameObject.Destroy(n.gameObject);
		}
		DisplayArchiveList();
		SetPageBtn();
	}

	/// <summary>
	/// ページ戻りボタンの設定
	/// </summary>
	public void OnPrevBtn ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		page--;
		PageObj.GetComponent<Text>().text = page + "/2";

		foreach ( Transform n in ArchiveListObj.transform.FindChild("List") ){
			GameObject.Destroy(n.gameObject);
		}
		DisplayArchiveList();
		SetPageBtn();
	}

	public void ActiveArchiveList ()
	{
		ArchiveListObj.SetActive(true);
		ArchiveList.SetActive(true);
	}

	/// <summary>
	/// ページ送りボタンの設定
	/// </summary>
	void SetPageBtn ()
	{
		if(page*5 < dic.Count){
			NextBtn.SetActive(true);
		} else {
			NextBtn.SetActive(false);
		}

		if(1 != page){
			PrevBtn.SetActive(true);
		} else {
			PrevBtn.SetActive(false);
		}

	}
}
