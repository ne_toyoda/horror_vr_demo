﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class TutorialScript : MonoBehaviour {

	[SerializeField]
	GameObject Tutorial1Obj;

	[SerializeField]
	GameObject Tutorial2Obj;

	[SerializeField]
	GameObject CloseBtn;


	// Use this for initialization
	void Start () 
	{
		GameData.Instance.CanPushUIFlg = false;
		GameData.Instance.CanTouchFlg = false;

		//GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
	}

	public void OnClose1 ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		Tutorial1Obj.SetActive(false);
		Tutorial2Obj.SetActive(true);
	}

	public void OnClose2 ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		GameData.Instance.CanPushUIFlg = true;
		GameData.Instance.CanTouchFlg = true;

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		CloseBtn.SetActive(false);
		Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => Destroy(gameObject));
	}
	

}
