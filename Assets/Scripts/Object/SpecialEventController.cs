﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UniRx;
using UnityEngine.EventSystems;

public class SpecialEventController : MonoBehaviour {

	[SerializeField]
	GameObject FullTextObj;
	[SerializeField]
	GameObject TextObj;
	[SerializeField]
	GameObject FullImgObj;

	EventData data;
	Dictionary<int, EventData.Param>eventDic = new Dictionary<int, EventData.Param>();
	int eventNo = 0;

	GameObject TextBtn;
	void Start () {

		string scene = GameData.Instance.GetNowPlaceKey();
		if(scene.Equals("ED_BEST")){
			GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(false);
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(18);
		} else if(scene.Equals("ED_BAD")){
			GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(false);
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(17);
		} else if(scene.Equals("FRIEND")){
			GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(false);
		}
		data = Resources.Load<EventData>("Xls/Data/EventData");

		// イベント情報をセットする
		foreach (EventData.Param p in data.list) {
			if(p.event_key.Equals(scene)){
				if(! eventDic.ContainsKey(p.no)){
					eventDic.Add(p.no, p);
				}
			}
		}

		// テキスト送りのボタンを作成しておく
		TextBtn = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_SPECIALEVENT_TEXT_BTN"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		TextBtn.name = "PREFAB_SPECIALEVENT_TEXT_BTN";

		if(scene.Equals("OP")){
			TextBtn.transform.SetParent(GameObject.Find("Limit360View/EventPrefab").transform, false);
		} else if(scene.Contains("ED")){
			GvrViewer.Instance.Recenter();
			TextBtn.transform.SetParent(GameObject.Find("Limit360View/EventPrefab").transform, false);
			// BGM消す
			SoundManager.Instance.StopBGM();
			SoundManager.Instance.StopRoomBGM();
		} else {
			GvrViewer.Instance.Recenter();
			TextBtn.transform.SetParent(GameObject.Find("360View/EventPrefab").transform, false);
			// BGM消す
			SoundManager.Instance.StopBGM();
			SoundManager.Instance.StopRoomBGM();

		}

		// イベント
		EventTrigger trigger = TextBtn.transform.FindChild("TouchEvent").GetComponent<EventTrigger>();
		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerClick;
		entry.callback.AddListener( (eventData) => { UpdateEventNo(); } );
		trigger.triggers.Add(entry);

		TextBtn.SetActive(false);

		EventController();

	}


	public void EventController ()
	{
		if(eventDic.ContainsKey(eventNo+1)){
			eventNo++;
			string eventContent = eventDic[eventNo].event_content;
			if(eventContent.Equals("FULL_TEXT")){
				// フル画面のテキスト表示
				FullTextObj.SetActive(true);

				Animator anim = FullTextObj.GetComponent<Animator>();
				anim.Play("Anim_event_text_close", 0, 0.0f);

				TextBtn.SetActive(true);
				TextBtn.transform.FindChild("TouchEvent").GetComponent<EventTrigger>().enabled = false;
				Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe( _ => 
					TextBtn.transform.FindChild("TouchEvent").GetComponent<EventTrigger>().enabled = true);

				Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(true));
				Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => DisplayText());
			} else if(eventContent.Equals("PLAY_BGM")){
				SoundManager.Instance.PlayBGM (eventDic[eventNo].content, 1);
				EventController();
			} else if(eventContent.Equals("TEXT_BACK_FADE")){
				// 背景の黒フェードを消す
				Animator animBack = transform.FindChild("FullTextBack").GetComponent<Animator>();
				animBack.Play("Anim_event_text_back_fade", 0, 0.0f);
				Observable.Timer(TimeSpan.FromSeconds(2f)).Subscribe( _ => 
					transform.FindChild("FullTextBack").gameObject.SetActive(false));

				// 文字も消す
				Animator animText = FullTextObj.GetComponent<Animator>();
				animText.Play("Anim_event_text_close", 0, 0.0f);
				Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => 
					FullTextObj.SetActive(false));

				Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe( _ => EventController());
			} else if(eventContent.Equals("DELAY")){
				Observable.Timer(TimeSpan.FromSeconds(
					float.Parse(eventDic[eventNo].content))).Subscribe( _ => EventController());
			} else if(eventContent.Equals("TEXT")){
				TextObj.SetActive(true);
				TextBtn.SetActive(true);
				TextBtn.transform.FindChild("TouchEvent").GetComponent<EventTrigger>().enabled = false;
				Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe( _ => 
					TextBtn.transform.FindChild("TouchEvent").GetComponent<EventTrigger>().enabled = true);

				TextObj.transform.FindChild("Text").GetComponent<Text>().text = eventDic[eventNo].content;

				Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(true));
			} else if(eventContent.Equals("AUTO_TEXT")){
				TextObj.SetActive(true);
				TextObj.transform.FindChild("Text").GetComponent<Text>().text = eventDic[eventNo].content;

				Observable.Timer(TimeSpan.FromSeconds(3f)).Subscribe( _ => UpdateEventNo());
			} else if(eventContent.Equals("FADE_OUT_AND_FADE_IN")){
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT_AND_FADE_IN"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.name = "PREFAB_FADE_OUT_AND_FADE_IN";
				prefab.transform.SetParent(GameObject.Find("FrontGvrMain/Head/Canvas").transform, false);

				Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => EventController());
			} else if(eventContent.Equals("PLAY_SE")){
				SoundManager.Instance.PlaySE (eventDic[eventNo].content, 1);
				EventController();
			} else if(eventContent.Equals("CREATE_SE_PREFAB")){
				// プレハブの場所を取得
				string placeName = GameData.Instance.GetNowPlaceKey();
				GameObject prefab = null;
				if(null != Resources.Load<GameObject> ("Prefab/" + placeName + "/" + eventDic[eventNo].content)){
					prefab = Instantiate (
						Resources.Load<GameObject> ("Prefab/" + placeName + "/" + eventDic[eventNo].content), 
						Vector3.zero, 
						Quaternion.identity
					) as GameObject;
				}

				EventController();
			} else if(eventContent.Equals("DISPLAY_360BACKGROUND")){
				GameData.Instance.SceneString = "SCENE_LOBBY";
				GameObject.Find("Manager").GetComponent<EventManager>().Set360Background(false);
				EventController();
				TextBtn.transform.SetParent(GameObject.Find("360View/EventPrefab").transform, false);
			} else if(eventContent.Equals("LOAR_SCENE")){
				GameData.Instance.SceneString = eventDic[eventNo].content;
				GlobalObject.LoadLevelWithString("Dummy", "");
				//GlobalObject.LoadLevelWithString(define.SceneNameGame, define.SceneOptGame);
			} else if(eventContent.Equals("ENABLED_UI_BUTTON")){
				GameData.Instance.CanPushUIFlg = true;
				EventController();
			} else if(eventContent.Equals("FULL_IMG")){

				string spritePlaceName = GameData.Instance.GetNowPlaceKey();
				if(GameData.Instance.GetNowPlaceKey().Contains("ED")){
					spritePlaceName = "ED";
				}

				Sprite[] sprites = Resources.LoadAll<Sprite> ("Event/" + spritePlaceName + "/"
					+ eventDic[eventNo].other);
				FullImgObj.GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (eventDic[eventNo].content));
				EventController();
			} else if(eventContent.Equals("CREATE_PREFAB")){
				// プレハブの場所を取得
				GameObject prefab = null;
				if(null != Resources.Load<GameObject> ("Prefab/" + GameData.Instance.GetNowPlaceKey() + "/" + eventDic[eventNo].content)){
					prefab = Instantiate (
						Resources.Load<GameObject> ("Prefab/" + GameData.Instance.GetNowPlaceKey() + "/" + eventDic[eventNo].content), 
						Vector3.zero, 
						Quaternion.identity
					) as GameObject;
				}
				prefab.name = eventDic[eventNo].content;
				prefab.transform.SetParent(transform, false);
				prefab.transform.SetSiblingIndex(3);

				EventController();
			} else if(eventContent.Equals("DESTROY_PREFAB")){
				Destroy(transform.FindChild(eventDic[eventNo].content).gameObject);
				EventController();
			} else if(eventContent.Equals("ARCHIEVEMENT")){
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/Common/PREFAB_ARCHIVEMENT"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;

				prefab.transform.SetParent(GameObject.Find("UICanvas").transform, false);
				GameObject.Find("UICanvas").GetComponent<MenuObject>().SetTutorialPopupTransform(prefab);

				FullTextObj.SetActive(false);
				Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(true));
			} else if(eventContent.Equals("SET_LIMIT_360")){
				GameObject.Find("Manager").GetComponent<EventManager>().SetEDLimit360();
				FullImgObj.SetActive(false);
				transform.FindChild("FullImageback").gameObject.SetActive(false);
				EventController();
			} else {
				Debug.Log("not key:" + eventContent);
			}
		}
	}

	/// <summary>
	/// フル画面のテキストを表示する
	/// </summary>
	void DisplayText ()
	{
		Animator anim = FullTextObj.GetComponent<Animator>();
		anim.Play("Anim_event_text_open", 0, 0.0f);

		FullTextObj.GetComponent<Text>().text = eventDic[eventNo].content;
	}

	/// <summary>
	/// イベントナンバーを進める
	/// </summary>
	public void UpdateEventNo ()
	{
		if(eventDic.ContainsKey(eventNo)){
			if(eventDic[eventNo].event_content.Equals("TEXT") ||
				eventDic[eventNo].event_content.Equals("FULL_TEXT") ||eventDic[eventNo].event_content.Equals("AUTO_TEXT") ){
				TextBtn.SetActive(false);
				TextObj.SetActive(false);
				GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(false);
				EventController();
			}
		}
	}
}
