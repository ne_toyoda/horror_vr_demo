﻿using UnityEngine;
using System.Collections;

public class MenuDisplayScript : MonoBehaviour {

	[SerializeField]
	GameObject Menu;
	GvrHead head;

	void Start ()
	{
		head = GameObject.Find("GvrMain/Head/Main Camera").GetComponent<StereoController>().Head;
	}

	void Update ()
	{
		if(Menu.activeInHierarchy){
			if(head.transform.localEulerAngles.x < 30){
				transform.GetComponent<BoxCollider>().enabled = true;
				Menu.SetActive(false);
			}
		}
	}


	/*
	private void OnBecameInvisible()
	{
		if(null != Menu)
			Menu.SetActive(false);
	}
	*/
}
