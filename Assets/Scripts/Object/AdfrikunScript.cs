﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;

public class AdfrikunScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//HideAdfurikunBannerAd();
	}
	
	public void ShowAdfurikunBannerAd () 
	{
		#if UNITY_EDITOR
		#else
		AdfurikunUtility au = GetComponent<AdfurikunUtility>();
		au.showBannerAd(0);
		#endif
	}

	public void HideAdfurikunBannerAd () 
	{
		#if UNITY_EDITOR
		#else
		AdfurikunUtility au = GetComponent<AdfurikunUtility>();
		au.hideBannerAd(0);
		#endif
	}

	public void ShowAdfurikunIntersAd () 
	{
		#if UNITY_EDITOR
		#else
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/AdsBack"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "AdsBack";
		prefab.transform.SetParent(GameObject.Find("Canvas").transform, false);

		AdfurikunUtility au = GetComponent<AdfurikunUtility>();
		au.showIntersAd(this.gameObject, 0);
		#endif
	}

	void AdfurikunIntersAdClose()
	{
		if(null != GameObject.Find("Canvas/AdsBack")){
			Destroy(GameObject.Find("Canvas/AdsBack"));
		}
	}

	void AdfurikunIntersAdNotDisplayed()
	{
		if(null != GameObject.Find("Canvas/AdsBack")){
			Destroy(GameObject.Find("Canvas/AdsBack"));
		}
	}
}
