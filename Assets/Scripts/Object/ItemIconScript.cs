﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemIconScript : MonoBehaviour {


	/// <summary>
	/// アイテムアイコンを選択
	/// </summary>
	public void OnItemIcon ()
	{
		GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1);
		GameObject.Find("UICanvas/ItemPop").GetComponent<ItemScript>().SelectItem(transform.name);
		SoundManager.Instance.PlaySE ("140", 1f);
	}

	/// <summary>
	/// アイテムアイコンを削除
	/// </summary>
	void DestroyIcon ()
	{
		Destroy(gameObject);
	}
}
