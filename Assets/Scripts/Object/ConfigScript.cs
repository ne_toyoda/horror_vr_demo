﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class ConfigScript : MonoBehaviour {

	[SerializeField]
	ToggleGroup toggleGroupBGM;
	[SerializeField]
	ToggleGroup toggleGroupSE;
	[SerializeField]
	ToggleGroup toggleGroupVIBRATE;
	[SerializeField]
	ToggleGroup toggleGroupGAZE;
	[SerializeField]
	ToggleGroup toggleGroupGAZESPEED;

	[SerializeField]
	GameObject GAZESPEEDObj;

	[SerializeField]
	GameObject TopPopupObj;
	[SerializeField]
	GameObject UserIDText;

	bool isStart = true;

	// Use this for initialization
	void Start () 
	{
		// user_id
		if (PlayerPrefs.HasKey("user_id")) {
			UserIDText.GetComponent<Text>().text = "No:" + PlayerPrefs.GetString ("user_id");
		} else {
			UserIDText.GetComponent<Text>().text = "";
		}


		if(GameData.Instance.IsBGM){
			transform.FindChild("BGM/ON").GetComponent<Toggle>().isOn = true;
			transform.FindChild("BGM/OFF").GetComponent<Toggle>().isOn = false;
		} else {
			transform.FindChild("BGM/ON").GetComponent<Toggle>().isOn = false;
			transform.FindChild("BGM/OFF").GetComponent<Toggle>().isOn = true;
		}

		if(GameData.Instance.IsSE){
			transform.FindChild("SE/ON").GetComponent<Toggle>().isOn = true;
			transform.FindChild("SE/OFF").GetComponent<Toggle>().isOn = false;
		} else {
			transform.FindChild("SE/ON").GetComponent<Toggle>().isOn = false;
			transform.FindChild("SE/OFF").GetComponent<Toggle>().isOn = true;
		}

		if(GameData.Instance.IsVIBRATE){
			transform.FindChild("VIBRATE/ON").GetComponent<Toggle>().isOn = true;
			transform.FindChild("VIBRATE/OFF").GetComponent<Toggle>().isOn = false;
		} else {
			transform.FindChild("VIBRATE/ON").GetComponent<Toggle>().isOn = false;
			transform.FindChild("VIBRATE/OFF").GetComponent<Toggle>().isOn = true;
		}

		if(GameData.Instance.IsGazeTouch){
			transform.FindChild("GAZE/ON").GetComponent<Toggle>().isOn = true;
			transform.FindChild("GAZE/OFF").GetComponent<Toggle>().isOn = false;

			GAZESPEEDObj.SetActive(true);

			transform.FindChild("GAZESPEED/1").GetComponent<Toggle>().isOn = false;
			transform.FindChild("GAZESPEED/2").GetComponent<Toggle>().isOn = false;
			transform.FindChild("GAZESPEED/3").GetComponent<Toggle>().isOn = false;

			Debug.Log("GazeSpeed:" +GameData.Instance.GazeSpeed);

			if(1 == GameData.Instance.GazeSpeed){
				transform.FindChild("GAZESPEED/1").GetComponent<Toggle>().isOn = true;
			} else if(2 == GameData.Instance.GazeSpeed){
				transform.FindChild("GAZESPEED/2").GetComponent<Toggle>().isOn = true;
			} else if(3 == GameData.Instance.GazeSpeed){
				transform.FindChild("GAZESPEED/3").GetComponent<Toggle>().isOn = true;
			}

		} else {
			transform.FindChild("GAZE/ON").GetComponent<Toggle>().isOn = false;
			transform.FindChild("GAZE/OFF").GetComponent<Toggle>().isOn = true;

			GAZESPEEDObj.SetActive(false);
		}

		isStart = false;
	}

	/// <summary>
	/// トグルボタンを押下
	/// </summary>
	public void OnToggle(string kind)
	{
		if(isStart)	return;

		if(kind.Equals("BGM")){
			string isToggle = toggleGroupBGM.ActiveToggles().First().name;

			if(isToggle.Equals("ON")){
				GameData.Instance.IsBGM = true;
				SoundManager.Instance.PlayBGM("", 0);
				SoundManager.Instance.PlayRoomBGM("", 0);
			} else {
				GameData.Instance.IsBGM = false;
				SoundManager.Instance.StopBGM();
				SoundManager.Instance.StopRoomBGM();
			}
		} else if(kind.Equals("SE")){
			string isToggle = toggleGroupSE.ActiveToggles().First().name;

			if(isToggle.Equals("ON")){
				GameData.Instance.IsSE = true;
			} else {
				GameData.Instance.IsSE = false;
			}
		} else if(kind.Equals("VIBRATE")){
			string isToggle = toggleGroupVIBRATE.ActiveToggles().First().name;

			if(isToggle.Equals("ON")){
				GameData.Instance.IsVIBRATE = true;
			} else {
				GameData.Instance.IsVIBRATE = false;
			}
		} else if(kind.Equals("GAZE")){
			string isToggle = toggleGroupGAZE.ActiveToggles().First().name;

			if(isToggle.Equals("ON")){
				GameData.Instance.IsGazeTouch = true;
				GAZESPEEDObj.SetActive(true);
			} else {
				GameData.Instance.IsGazeTouch = false;
				GAZESPEEDObj.SetActive(false);
			}
		} else if(kind.Equals("GAZESPEED")){
			string isToggle = toggleGroupGAZESPEED.ActiveToggles().First().name;
			if(isToggle.Contains("1")){
				Debug.Log("1");
				GameData.Instance.GazeSpeed = 1;
			} else if(isToggle.Contains("2")){
				Debug.Log("2");
				GameData.Instance.GazeSpeed = 2;
			} else if(isToggle.Contains("3")){
				Debug.Log("3");
				GameData.Instance.GazeSpeed = 3;
			}

			if(null != GameObject.Find("EventSystem").GetComponent<GazeInputModule>()){
				GameObject.Find("EventSystem").GetComponent<GazeInputModule>().ChangeGazeSpeed();
			}
		}

		if(!isStart){
			SoundManager.Instance.PlaySE ("140", 1f);
		}
	}

	/// <summary>
	/// トップへ戻るボタンの押下
	/// </summary>
	public void OnTopBtn ()
	{
		TopPopupObj.SetActive(true);
	}

	/// <summary>
	/// トップへ戻るポップアップのボタン押下
	/// </summary>
	public void OnTopPopupBtn(bool flg)
	{
		if(flg){
			GlobalObject.LoadLevelWithString("Top", "");
		} else {
			TopPopupObj.SetActive(false);
		}
	}

	public void CloseConfigPopup ()
	{
		GameObject.Find("UICanvas").GetComponent<MenuObject>().ClosePopup();
	}

	public void CloseTitleConfigPopup ()
	{
		Destroy(gameObject);
	}
}
