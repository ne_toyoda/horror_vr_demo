﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;

public class NewGameScript : MonoBehaviour {

	/// <summary>
	/// ポップアップ選択
	/// </summary>
	public void OnSelectBtn (bool flg)
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		if(flg){
			GameData.Instance.AllInit();
			GameData.Instance.SceneString = "SCENE_OP";

			// フラグ初期化
			GameData.Instance.InitFlgData();
			GameData.Instance.InitItemData();
			GameData.Instance.SetItem = "";

			GameObject.Find("Manager").GetComponent<TitleManager>().OnStartGame();
		} else {
			Destroy(gameObject);
		}
	}
}
