﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineController : MonoBehaviour {

	LineRenderer lineRenderer;
	[SerializeField]
	GvrHead head;
	[SerializeField]
	Camera thisCamera;

	List<Vector3> linePoints = new List<Vector3>();
	public float startWidth = 1.0f;
	public float endWidth = 1.0f;
	public float threshold = 0.001f;
	int lineCount = 0;

	Vector3 lastPos = Vector3.one * float.MaxValue;

	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer>();
		//lineRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update()
	{
		Vector3 mousePos = head.transform.localPosition;
		mousePos.z = thisCamera.nearClipPlane;
		Vector3 mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);

		float dist = Vector3.Distance(lastPos, mouseWorld);
		if(dist <= threshold)
			return;

		lastPos = mouseWorld;
		if(linePoints == null)
			linePoints = new List<Vector3>();
		linePoints.Add(mouseWorld);

		UpdateLine();
	}

	void UpdateLine()
	{
		lineRenderer.SetWidth(startWidth, endWidth);
		lineRenderer.SetVertexCount(linePoints.Count);

		for(int i = lineCount; i < linePoints.Count; i++)
		{
			lineRenderer.SetPosition(i, linePoints[i]);
		}
		lineCount = linePoints.Count;
	}
}
