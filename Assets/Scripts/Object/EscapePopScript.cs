﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class EscapePopScript : MonoBehaviour {

	[SerializeField]
	GameObject YesBtn;
	[SerializeField]
	GameObject NoBtn;

	public void OnNo ()
	{
		GameData.Instance.CanPushUIFlg = true;
		GameData.Instance.CanTouchFlg = true;

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}

		NoBtn.SetActive(false);
		Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => Destroy(gameObject));
	}

	public void OnYesFirst ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_RE_ESCAPE_SELECT");
		GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();

		YesBtn.SetActive(false);
		Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => Destroy(gameObject));
	}

	public void OnYesSecond ()
	{
		if(GameData.Instance.GetEventFlg("FLG_MORTUARY_HELP_FRIEND")){
			GameData.Instance.SceneString = "SCENE_BEST_END";
		} else {
			GameData.Instance.SceneString = "SCENE_BAD_END";
		}
		GlobalObject.LoadLevelWithString("Dummy", "");
		//GlobalObject.LoadLevelWithString(define.SceneNameGame, define.SceneOptGame);
	}
}
