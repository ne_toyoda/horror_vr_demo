﻿using UnityEngine;
using System.Collections;

public class MapFloorScript : MonoBehaviour {

	public void OnFloorBtn (string placeName)
	{
		transform.parent.GetComponent<MapScript>().MovePlace(placeName);
	}
}
