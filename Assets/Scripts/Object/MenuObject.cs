﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuObject : MonoBehaviour {

	[SerializeField]
	GameObject Menu;
	[SerializeField]
	GameObject HeadCamera;
	[SerializeField]
	GameObject UseItemIconObj;
	[SerializeField]
	GameObject MenuBack;
	[SerializeField]
	GameObject MapBtn;
	[SerializeField]
	GameObject MagatamaText;

	GameObject OpenPopupObj = null;
	string usedItem = "";

	void Start () 
	{
		// ユーザー
		SetUseItemIcon();
	}

	public void EnterEvent ()
	{
		// UI無効の場合は表示しない
		if(! GameData.Instance.CanPushUIFlg) return;

		// あたり判定を消す
		transform.FindChild("Cube").GetComponent<BoxCollider>().enabled = false;

		// マガタマの個数をセットする
		MagatamaText.GetComponent<Text>().text = GameData.Instance.GetAllMagatama().ToString();

		/*
		// MAPをとっていなかったらマップボタンは非表示
		if(GameData.Instance.GetEventFlg("FLG_STAFF_ROOM_MAP")){
			Debug.Log("map true");
			MapBtn.SetActive(true);
		} else {
			Debug.Log("map false");
			MapBtn.SetActive(false);
		}
		*/

		if(! Menu.activeSelf){
			// メニューを開いてる時は表示しないようにする
			if(2 < transform.childCount) return;

			Menu.SetActive(true);
			GvrHead head = HeadCamera.GetComponent<StereoController>().Head;
			Menu.transform.position = head.transform.position + (head.Gaze.direction * 500);
			Menu.transform.localPosition = new Vector3(Menu.transform.localPosition.x, -370f, Menu.transform.localPosition.z);
			Menu.transform.localRotation = head.transform.rotation;
			/*
			Menu.transform.localRotation = Quaternion.Euler(head.transform.localRotation.x, 
				head.transform.localRotation.y+30,
				head.transform.localRotation.z);
				*/
		}

		//Menu.transform.localPosition = transform.position+(transform.forward*2);
		//Instantiate(meleePrefab, transform.position+(transform.forward*2), transform.rotation);
	}

	/// <summary>
	/// メニューポップアップを開けるかチェックする
	/// </summary>
	bool IsCreatablePop ()
	{
		if(2 < transform.childCount) {
			return false;
		} else {
			if(null != GameObject.Find("360View/360")){
				GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
			}
			if(null != GameObject.Find("Limit360View/EventPrefab")){
				GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
			}
			Menu.SetActive(false);

			return true;
		}
	}

	/// <summary>
	/// アイテムボタンを押下
	/// </summary>
	public void OnItem ()
	{
		if(!IsCreatablePop()) return;

		usedItem = GameData.Instance.SetItem;

		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/ItemPop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "ItemPop";

		OpenPopupObj = prefab;
		MenuBack.SetActive(true);

		SetPopupTransform(prefab);
	}

	/// <summary>
	/// アーカイブボタンを押下
	/// </summary>
	public void OnArchive ()
	{
		if(!IsCreatablePop()) return;

		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/ArchivePop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "ArchivePop";

		OpenPopupObj = prefab;
		MenuBack.SetActive(true);

		SetPopupTransform(prefab);
	}

	/// <summary>
	/// マップボタンを押下
	/// </summary>
	public void OnMap ()
	{
		if(!IsCreatablePop()) return;

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}

		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/MapPop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "MapPop";

		OpenPopupObj = prefab;
		MenuBack.SetActive(true);

		SetPopupTransform(prefab);
	}

	/// <summary>
	/// 設定ボタンを押下
	/// </summary>
	public void OnConfig ()
	{
		if(!IsCreatablePop()) return;

		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/ConfigPop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "ConfigPop";

		OpenPopupObj = prefab;
		MenuBack.SetActive(true);

		SetPopupTransform(prefab);
	}

	/// <summary>
	/// ヒントボタンを押下
	/// </summary>
	public void OnHint ()
	{
		if(!IsCreatablePop()) return;

		SoundManager.Instance.PlaySE ("113", 1.0f);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/HintPop"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "HintPop";

		OpenPopupObj = prefab;
		MenuBack.SetActive(true);

		SetPopupTransform(prefab);
	}

	/// <summary>
	/// ポップアップの配置を設定する
	/// </summary>
	public void SetPopupTransform(GameObject prefab)
	{
		GvrHead head = HeadCamera.GetComponent<StereoController>().Head;
		Vector3 pos;

		if(prefab.name.Equals("MapPop")){
			pos = head.transform.position + (head.Gaze.direction *1000);
			prefab.transform.localPosition = new Vector3(pos.x, -400, pos.z);
			prefab.transform.localRotation = head.transform.rotation;
			prefab.transform.Rotate(new Vector3(0f,0f,0f));
		} else {
			pos = head.transform.position + (head.Gaze.direction *1200);
			prefab.transform.localPosition = new Vector3(pos.x, -400, pos.z);
			prefab.transform.localRotation = head.transform.rotation;
			prefab.transform.Rotate(new Vector3(-40f,0f,0f));
		}
	}

	public void SetTutorialPopupTransform(GameObject prefab)
	{
		GvrHead head = HeadCamera.GetComponent<StereoController>().Head;
		Vector3 pos = head.transform.position + (head.Gaze.direction *1200);
		prefab.transform.localPosition = new Vector3(pos.x, -400, pos.z);
		prefab.transform.localRotation = head.transform.rotation;
		prefab.transform.Rotate(new Vector3(0f,0f,0f));
	}

	public void SetTutorialLimitPopupTransform(GameObject prefab)
	{
		GvrHead head = HeadCamera.GetComponent<StereoController>().Head;
		Vector3 pos = head.transform.position + (head.Gaze.direction *1000);
		prefab.transform.localPosition = new Vector3(pos.x, 0, pos.z);
		prefab.transform.localRotation = head.transform.rotation;
		prefab.transform.Rotate(new Vector3(0f,0f,0f));
	}

	/// <summary>
	/// 開いてるポップアップ系を消す
	/// </summary>
	public void ClosePopup (bool flg=true)
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		Destroy(OpenPopupObj);
		MenuBack.SetActive(false);

		GameData.Instance.CanTouchFlg = true;

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}

		GameObject.Find("UICanvas/Cube").GetComponent<BoxCollider>().enabled = true;
	}

	/// <summary>
	/// アイテムアイコンを設定
	/// </summary>
	public void SetUseItemIcon()
	{
		if(string.Empty == GameData.Instance.SetItem){
			// アイテムアイコン非表示
			UseItemIconObj.SetActive(false);
		} else {
			// アイテムアイコン表示
			Sprite[] sprites = Resources.LoadAll<Sprite> ("Item/ItemAtlas");

			UseItemIconObj.GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals ("icon_"+ GameData.Instance.SetItem));
			UseItemIconObj.SetActive(true);
		}

		if(string.Empty != GameData.Instance.SetItem){
			if(null != GameData.Instance.GetItemUseEvent(GameData.Instance.SetItem) &&
				null != GameData.Instance.GetItemUseEvent(GameData.Instance.SetItem)){

				string[] event_array = GameData.Instance.GetItemUseEvent(GameData.Instance.SetItem);
				for(int i=0; i<event_array.Length; i++){
					string[] event_info = event_array[i].ToString().Split('+');
					if(event_info.Length < 2) continue;

					if(GameData.Instance.GetNowPlaceKey().Equals(event_info[1])){
						if(GameData.Instance.GetNowPlaceKey().Equals("Mortuary") && GameData.Instance.GetEventFlg("FLG_MORTUARY_LIGHT_ON")){
							return;
						} else {
							// 場所が指定の場所のときイベント発生
							GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(event_info[0]);
							GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
							return;
						}
					}
				}
			}
		}

		// 使ってたアイテムをはずしたときにもイベントを起こす
		if(string.Empty != usedItem){
			if(null != usedItem && null != GameData.Instance.GetItemUseEvent(usedItem)){
				string[] event_array = GameData.Instance.GetItemUseEvent(usedItem);
				for(int i=0; i<event_array.Length; i++){
					string[] event_info = event_array[i].ToString().Split('+');
					if(event_info.Length < 2) continue;

					if(GameData.Instance.GetNowPlaceKey().Equals(event_info[1])){

						// 場所が指定の場所のときイベント発生
						GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(event_info[0]);
						GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
						return;
					}
				}
			}
		}
		return;
	}


	/// <summary>
	/// メニュー開いているかを返す
	/// </summary>
	public bool IsOpenMenu ()
	{
		if(2 < transform.childCount) return true;
		else return false;
	}

}
