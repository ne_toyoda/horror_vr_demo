﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using System;
using UniRx;

public class MapScript : MonoBehaviour {

	int nowFloorNo = 1;

	int F1 = 1;
	int F2 = 2;
	int B1 = 0;

	void Start () 
	{

		Debug.Log("scene:" + GameData.Instance.SceneString);

		/*
		string floor = GameData.Instance.GetNowMapFloor();

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/MAP_" + floor), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "MAP_" + floor;

		// 現在の場所は押せないように
		transform.FindChild("MAP_" + floor + "/" + GameData.Instance.GetNowPlaceKey())
			.GetComponent<Button>().interactable = false; 
		*/
		// 現在のフロア
		string floor = GameData.Instance.GetNowMapFloor();
		if(floor.Equals("F1")){
			nowFloorNo = F1;
		} else if(floor.Equals("F2")){
			nowFloorNo = F2;
		} else if(floor.Equals("B1")){
			nowFloorNo = B1;
		}

		// 現在いる部屋のMAPを表示
		CreateFloorPrefab();
	}

	/// <summary>
	/// 移動する
	/// </summary>
	public void MovePlace (string place)
	{
		// 移動するシーンを特定
		Dictionary<int, string> sceneDic = GameData.Instance.GetPlaceSceneArray(place);

		// 場所ごとに進行が進んでいるシーンから検索をかけるためソートして逆順にする
		List<int> keysList = new List<int>(sceneDic.Keys);
		keysList.Sort();
		keysList.Reverse();

		// どのシーンに移動するか特定する
		foreach(int key in keysList){
			if(GameData.Instance.IsSceneDecisionFlg(sceneDic[key])){
				// シーンの条件フラグがたっていればそのシーンに移動する
				GameData.Instance.SceneString = sceneDic[key];

				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/Common/FADE_OUT_NOT_DELETE"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.name = "FADE_OUT_NOT_DELETE";
				prefab.transform.SetParent(GameObject.Find("FrontGvrMain/Head/Canvas").transform, false);

				MoveScene(sceneDic[key]);
				//Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => MoveScene(sceneDic[key]));

				return;
			}
		}
	}

	/// <summary>
	/// 次へボタンを押す
	/// </summary>
	public void OnNextButton ()
	{
		// 消す
		Destroy(transform.FindChild("MAP_" + GetFloorString()).gameObject);

		// 移動
		nowFloorNo++;
		if( 2 < nowFloorNo){
			nowFloorNo = 0;
		}

		// マップを作成する
		CreateFloorPrefab();
	}

	/// <summary>
	/// 前へボタンを押す
	/// </summary>
	public void OnPrevButton ()
	{
		// 消す
		Destroy(transform.FindChild("MAP_" + GetFloorString()).gameObject);

		// 移動
		nowFloorNo--;
		if(nowFloorNo < 0){
			nowFloorNo = 2;
		}

		// 作る
		CreateFloorPrefab();
	}

	/// <summary>
	/// フロアボタンを押す
	/// </summary>
	public void OnFloorbtn (int floorNo)
	{
		SoundManager.Instance.PlaySE ("113", 1.0f);

		// 消す
		Destroy(transform.FindChild("MAP_" + GetFloorString()).gameObject);

		// 移動
		nowFloorNo = floorNo;

		// 作る
		CreateFloorPrefab();
	}

	/// <summary>
	/// マップを作成
	/// </summary>
	void CreateFloorPrefab ()
	{
		string floor = GetFloorString();

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/UI/MAP_" + floor), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(transform, false);
		prefab.name = "MAP_" + floor;

		// 現在の場所は押せないように
		Debug.Log("MAP_" + floor + "/" + GameData.Instance.GetNowPlaceKey());

		if(null != transform.FindChild("MAP_" + floor + "/" + GameData.Instance.GetNowPlaceKey())){
			transform.FindChild("MAP_" + floor + "/" + GameData.Instance.GetNowPlaceKey()).
			GetComponent<Button>().interactable = false; 
		}

		// 階数ボタンの設定
		transform.FindChild("F1").GetComponent<Button>().interactable = true;
		transform.FindChild("F2").GetComponent<Button>().interactable = true;
		transform.FindChild("B1").GetComponent<Button>().interactable = true;

		// 現在のフロアは押せなくする
		transform.FindChild(floor).GetComponent<Button>().interactable = false;
	}

	/// <summary>
	/// シーンを移動する
	/// </summary>
	void MoveScene(string scene)
	{
		SoundManager.Instance.PlaySE ("28", 1f);

		// フォーカスを切る
		GameObject.Find("Manager").GetComponent<EventManager>().SetReticle(false);

		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/Common/PREFAB_FADE_OUT"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.name = "PREFAB_FADE_OUT";
		prefab.transform.SetParent(GameObject.Find("FrontGvrMain/Head/Canvas").transform, false);

		Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => {
			// そのシーンに移動
			GameData.Instance.SceneString = scene;
			GlobalObject.LoadLevelWithString("Dummy", "");
			//GlobalObject.LoadLevelWithString("Game", "game");
		});
	}

	/// <summary>
	/// フロアの文字列を取得する
	/// </summary>
	string GetFloorString ()
	{
		if(F1 == nowFloorNo){
			return "F1";
		} else if(F2 == nowFloorNo){
			return "F2";
		} else if(B1 == nowFloorNo){
			return "B1";
		}

		return "";
	}

	public void OnCloseMapPopup ()
	{
		Observable.Timer(TimeSpan.FromSeconds(0.3f)).Subscribe( _ => GameObject.Find("UICanvas").GetComponent<MenuObject>().ClosePopup());
	}
}
