﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeskBoxPeopleScript : MonoBehaviour {

	[SerializeField]
	bool isNurse;

	public void SetProfileBtn ()
	{
		if(isNurse){
			if(GameData.Instance.GetArchive("archive_resume")){
				GetComponent<Button>().interactable = true;
			} else {
				GetComponent<Button>().interactable = false;
			}
		} else {
			if(GameData.Instance.GetArchive("archive_record")){
				GetComponent<Button>().interactable = true;
			} else {
				GetComponent<Button>().interactable = false;
			}
		}
	}

	public void OnSelect ()
	{
		GameObject.Find("UICanvas/PREFAB_DESK_BOX_FILE").GetComponent<DeskBoxFileScript>().OnPeople(transform.name);
	}
}
