﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeskBoxFileScript : MonoBehaviour {

	[SerializeField]
	GameObject Diary1Obj;
	[SerializeField]
	GameObject Diary2Obj;
	[SerializeField]
	GameObject Diary3Obj;
	[SerializeField]
	GameObject Diary4Obj;
	[SerializeField]
	GameObject Diary5Obj;
	[SerializeField]
	GameObject Diary6Obj;
	[SerializeField]
	GameObject Saito;
	[SerializeField]
	GameObject Hayashi;
	[SerializeField]
	GameObject Komura;
	[SerializeField]
	GameObject Chiba;
	[SerializeField]
	GameObject Meguro;

	[SerializeField]
	GameObject NextBtn;
	[SerializeField]
	GameObject CloseBtn;
	[SerializeField]
	GameObject BackBtn;

	int displayDiaryPage = 0;

	// Use this for initialization
	void Start () {
		// 持ってる日記の最初のページを表示する
		for(int i=1; i<7; i++){
			if(GameData.Instance.GetArchive("archive_diary_" + i.ToString())){
				displayDiaryPage = i;
				break;
			}
		}

		DisplayDiary();

	}
	
	void DisplayDiary ()
	{
		Diary1Obj.SetActive(false);
		Diary2Obj.SetActive(false);
		Diary3Obj.SetActive(false);
		Diary4Obj.SetActive(false);
		Diary5Obj.SetActive(false);
		Diary6Obj.SetActive(false);

		if(1 == displayDiaryPage){
			Diary1Obj.SetActive(true);
		} else if(2 == displayDiaryPage){
			Diary2Obj.SetActive(true);
		} else if(3 == displayDiaryPage){
			Diary3Obj.SetActive(true);
		} else if(4 == displayDiaryPage){
			Diary4Obj.SetActive(true);
		} else if(5 == displayDiaryPage){
			Diary5Obj.SetActive(true);
		} else if(6 == displayDiaryPage){
			Diary6Obj.SetActive(true);
		}
			
		BroadcastMessage ("SetProfileBtn", SendMessageOptions.DontRequireReceiver);

		if(0 == displayDiaryPage){
			// 日記が一個もない
			NextBtn.SetActive(false);
		} else {
			BackBtn.SetActive(false);
			NextBtn.SetActive(true);
		}

	}

	void DisplayPeopleInfo (string name)
	{
		Diary1Obj.SetActive(false);
		Diary2Obj.SetActive(false);
		Diary3Obj.SetActive(false);
		Diary4Obj.SetActive(false);
		Diary5Obj.SetActive(false);
		Diary6Obj.SetActive(false);

		if(name.Equals("Saito")){
			Saito.SetActive(true);
		} else if(name.Equals("Hayashi")){
			Hayashi.SetActive(true);
		} else if(name.Equals("Komura")){
			Komura.SetActive(true);
		} else if(name.Equals("Chiba")){
			Chiba.SetActive(true);
		} else if(name.Equals("Meguro")){
			Meguro.SetActive(true);
		}

		NextBtn.SetActive(false);
		BackBtn.SetActive(true);
	}

	public void OnClose ()
	{
		Destroy(gameObject);
	}

	public void OnNext ()
	{
		if(0 == displayDiaryPage)	return;

		for(int i=0; i<7; i++){
			displayDiaryPage++;
			if(6 < displayDiaryPage) displayDiaryPage = 1;
			if(GameData.Instance.GetArchive("archive_diary_" + displayDiaryPage.ToString())){
				break;
			}
		}

		DisplayDiary();
	}

	public void OnPeople (string name)
	{
		DisplayPeopleInfo(name);
	}

	public void OnBack ()
	{
		Saito.SetActive(false);
		Hayashi.SetActive(false);
		Komura.SetActive(false);
		Chiba.SetActive(false);
		Meguro.SetActive(false);

		DisplayDiary();
	}
}
