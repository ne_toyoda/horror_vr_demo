﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ArchiveTextDetailScript : MonoBehaviour {

	[SerializeField]
	GameObject PageBtn;
	[SerializeField]
	GameObject NextBtn;

	string[] textArray;
	int page = 0;
	int max_page = 0;

	/// <summary>
	/// アーカイブポップアップを消す
	/// </summary>
	public void ClosePopup ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		GameObject.Find("UICanvas/ArchivePop").GetComponent<ArchiveScript>().ActiveArchiveList();
		Destroy(gameObject);
	}

	/// <summary>
	/// アーカイブ内容を表示する
	/// </summary>
	public void SetText (string text)
	{
		// 複数ページがあるか
		if(text.Contains(",")){
			PageBtn.SetActive(true);
			textArray = text.ToString().Split(new String[] { ",\n" }, StringSplitOptions.None);
			max_page = textArray.Length;

			// ページ数をセット
			PageBtn.transform.FindChild("Page").GetComponent<Text>().text = (page+1).ToString() + "/" + max_page.ToString();
			transform.FindChild("Content").GetComponent<Text>().text = textArray[page];

			SetPageBtn();
		} else {
			transform.FindChild("Content").GetComponent<Text>().text = text;
		}
	}

	void SetPageBtn ()
	{
		NextBtn.SetActive(true);
	}

	/*
	public void OnPrev ()
	{
		page--;

		// ページ数をセット
		PageBtn.transform.FindChild("Page").GetComponent<Text>().text = (page+1).ToString() + "/" + max_page.ToString();
		transform.FindChild("Content").GetComponent<Text>().text = textArray[page];

		SetPageBtn();
	}
	*/

	public void OnNext ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		page++;
		if(max_page <= page) page = 0;

		// ページ数をセット
		PageBtn.transform.FindChild("Page").GetComponent<Text>().text = (page+1).ToString() + "/" + max_page.ToString();
		transform.FindChild("Content").GetComponent<Text>().text = textArray[page];

		SetPageBtn();
	}
}
