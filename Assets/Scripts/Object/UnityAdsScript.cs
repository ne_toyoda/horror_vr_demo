﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using System;


public class UnityAdsScript : MonoBehaviour {

	[SerializeField]
	string AndroidAppId;
	[SerializeField]
	string IOSAppId;

	// Use this for initialization
	void Start () {
		string gameId = null;

		#if UNITY_ANDROID
		gameId = AndroidAppId;
		#elif UNITY_IOS
		gameId = IOSAppId;
		#endif

		if (Advertisement.isSupported && !Advertisement.isInitialized) {
			Advertisement.Initialize(gameId, false);
		}
	}


	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log("The ad was successfully shown.");
			// 勾玉+1
			GameObject.Find("Manager").GetComponent<TimerManager>().SetRecoveryTime();
			GameObject.Find("Manager").GetComponent<TimerManager>().NotActiveMovieBtn();

			GameData.Instance.AddFreeMagatama(1);
			GameObject.Find("Manager").GetComponent<TitleManager>().UpdateMagatamaDisplay();

			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}
	}

	public void ShowRewardVideo ()
	{
		GameData.Instance.IsAds = true;
		GameData.Instance.IsDisplayAds = false;
		GameData.Instance.ADsTime = DateTime.Now;
		GameData.Instance.IsDisplayAdsPop = false;
		#if UNITY_EDITOR
		GameObject.Find("Manager").GetComponent<TimerManager>().SetRecoveryTime();
		GameObject.Find("Manager").GetComponent<TimerManager>().NotActiveMovieBtn();

		GameData.Instance.AddFreeMagatama(1);
		GameObject.Find("Manager").GetComponent<TitleManager>().UpdateMagatamaDisplay();
		#else
		if (Advertisement.IsReady("rewardedVideo"))
		{
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);
		}
		#endif
	}



}
