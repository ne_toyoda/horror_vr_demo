﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class ShareScript : MonoBehaviour {

	string msgTxt = "";

	void Start ()
	{

	}

	/// <summary>
	/// 閉じる
	/// </summary>
	public void OnClose ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		Destroy(gameObject);
	}

	/// <summary>
	/// ツイッター
	/// </summary>
	public void OnTwitter ()
	{
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendShare("Twitter");
		SoundManager.Instance.PlaySE ("140", 1.0f);

		// Twitter用テキスト
		msgTxt = "【VR恐怖脱出】廃病院から脱出するVRホラー脱出アプリ\n" +
			"「改・恐怖！廃病院からの脱出：無影灯」の配信中！臨場感溢れる恐怖を体験しよう！\n" +
			"https://zzyzx.jp/mueitou_vr/hp/store.php\n" +
			"#無影灯 #ホラゲ #VR";
		SWorker.SocialWorker.PostTwitter(msgTxt, null, null, OnResultTwitter);
	}

	public void OnResultTwitter(SWorker.SocialWorkerResult res)
	{
		switch(res)
		{
		case SWorker.SocialWorkerResult.Success:
			break;
		case SWorker.SocialWorkerResult.NotAvailable:
			Application.OpenURL("http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(msgTxt));
			break;
		case SWorker.SocialWorkerResult.Error:
			break;
		}
	}

	public void PushLine ()
	{
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendShare("Line");
		SoundManager.Instance.PlaySE ("140", 1.0f);

		msgTxt = "【VR恐怖脱出】廃病院から脱出するVRホラー脱出アプリ\n" +
			"「改・恐怖！廃病院からの脱出：無影灯」の配信中！臨場感溢れる恐怖を体験しよう！\n" +
			"https://zzyzx.jp/mueitou_vr/hp/store.php";
		#if UNITY_IPHONE
		Application.OpenURL("http://line.naver.jp/R/msg/text/?" + WWW.EscapeURL(msgTxt, System.Text.Encoding.UTF8));
		#elif UNITY_ANDROID
		SWorker.SocialWorker.PostLine(msgTxt, null, null);
		#endif
	}

}
