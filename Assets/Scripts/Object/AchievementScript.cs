﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UniRx;
using System.Linq;

public class AchievementScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SetRecode();
	}
	
	void SetRecode ()
	{
		GameData.Instance.ClearCount = GameData.Instance.ClearCount+1;
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendGameClear();

		Animator _anim = GetComponent<Animator>();
		_anim.Play("Anim_point", 0, 0.0f);
		int point = 50;

		// ヒントのための達成してない配列
		Dictionary<string, string> hintDic = new Dictionary<string, string>();

		// 達成度を求める
		Dictionary<string, AchievementData.Param> dic = GameData.Instance.GetAchievementDic();
		foreach(string key in dic.Keys){
			if(GameData.Instance.GetEventFlg(dic[key].flgName)){
				// 加点する
				GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendAchievement(key);
				try {
					int add_point = Int32.Parse(dic[key].point);
					point += add_point;
				} catch {

				}
			} else {
				if(string.Empty != key){
					hintDic.Add(key, dic[key].hint);
				}
			}
		}

		GameData.Instance.AchievementPoint = point;

		// 表示
		transform.FindChild("Point").GetComponent<Text>().text = point.ToString() + "%";
		GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendAchievementRate(point.ToString());

		// ヒント
		if(point < 100){
			//配列を作成する
			string[] keys = new string[hintDic.Keys.Count];
			//キーを配列にコピーする
			hintDic.Keys.CopyTo(keys, 0);

			int no = UnityEngine.Random.Range(0, keys.Count());

			transform.FindChild("HintTitle").GetComponent<Text>().text = "達成への道 その" + (keys[no]).ToString();
			transform.FindChild("Hint").GetComponent<Text>().text = hintDic[keys[no]];
		} else {
			// 100%
			transform.FindChild("HintTitle").GetComponent<Text>().text = "";
			transform.FindChild("Hint").GetComponent<Text>().text = "無影灯マスターおめでとうございます！";
		}
	}


	public void OnTop()
	{
		GameData.Instance.SceneString = "SCENE_OP";

		// フラグ初期化
		GameData.Instance.AllInit();

		GlobalObject.LoadLevelWithString("VRRelease", "");
	}
}
