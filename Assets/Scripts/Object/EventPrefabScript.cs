﻿using UnityEngine;
using System.Collections;

public class EventPrefabScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	/// <summary>
	/// prefabを削除して次のイベントに進む
	/// </summary>
	public void NextEventAndDestroyPrefab ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().UpdateEventNo();
		Destroy(gameObject);
	}

	/// <summary>
	/// 次のイベントに進む
	/// </summary>
	public void NextEvent ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().UpdateEventNo();
	}

	/// <summary>
	/// prefabを削除する
	/// </summary>
	public void DestroyPrefab ()
	{
		Destroy(gameObject);
	}

	/// <summary>
	/// 振動
	/// </summary>
	public void VibrateEvent (float time)
	{
		if(! GameData.Instance.IsVIBRATE)	return;

		#if UNITY_EDITOR
		Debug.Log ( "vibrate(1):");
		#elif UNITY_WEBPLAYER

		#else
		Handheld.Vibrate();
		VibrateScript.vibrate(5);
		#endif
	}

	/// <summary>
	/// だーれだ終了
	/// </summary>
	public void EndBliendfold ()
	{
		GameData.Instance.CanTouchFlg = true;
		GameData.Instance.CanPushUIFlg = true;

		Destroy(gameObject);
	}

	public void PlaySE (string seName)
	{
		SoundManager.Instance.PlaySE (seName, 1f);
	}

	public void PlayFixSE (string seName)
	{
		SoundManager.Instance.PlaySE (seName, 1f);
	}
}
