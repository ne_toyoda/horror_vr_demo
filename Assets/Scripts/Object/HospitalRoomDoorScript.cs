﻿using UnityEngine;
using System.Collections;

public class HospitalRoomDoorScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameData.Instance.HospitalDoorCount++;

		if(3 == GameData.Instance.HospitalDoorCount){
			// ポップアップを出す
			GameObject.Find("Manager").GetComponent<EventManager>().DisplayHintPop();
		}
	}
	

}
