﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UniRx;

public class BoxKeyView : MonoBehaviour {

	[SerializeField]
	GameObject Icon1object;
	[SerializeField]
	GameObject Icon2object;
	[SerializeField]
	GameObject Icon3object;
	[SerializeField]
	GameObject Icon4object;
	[SerializeField]
	GameObject Icon5object;

	[SerializeField]
	GameObject BoxObj;

	int icon_1 = 1;
	int icon_2 = 2;
	int icon_3 = 3;
	int icon_4 = 4;
	int icon_5 = 5;

	void Start ()
	{
		GameData.Instance.DeskBoxCount++;

		if(3 == GameData.Instance.DeskBoxCount){
			// ポップアップを出す
			GameObject.Find("Manager").GetComponent<EventManager>().DisplayHintPop();
		}
	}

	public void PushButton (int no)
	{
		if(GameData.Instance.GetEventFlg("FLG_EXAMINATION_CLEAR_DESK_BOX_KEY")) return;

		int set_no = 0;

		if(1 == no){
			icon_1 = SetNo(icon_1);
			Icon1object.transform.SetLocalEulerAnglesZ((icon_1-1)*72);
		} else if(2 == no){
			icon_2 = SetNo(icon_2);
			Icon2object.transform.SetLocalEulerAnglesZ((icon_2-1)*72);
		} else if(3 == no){
			icon_3 = SetNo(icon_3);
			Icon3object.transform.SetLocalEulerAnglesZ((icon_3-1)*72);
		} else if(4 == no){
			icon_4 = SetNo(icon_4);
			Icon4object.transform.SetLocalEulerAnglesZ((icon_4-1)*72);
		} else if(5 == no){
			icon_5 = SetNo(icon_5);
			Icon5object.transform.SetLocalEulerAnglesZ((icon_5-1)*72);
		}

		SoundManager.Instance.PlaySE ("53", 1f);

		//Debug.Log("icon_1:" + icon_1 + " icon_2:" + icon_2 + " icon_3:" +icon_3 + " icon_4:" + icon_4 + " icon_5:" +icon_5);

		if(2 == icon_1 && 1== icon_2 && 5 == icon_3 && 4 == icon_4 && 3 == icon_5){
			// 日記もってることが条件
			if(GameData.Instance.GetArchive("archive_diary_1") && GameData.Instance.GetArchive("archive_diary_2") &&
				GameData.Instance.GetArchive("archive_diary_3") && GameData.Instance.GetArchive("archive_diary_4") &&
				GameData.Instance.GetArchive("archive_diary_5") && GameData.Instance.GetArchive("archive_diary_6")){

				// 正解
				GameData.Instance.SetEventFlg("FLG_EXAMINATION_CLEAR_DESK_BOX_KEY");

				// オープンアニメ
				Animator anim = BoxObj.GetComponent<Animator>();
				anim.runtimeAnimatorController = 
					(RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load ("Anim/AnimC_box_open"));

				Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => Complete());

			} else {
				// メッセージ
				GameObject.Find("Manager").GetComponent<EventManager>().DisplayMessageSpecialEvent("なにかファイルが足りないようだ…");
			}
		}
	}

	public void Complete ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_BOX_KEY_COMPLETE");
		GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
		Destroy(gameObject);
	}

	public void OnBoxKeyBack ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_BOX_KEY_BACK");
		GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
		Destroy(gameObject);
	}

	int SetNo (int no)
	{
		no++;
		if(5 < no){
			return 1;
		}
		return no;
	}

	public void OnFileBtn ()
	{
		GameObject prefab = Instantiate (
			Resources.Load<GameObject> ("Prefab/ExaminationRoom/PREFAB_DESK_BOX_FILE"), 
			Vector3.zero, 
			Quaternion.identity
		) as GameObject;
		prefab.transform.SetParent(GameObject.Find("UICanvas").transform, false);
		prefab.name = "PREFAB_DESK_BOX_FILE";

		GvrHead head = GameObject.Find("GvrMain/Head/Main Camera").GetComponent<StereoController>().Head;
		Vector3 pos = head.transform.position + (head.Gaze.direction *1000);
		prefab.transform.localPosition = new Vector3(pos.x, -400, pos.z);
		prefab.transform.localRotation = head.transform.rotation;
		prefab.transform.Rotate(new Vector3(0f,0f,0f));
	}
}
