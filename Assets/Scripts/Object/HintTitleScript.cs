﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HintTitleScript : MonoBehaviour {

	[SerializeField]
	GameObject TitleObject;

	[SerializeField]
	GameObject MagatamaObject;

	[SerializeField]
	GameObject BackImgObject;

	GameObject ParentObj;
	string no;

	/// <summary>
	/// ヒント情報をセットする
	/// </summary>
	public void SetInfo (GameObject obj, string no) {
		this.ParentObj = obj;
		this.no = no;

		TitleObject.GetComponent<Text>().text = GameData.Instance.GetHintTitle(no);

		UpdateBackImg();
	}

	/// <summary>
	/// ヒント枠画像をセットする
	/// </summary>
	public void UpdateBackImg ()
	{
		// マガタマの個数と背景
		Sprite[] sprites = Resources.LoadAll<Sprite> ("UI/UIAtlas");

		if(GameData.Instance.IsHintRelease(no)){
			// 解放済み
			MagatamaObject.GetComponent<Text>().text = "済";
			BackImgObject.GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals ("hint_cleared"));
		} else {
			MagatamaObject.GetComponent<Text>().text = 
				"×" + GameData.Instance.GetHintMagatama(no);
			BackImgObject.GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals ("hint_thinking"));
		}
	}

	public void OnHintBtn ()
	{
		SoundManager.Instance.PlaySE ("140", 1.0f);

		if(GameData.Instance.IsHintRelease(no)){
			// 解放済み
			ParentObj.GetComponent<HintScript>().DisplayContent(no);
		} else {
			// 未解放
			ParentObj.GetComponent<HintScript>().DisplayMagatamaPop(no);
		}
	}
}
