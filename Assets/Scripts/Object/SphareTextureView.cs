﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SphareTextureView : MonoBehaviour {

	bool isAlpha = false;
	float fadeSpeed = 3f;

	void Update () {
		if(isAlpha){
			Material[] mats = GetComponent<Renderer>().materials;
			Color color = mats[1].color;
			if(FloatFormat( color.a, 2) == 0.0f){
				Debug.Log("END");
				isAlpha = false;
				return;
			}
			color.a = 0.0f;
			mats[1].color = Color.Lerp(mats[1].color, color, fadeSpeed * Time.deltaTime);
		}
	}

	/// <summary>
	/// 360度テクスチャの設定
	/// </summary>
	/// <remarks>
	/// BroadcastMessageでよばれる
	/// </remarks>
	public void SetSceneTexture ()
	{
		int textureNo = 0;
		if(! Int32.TryParse(GameData.Instance.GetSceneNoPerRoom(), out textureNo)){
			Debug.LogError("背景テクスチャのシーンナンバーがおかしい");
			return;
		}

		// 画像があるフォルダを取得
		for(int i=textureNo; 0<i; --i ){
			string textureName = "Background/" + GameData.Instance.GetNowPlaceKey() + "/" +
				i.ToString() + "/" + transform.name + "_" + i.ToString();
			if(null != Resources.Load(textureName)){
				GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
				return;
			}
		}
	}

	/// <summary>
	/// タイトル360度テクスチャの設定
	/// </summary>
	/// <remarks>
	/// BroadcastMessageでよばれる
	/// </remarks>
	public void SetTitleTexture ()
	{
		string textureName = "Background/" + GameData.Instance.GetNowPlaceKey() + "/1/"
			+ transform.name + "_1";
		if(null != Resources.Load(textureName)){
			GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
			return;
		}
	}

	/// <summary>
	/// 制限360度テクスチャの設定
	/// </summary>
	public void SetLimitSceneTexture (string folder)
	{
		// 画像があるフォルダを取得
		// 背後部分
		string[] commonPrefabNoArray = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "12",
			"21", "24", "25", "26", "27", "28", "29", "30", "31", "32"};
		List<string> commonPrefabNoList = new List<string>();
		commonPrefabNoList.AddRange(commonPrefabNoArray);

		string textureName = "";
		if(commonPrefabNoList.Contains(transform.name)){
			// 背後部分特定のテクスチャ
			//textureName = "Event/normal";
		} else {
			textureName = "Event/" + GameData.Instance.GetNowPlaceKey() + "/" +
				folder + "/" + folder + "_" + transform.name;
			//Debug.Log("textureName:" + textureName);

			GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
		}


		/*
		for(int i=textureNo; 0<i; --i ){
			string textureName = "Background/" + GameData.Instance.GetNowPlaceKey() + "/" +
				i.ToString() + "/" + transform.name + "_" + i.ToString();
			if(null != Resources.Load(textureName)){
				GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
				return;
			}
		}
		*/
	}

	public void SetLimitEDSceneTexture ()
	{
		// 画像があるフォルダを取得
		// 背後部分
		string[] commonPrefabNoArray = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "12",
			"21", "24", "25", "26", "27", "28", "29", "30", "31", "32"};
		List<string> commonPrefabNoList = new List<string>();
		commonPrefabNoList.AddRange(commonPrefabNoArray);

		string textureName = "";
		if(commonPrefabNoList.Contains(transform.name)){
			// 背後部分特定のテクスチャ
			//textureName = "Event/normal";
		} else {
			textureName = "Event/OP/op/op_" + transform.name;
			GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
		}
	}

	public void SetSettingTexture ()
	{
		// 画像があるフォルダを取得
		// 背後部分
		string[] commonPrefabNoArray = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "12",
			"21", "24", "25", "26", "27", "28", "29", "30", "31", "32"};
		List<string> commonPrefabNoList = new List<string>();
		commonPrefabNoList.AddRange(commonPrefabNoArray);

		string textureName = "";
		if(commonPrefabNoList.Contains(transform.name)){
			// 背後部分特定のテクスチャ
			//textureName = "Event/normal";

		} else {
			textureName = "Event/OP/op/op_" + transform.name;
			GetComponent<Renderer>().material.mainTexture = Resources.Load(textureName) as Texture;
		}

	}

	/// <summary>
	/// 360度テクスチャをフェードで切り替える時に呼ばれる
	/// </summary>
	/// <remarks>
	/// イベント中に切り替える時呼ばれる
	/// <remarks>
	public bool ChangeSceneTextureFade (int sceneNo)
	{
		// 画像があるフォルダを取得
		string texture_name = "Background/" + GameData.Instance.GetNowPlaceKey() + "/" +
			sceneNo.ToString() + "/" + transform.name + "_" + sceneNo.ToString();
		if(null != Resources.Load(texture_name)){
			Material[] mats = GetComponent<Renderer>().materials;
			if(mats[0].mainTexture.name.Equals(transform.name + "_" + sceneNo.ToString())){
				return false;
			}

			Material tmp_material = mats[0];

			Material[] new_mats = new Material[2];
			new_mats[1] = tmp_material;
			new_mats[0] = new Material(tmp_material.shader);
			new_mats[0].color = tmp_material.color;
			new_mats[0].mainTexture = Resources.Load(texture_name) as Texture;
			GetComponent<Renderer>().materials = new_mats;
			isAlpha = true;
		}

		return true;
	}
		
	/// <summary>
	/// 小数点以下丸め込みメソッド
	/// </summary>
	public float FloatFormat( float number, int n ) {
		var _pow = Math.Pow( 10 , n ) ;
		return (float)(Math.Round( number * _pow ) / _pow);
	}

}
