﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class MortuaryNoLightScript : MonoBehaviour {

	bool isTouch = false;

	// Use this for initialization
	void Start () {
		GameData.Instance.CanTouchFlg = true;
		GvrViewer.Instance.Recenter();

		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
	}

	public void OnTouch ()
	{
		if(isTouch) return;

		isTouch = true;

		GameObject.Find("Manager").GetComponent<EventManager>().DisplayMessageSpecialEvent("真っ暗で何も見えない…");

		Observable.Timer(TimeSpan.FromSeconds(2.0f)).Subscribe( _ => 
			{
				GameObject.Find("UICanvas").GetComponent<MenuObject>().OnMap();
				isTouch = false;
			});
	}
}
