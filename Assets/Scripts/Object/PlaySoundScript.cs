﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;

public class PlaySoundScript : MonoBehaviour {

	[SerializeField]
	int playTime = 0;

	// Use this for initialization
	void Start () {
		Play3DSound();
	}



	public void Play3DSound ()
	{
		// SEの設定をチェック
		if(! GameData.Instance.IsSE) return;

		if(0 != playTime){
			Observable.Timer(TimeSpan.FromSeconds(playTime)).Subscribe( _ => 
				{
					if(null != gameObject){
						GetComponent<GvrAudioSource>().Play ();
					}
				}
			);
		} else {
			GetComponent<GvrAudioSource>().Play ();
		}
	}
	

}
