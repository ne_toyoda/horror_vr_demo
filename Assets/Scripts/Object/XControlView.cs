﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using UnityEngine.UI;

public class XControlView : MonoBehaviour {

	[SerializeField]
	GameObject RedMemory;
	[SerializeField]
	GameObject GreenMemory;
	[SerializeField]
	GameObject BlueMemory;

	[SerializeField]
	GameObject Flash;
	[SerializeField]
	GameObject Key;

	int red_mem, blue_mem = 0;
	int green_mem = 10;


	bool CheckPower ()
	{
		// 終わったあとはいじれない
		if(GameData.Instance.GetEventFlg("FLG_X_CONTROLL_MANNEQUIN")) return false;

		// 電気が入ってないとボタン押せないように
		if(! GameData.Instance.GetEventFlg("FLG_MORTUARY_LIGHT_ON")){
			SoundManager.Instance.PlaySE ("53", 0.5f);

			GameObject.Find("Manager").GetComponent<EventManager>().DisplayMessageSpecialEvent("電気が通っていないようだ");

			return false;
		}

		return true;
	}

	public void OnPrevButton (string color)
	{
		if(! CheckPower()) return; 

		SoundManager.Instance.PlaySE ("55", 0.5f);

		if(color.Equals("Red")){
			if(0 < red_mem){
				red_mem--;
				RedMemory.transform.SetLocalPositionX(1.1f-(0.155f*red_mem));
			}
		} else if(color.Equals("Green")){
			if(0 < green_mem){
				green_mem--;
				GreenMemory.transform.SetLocalPositionX(1.1f-(0.155f*green_mem));
			}
		} else if(color.Equals("Blue")){
			if(0 < blue_mem){
				blue_mem--;
				BlueMemory.transform.SetLocalPositionX(1.1f-(0.155f*blue_mem));
			}
		}
	}

	public void PushNextButton (string color)
	{
		if(! CheckPower()) return; 

		SoundManager.Instance.PlaySE ("55", 0.5f);

		if(color.Equals("Red")){
			if(red_mem < 10){
				red_mem++;
				RedMemory.transform.SetLocalPositionX(1.1f-(0.155f*red_mem));
			}
		} else if(color.Equals("Green")){
			if(green_mem < 10){
				green_mem++;
				GreenMemory.transform.SetLocalPositionX(1.1f-(0.155f*green_mem));
			}
		} else if(color.Equals("Blue")){
			if(blue_mem < 10){
				blue_mem++;
				BlueMemory.transform.SetLocalPositionX(1.1f-(0.155f*blue_mem));
			}
		}
	}

	public void PushDecision ()
	{
		if(! CheckPower()) return; 

		Flash.SetActive(true);
		Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => Flash.SetActive(false));

		SoundManager.Instance.PlaySE ("57", 1f);

		// 正解
		if(4 == red_mem && 7 == green_mem && 2 == blue_mem){
			if(GameData.Instance.GetEventFlg("FLG_MANNEQUIN_PUT")){
				GameData.Instance.SetEventFlg("FLG_X_CONTROLL_MANNEQUIN");

				Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe( _ => Key.SetActive(true));
				Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe( _ => DisplayCompleteMsg());
			}
		} else {
			GameData.Instance.XRayCount++;

			if(3 == GameData.Instance.XRayCount){
				// ポップアップを出す
				GameObject.Find("Manager").GetComponent<EventManager>().DisplayHintPop();
			}
		}
	}

	public void OnXControllBack ()
	{
		GameData.Instance.CanTouchFlg = true;

		GameObject.Find("Manager").GetComponent<EventManager>().SetEvent("EVENT_X_CONTROL_BACK");
		GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
		Destroy(gameObject);
	}

	void DisplayCompleteMsg ()
	{
		GameObject.Find("Manager").GetComponent<EventManager>().DisplayMessageSpecialEvent("中に鍵のようなものが見える");
	}
}
