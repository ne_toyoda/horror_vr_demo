﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UniRx;
using System;

public class GetMagatamaScript : MonoBehaviour {

	GameObject target;

	// Use this for initialization
	void Start () {
		target = GameObject.Find("GvrMain/Head/Main Camera");
		transform.DOLocalMove (new Vector3 (transform.localPosition.x, transform.localPosition.y+1f, transform.localPosition.z), 1f).SetEase (Ease.InOutQuart);
		Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe( _ => Destroy(gameObject));
	}

	void Update ()
	{
		transform.LookAt(-target.transform.forward);
		transform.forward = target.transform.forward;
	}
	

}
