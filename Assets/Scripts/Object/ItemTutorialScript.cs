﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ItemTutorialScript : MonoBehaviour {

	[SerializeField]
	GameObject ItemListWindowObj;
	[SerializeField]
	GameObject ItemDetailObj;
	[SerializeField]
	GameObject MatchDetailObj;
	[SerializeField]
	GameObject ItemBtnObj;
	[SerializeField]
	GameObject TutorialMsg;
	[SerializeField]
	GameObject EndMsgObj;

	GameObject target;

	string selectItemKey = "";
	string matchItemKey = "";

	bool isUse = true;

	public int tutorialNo = 1;

	// Use this for initialization
	void Start () {
		target = GameObject.Find("UIGvrMain/Head");
		CreateItemIcon();

		// 使っているアイテムがあればセットする
		if(string.Empty != GameData.Instance.SetItem){
			ItemListWindowObj.transform.FindChild(GameData.Instance.SetItem).GetComponent<ItemIconScript>().OnItemIcon();
		}

		GameObject.Find("Window/item_stainless").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Use").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Match").GetComponent<Button>().interactable = false;

		// タッチ
		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetAllNonActiveTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
	}

	void Tutorial2Controll ()
	{
		tutorialNo = 2;
		transform.FindChild("Tutorial/1").GetComponent<Image>().enabled = false;
		transform.FindChild("Tutorial/2").GetComponent<Image>().enabled = true;

		GameObject.Find("Window/item_stainless").GetComponent<Button>().interactable = false;
		GameObject.Find("Window/item_key_staff_room").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Use").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Match").GetComponent<Button>().interactable = true;
	}

	void Tutorial3Controll()
	{
		tutorialNo = 3;
		transform.FindChild("Tutorial/2").GetComponent<Image>().enabled = false;
		transform.FindChild("Tutorial/3").GetComponent<Image>().enabled = true;

		GameObject.Find("Window/item_stainless").GetComponent<Button>().interactable = true;
		GameObject.Find("Window/item_key_staff_room").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Use").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Match").GetComponent<Button>().interactable = false;
	}

	void Tutorial4Controll ()
	{
		tutorialNo = 4;
		transform.FindChild("Tutorial/2").GetComponent<Image>().enabled = true;
		transform.FindChild("Tutorial/3").GetComponent<Image>().enabled = false;

		GameObject.Find("Window/item_stainless").GetComponent<Button>().interactable = false;
		GameObject.Find("Window/item_key_staff_room").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Use").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Match").GetComponent<Button>().interactable = true;
	}

	void Tutorial5Controll ()
	{
		tutorialNo = 5;
		transform.FindChild("Tutorial/2").GetComponent<Image>().enabled = false;
		transform.FindChild("Tutorial/4").GetComponent<Image>().enabled = true;

		GameObject.Find("Window/item_key_staff_room_2").GetComponent<Button>().interactable = false;
		GameObject.Find("ItemBtn/Use").GetComponent<Button>().interactable = true;
		GameObject.Find("ItemBtn/Match").GetComponent<Button>().interactable = false;

		GameData.Instance.SetEventFlg("FLG_TUTORIAL_ITEM");
	}

	void Tutorial6Controll ()
	{
		tutorialNo = 6;
		ItemListWindowObj.SetActive(false);
		ItemDetailObj.SetActive(false);
		MatchDetailObj.SetActive(false);
		ItemBtnObj.SetActive(false);
		TutorialMsg.SetActive(false);

		EndMsgObj.SetActive(true);
	}
	
	/// <summary>
	/// もってるアイテムアイコンを作成
	/// </summary>
	void CreateItemIcon ()
	{
		// 持ってるアイテムをセットする
		int count = 0;

		Dictionary<string, bool> dic = GameData.Instance.GetItemDic();
		foreach (string key  in dic.Keys) {
			if(dic[key]){
				// 持っているなら表示
				GameObject prefab = Instantiate (
					Resources.Load<GameObject> ("Prefab/UI/ItemTutorialIcon"), 
					Vector3.zero, 
					Quaternion.identity
				) as GameObject;
				prefab.transform.SetParent(ItemListWindowObj.transform, false);

				Sprite[] sprites = Resources.LoadAll<Sprite> ("Item/ItemAtlas");
				prefab.GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals ("icon_"+ key));

				prefab.GetComponent<RectTransform> ().SetLocalPosition (-98+((count%4)*66), 34-(66*(count/4)));
				prefab.name = key;
				count++;
			}
		}
	}

	/// <summary>
	/// アイテムアイコンを選択した時
	/// </summary>
	public void SelectItem (string item_name)
	{
		if(string.Empty != selectItemKey ){
			// すでにアイテムが選択されていた場合は色を元に戻す
			ItemListWindowObj.transform.FindChild(selectItemKey).GetComponent<Image>().color = new Color(1, 1, 1, 1);
		}

		if(item_name.Equals(selectItemKey)){
			// 選択済みのアイテムをタッチした時はアイテムを解除
			if(isUse){
				selectItemKey = "";
				GameData.Instance.SetItem = "";

				// 詳細を消す
				ItemDetailObj.SetActive(false);
				ItemBtnObj.SetActive(false);
			} else {
				selectItemKey = "";

				// 組み合わせ先は非表示にしておく
				MatchDetailObj.transform.FindChild("After/Image").GetComponent<Image>().enabled = false;
				MatchDetailObj.transform.FindChild("After/Text").GetComponent<Text>().enabled = false;

				// 文章も非表示にしておく
				MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = false;

				// 組み合わせるボタン選択不可
				ItemBtnObj.transform.FindChild("Match").GetComponent<Button>().interactable = false;
			}
		} else {
			selectItemKey = item_name;
			Debug.Log("item_name:" +item_name);

			if(isUse){
				// アイテム画像表示
				ItemDetailObj.SetActive(true);
				ItemBtnObj.SetActive(true);

				// 戻るボタンを使うに
				Sprite[] ui_sprites = Resources.LoadAll<Sprite> ("UI/UIAtlas");
				ItemBtnObj.transform.FindChild("Use").GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (ui_sprites, (sprite) => sprite.name.Equals ("item_button_use"));

				Sprite[] sprites = Resources.LoadAll<Sprite> ("Item/ItemAtlas");
				ItemDetailObj.transform.FindChild("Item").GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (item_name));

				// 説明
				ItemDetailObj.transform.FindChild("Content").GetComponent<Text>().text = 
					GameData.Instance.GetItemDetailContext(item_name);
				// 名前
				ItemDetailObj.transform.FindChild("Name").GetComponent<Text>().text = 
					GameData.Instance.GetItemDetailName(item_name);

				// 組み合わせるボタン選択不可
				ItemBtnObj.transform.FindChild("Match").GetComponent<Button>().interactable = true;

				if(1 == tutorialNo){
					Tutorial2Controll();
				}

			} else {
				if(3 == tutorialNo){
					Tutorial4Controll();
				}

				// 組み合わせるのとき
				// 文章は非表示にしておく
				MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = false;

				if(matchItemKey.Equals(selectItemKey)){
					// 同じアイテム選択してるの消す
					selectItemKey = "";

					// 組み合わせ先は非表示にしておく
					MatchDetailObj.transform.FindChild("After/Image").GetComponent<Image>().enabled = false;
					MatchDetailObj.transform.FindChild("After/Text").GetComponent<Text>().enabled = false;

					// 文章も非表示にしておく
					MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = false;

					// 組み合わせるボタン選択不可
					ItemBtnObj.transform.FindChild("Match").GetComponent<Button>().interactable = false;
					return;
				}
				// 組み合わせるボタン選択可能
				ItemBtnObj.transform.FindChild("Match").GetComponent<Button>().interactable = true;

				// 組み合わせ元の画像と名前を表示
				Sprite[] sprites = Resources.LoadAll<Sprite> ("item/ItemAtlas");

				MatchDetailObj.transform.FindChild("After/Image").GetComponent<Image>().sprite = 
					System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (selectItemKey));
				MatchDetailObj.transform.FindChild("After/Image").GetComponent<Image>().enabled = true;
				MatchDetailObj.transform.FindChild("After/Text").GetComponent<Text>().text = 
					GameData.Instance.GetItemDetailName(selectItemKey);
				MatchDetailObj.transform.FindChild("After/Text").GetComponent<Text>().enabled = true;
			}
		}

	}

	/// <summary>
	/// 使うボタンを押した時
	/// </summary>
	public void OnItemUse ()
	{
		SoundManager.Instance.PlaySE ("140", 1);

		// アイテムセット
		if(string.Empty == selectItemKey) return;

		if(isUse){
			Debug.Log("selectItemKey:" +selectItemKey);
			GameData.Instance.SetItem = selectItemKey;
			Tutorial6Controll();
		} else {
			// 組み合わせから戻る
			// 戻るボタンを使うに
			Sprite[] ui_sprites = Resources.LoadAll<Sprite> ("UI/UIAtlas");
			ItemBtnObj.transform.FindChild("Use").GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (ui_sprites, (sprite) => sprite.name.Equals ("item_button_use"));
			MatchDetailObj.SetActive(false);
			// 組み合わせるボタン選択可能
			ItemBtnObj.SetActive(false);

			if(string.Empty != selectItemKey){
				ItemListWindowObj.transform.FindChild(selectItemKey).GetComponent<Image>().color = new Color(1, 1, 1, 1);
			}
			selectItemKey = matchItemKey;
			matchItemKey = "";

			isUse = true;
			SelectItem(selectItemKey);
		}
	}

	/// <summary>
	/// 組み合わせるボタンを押す
	/// </summary>
	public void OnItemMatch ()
	{
		SoundManager.Instance.PlaySE ("140", 1);
		// アイテムセット
		if(string.Empty == selectItemKey) return;

		if(isUse){
			// ベースアイテムを選択
			matchItemKey = selectItemKey;
			selectItemKey = "";

			// 組み合わせるボタン選択不可
			ItemBtnObj.transform.FindChild("Match").GetComponent<Button>().interactable = false;

			// アイテム画像を消す
			// 詳細を消す
			ItemDetailObj.SetActive(false);

			// 組み合わせる画面を表示
			MatchDetailObj.SetActive(true);

			// 組み合わせ元の画像と名前を表示
			Sprite[] sprites = Resources.LoadAll<Sprite> ("item/ItemAtlas");
			MatchDetailObj.transform.FindChild("Before/Image").GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (sprites, (sprite) => sprite.name.Equals (matchItemKey));
			MatchDetailObj.transform.FindChild("Before/Text").GetComponent<Text>().text = 
				GameData.Instance.GetItemDetailName(matchItemKey);

			// 組み合わせ先は非表示にしておく
			MatchDetailObj.transform.FindChild("After/Image").GetComponent<Image>().enabled = false;
			MatchDetailObj.transform.FindChild("After/Text").GetComponent<Text>().enabled = false;

			// 文章も非表示にしておく
			MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = false;

			// 使うボタンを戻るに
			Sprite[] ui_sprites = Resources.LoadAll<Sprite> ("UI/UIAtlas");
			ItemBtnObj.transform.FindChild("Use").GetComponent<Image>().sprite = 
				System.Array.Find<Sprite> (ui_sprites, (sprite) => sprite.name.Equals ("item_back"));


			isUse = false;
		} else {
			// 組み合わせるアイテムを選択
			string canMatchItem = GameData.Instance.GetMatchItemKey(matchItemKey);

			if(null != canMatchItem && string.Empty != canMatchItem){
				if(canMatchItem.Equals(selectItemKey)){
					// 合成OK アイテム取得
					string matchedItem = GameData.Instance.GetMatchedItemName(matchItemKey);
					GameData.Instance.SetItemGet(matchedItem);
					// アイテムを表示する
					GameData.Instance.SetItem = matchedItem;

					// 使ったら消えるアイテムは消す
					if(GameData.Instance.isDeleteMatchedItem(selectItemKey)){
						GameData.Instance.DeleteItem(selectItemKey);
					}
					if(GameData.Instance.isDeleteMatchedItem(matchItemKey)){
						GameData.Instance.DeleteItem(matchItemKey);
					}

					// イベント発生するか調べる
					string matchEventName = GameData.Instance.GetMatchItemEvent(matchItemKey);
					if( null != matchEventName && string.Empty != matchEventName){
						GameObject.Find("Manager").GetComponent<EventManager>().SetEvent(matchEventName);
						GameObject.Find("Manager").GetComponent<EventManager>().StartEvent();
					}
					SoundManager.Instance.PlaySE ("113", 1);

					// 合成完成画像表示
					MatchDetailObj.SetActive(false);
					ItemBtnObj.SetActive(false);

					//アイテムアイコンを更新する
					ItemListWindowObj.BroadcastMessage ("DestroyIcon", SendMessageOptions.DontRequireReceiver);
					CreateItemIcon();

					isUse = true;
					ItemListWindowObj.transform.FindChild(matchedItem).GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1);

					Debug.Log("matchedItem:" + matchedItem);

					SelectItem(matchedItem);
					matchItemKey = "";
					GameData.Instance.SetItem = "";
				} else {
					// 組み合わせることができない場合
					MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = true;
				}
			} else {
				// 組み合わせることができない場合
				MatchDetailObj.transform.FindChild("Text").GetComponent<Text>().enabled = true;
			}
		}

		if(2 == tutorialNo){
			Tutorial3Controll();
		} else if(4 == tutorialNo){
			Tutorial5Controll();
		}
	}

	public void CloseItemPopup ()
	{
		// タッチ
		GameData.Instance.CanTouchFlg = true;
		if(null != GameObject.Find("360View/360")){
			GameObject.Find("360View/360").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		if(null != GameObject.Find("Limit360View/EventPrefab")){
			GameObject.Find("Limit360View/EventPrefab").BroadcastMessage ("SetTouchEvent", SendMessageOptions.DontRequireReceiver);
		}
		Destroy(gameObject);
		/*
		if(! GameObject.Find("Canvas").GetComponent<UIController>().SetUserItemIcon()){
			GameObject.Find("Canvas").GetComponent<UIController>().ClosePopup();
		} else {
			GameObject.Find("Canvas").GetComponent<UIController>().ClosePopup(false);
		}
		*/
	}
}
