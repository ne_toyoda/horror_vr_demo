﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SaveData
{
	public string scene = "SCENE_OP";
	public string set_item = "";

	public int free_magatama = 0;
	public int pay_magatama = 0;

	public bool isBGM = true;
	public bool isSE = true;
	public bool isVIBRATE = true;

	public int mail_no = 0;
	public string diagnosisPlace = "";

	public Dictionary<string, bool>event_flg_data = new Dictionary<string, bool>();
	public Dictionary<string, bool>has_item_data = new Dictionary<string, bool>();
	public Dictionary<string, bool>has_archive_data = new Dictionary<string, bool>();
	public Dictionary<string, bool>used_hint = new Dictionary<string, bool>();
	public Dictionary<string, bool>read_file = new Dictionary<string, bool>();
	public Dictionary<string, bool>is_achievement = new Dictionary<string, bool>();

	public DateTime ads_time;
	public bool isDisplayAds = true;

	public int clear_count = 0;

	public int xRayCount = 0;
	public int hospitalDoorCount = 0;
	public int deskBoxCount = 0;

	public bool isGazeTouch = true;
	public int gazeSpeed = 2;
}


public class GameData  : MonoBehaviour {
	static GameData instance = null;

	public SaveData savedata;

	bool canTouch = true;
	bool canPushUI = true;

	bool isAds = false;
	bool isAdPop = false;

	int achievementPoint = 0;



	Dictionary<string, EventNameData.Param>event_list_dic = new Dictionary<string, EventNameData.Param>();
	Dictionary<string, ItemData.Param>item_data_dic = new Dictionary<string, ItemData.Param>();
	Dictionary<string, ArchiveData.Param>archive_data_dic = new Dictionary<string, ArchiveData.Param>();
	Dictionary<string, PlaceData.Param>place_data_dic = new Dictionary<string, PlaceData.Param>();
	Dictionary<string, SceneData.Param>scene_dic = new Dictionary<string, SceneData.Param>();
	Dictionary<string, HintData.Param>hint_dic = new Dictionary<string, HintData.Param>();
	Dictionary<string, AchievementData.Param>achievement_dic = new Dictionary<string, AchievementData.Param>();

	Dictionary<string, string>android_achievement = new Dictionary<string, string>();

	SaveDataManager dataManager;

	public static GameData Instance {
		get { return GameData.instance; }
	}

	void Awake() {
		Application.targetFrameRate = 60;
		dataManager = GetComponent<SaveDataManager>();
		savedata = dataManager.LoadData();


		if ( instance == null ) {
			instance = this;
			SetInitData();
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	void Start ()
	{
		#if UNITY_IPHONE
		UnityEngine.iOS.Device.SetNoBackupFlag(Application.persistentDataPath);
		#endif
	}

	void Update ()
	{
		// プラットフォームがアンドロイドかチェック
		if (Application.platform == RuntimePlatform.Android)
		{
			// エスケープキー取得
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				// アプリケーション終了
				Application.Quit();
				return;
			}
		}
	}

	public void SetInitData ()
	{
		// データをセットする
		SceneData scene_data = Resources.Load<SceneData>("Xls/Data/SceneData");

		foreach (SceneData.Param p in scene_data.list) {
			if(! scene_dic.ContainsKey(p.scene_key)){
				scene_dic.Add(p.scene_key, p);
			}
		}

		FlgData data = Resources.Load<FlgData>("Xls/Data/FlgData");
		foreach (FlgData.Param p in data.list) {
			if(! savedata.event_flg_data.ContainsKey(p.flg_key)){
				savedata.event_flg_data.Add(p.flg_key, false);
			}
		}

		EventNameData event_list_data = Resources.Load<EventNameData>("Xls/Data/EventNameData");
		foreach (EventNameData.Param p in event_list_data.list) {
			if(! event_list_dic.ContainsKey(p.event_name)){
				event_list_dic.Add(p.event_name, p);
			}
		}

		ItemData item_list_data = Resources.Load<ItemData>("Xls/Data/ItemData");
		foreach (ItemData.Param p in item_list_data.list) {
			if(! item_data_dic.ContainsKey(p.item_key)){
				item_data_dic.Add(p.item_key, p);
			}
			if(! savedata.has_item_data.ContainsKey(p.item_key)){
				savedata.has_item_data.Add(p.item_key, false);
			}
		}

		ArchiveData archive_list_data = Resources.Load<ArchiveData>("Xls/Data/ArchiveData");
		foreach (ArchiveData.Param p in archive_list_data.list) {
			if(! archive_data_dic.ContainsKey(p.archive_key)){
				archive_data_dic.Add(p.archive_key, p);
			}
			if(! savedata.has_archive_data.ContainsKey(p.archive_key)){
				savedata.has_archive_data.Add(p.archive_key, false);
			}
			if(! savedata.read_file.ContainsKey(p.archive_key)){
				savedata.read_file.Add(p.archive_key, false);
			}
		}

		PlaceData place_list_data = Resources.Load<PlaceData>("Xls/Data/PlaceData");
		foreach (PlaceData.Param p in place_list_data.list) {
			if(! place_data_dic.ContainsKey(p.place_key)){
				place_data_dic.Add(p.place_key, p);
			}
		}

		HintData hint_data = Resources.Load<HintData>("Xls/Data/HintData");
		foreach (HintData.Param p in hint_data.list) {
			if(! hint_dic.ContainsKey(p.no)){
				hint_dic.Add(p.no, p);
			}
			if(! savedata.used_hint.ContainsKey(p.no)){
				savedata.used_hint.Add(p.no, false);
			}
		}

		AchievementData achievement_data = Resources.Load<AchievementData>("Xls/Data/AchievementData");
		foreach (AchievementData.Param p in achievement_data.list) {
			if(! achievement_dic.ContainsKey(p.no)){
				achievement_dic.Add(p.no, p);
			}
			if(! savedata.is_achievement.ContainsKey(p.no)){
				savedata.is_achievement.Add(p.no, false);
			}
		}
		/*
		android_achievement.Add("1", "CgkI08zdg8AYEAIQAQ");
		android_achievement.Add("2", "CgkI08zdg8AYEAIQAg");
		android_achievement.Add("3", "CgkI08zdg8AYEAIQAw");
		android_achievement.Add("4", "CgkI08zdg8AYEAIQBA");
		android_achievement.Add("5", "CgkI08zdg8AYEAIQBQ");
		android_achievement.Add("6", "CgkI08zdg8AYEAIQBg");
		android_achievement.Add("7", "CgkI08zdg8AYEAIQBw");
		android_achievement.Add("8", "CgkI08zdg8AYEAIQCA");
		android_achievement.Add("9", "CgkI08zdg8AYEAIQCQ");
		android_achievement.Add("10", "CgkI08zdg8AYEAIQCg");
		android_achievement.Add("11", "CgkI08zdg8AYEAIQCw");
		android_achievement.Add("12", "CgkI08zdg8AYEAIQDA");
		android_achievement.Add("13", "CgkI08zdg8AYEAIQDQ");
		android_achievement.Add("14", "CgkI08zdg8AYEAIQDg");
		android_achievement.Add("15", "CgkI08zdg8AYEAIQDw");
		android_achievement.Add("16", "CgkI08zdg8AYEAIQEA");
		android_achievement.Add("17", "CgkI08zdg8AYEAIQEQ");
		android_achievement.Add("18", "CgkI08zdg8AYEAIQEg");
		android_achievement.Add("19", "CgkI08zdg8AYEAIQEw");
		android_achievement.Add("20", "CgkI08zdg8AYEAIQFA");
		*/
		GetComponent<SaveDataManager>().SaveData();
	}

	/// <summary>
	/// 現在のシーン
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string SceneString
	{
		get {
			return savedata.scene;
		}
		set {
			savedata.scene = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// セットアイテム変更
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string SetItem
	{
		get {
			return savedata.set_item;
		}
		set {
			savedata.set_item = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// カメラの回転、タッチフラグ
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool CanTouchFlg
	{
		get {
			return this.canTouch;
		}
		set {
			this.canTouch = value;
		}
	}



	/// <summary>
	/// UIボタンを押せるかフラグ
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool CanPushUIFlg
	{
		get {
			return this.canPushUI;
		}
		set {
			this.canPushUI = value;
		}
	}

	/// <summary>
	/// シーン配列ゲット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public Dictionary<string, SceneData.Param> GetSceneDic()
	{
		return scene_dic;
	}

	/// <summary>
	/// 現在のマップの階数をゲット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetNowMapFloor()
	{
		string key = scene_dic[savedata.scene].prefab_pass;

		if(place_data_dic.ContainsKey(key)){
			return place_data_dic[key].place_floor;
		}

		Debug.LogError("そのシーンの場所がないよー");

		return "";
	}

	/// <summary>
	/// 現在のマップの場所キーゲット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetNowPlaceKey()
	{
		string key = scene_dic[savedata.scene].prefab_pass;

		if(place_data_dic.ContainsKey(key)){
			return key;
		}

		return key;
	}
		
	/// <summary>
	/// 場所のシーン配列を返す
	/// </summary>
	/// <remarks>
	/// </remarks>
	public Dictionary<int, string> GetPlaceSceneArray(string place_name)
	{
		Dictionary<int, string> data = new Dictionary<int, string>();

		foreach(string key in scene_dic.Keys){
			if(place_name.Equals(scene_dic[key].prefab_pass)){
				data.Add(scene_dic[key].scene_no, key);
			}
		}

		return data;
	}
	/// <summary>
	/// シーン特定のフラグがあるかどうかを返す
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool IsSceneDecisionFlg (string scene_name)
	{
		if(scene_dic.ContainsKey(scene_name)){
			string flg_name = scene_dic[scene_name].scene_decisioin_flg;
			if(null == flg_name || string.Empty == flg_name){
				return true;
			} else {
				Debug.Log("flg_name:"+flg_name + " " + GetEventFlg(flg_name));
				return GetEventFlg(flg_name);
			}
		} else {
			Debug.LogError("そのシーンないっす");
		}

		return true;
	}

	/// <summary>
	/// イベントフラグ
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void SetEventFlg(string flg_name)
	{
		if(savedata.event_flg_data.ContainsKey(flg_name)){
			savedata.event_flg_data[flg_name] = true;
			dataManager.SaveData();

			SendAnalytics(flg_name);
		} else {
			Debug.LogError("イベントフラグない:"+flg_name);
		}
	}

	public bool GetEventFlg(string flg_name)
	{
		if(savedata.event_flg_data.ContainsKey(flg_name)){
			return savedata.event_flg_data[flg_name];
		} else {
			Debug.LogError("イベントフラグない:"+flg_name);
			return false;
		}
	}

	/// <summary>
	/// テキストのみイベントのテキスト取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetTextEventContext(string key)
	{
		if (event_list_dic.ContainsKey(key)){
			return event_list_dic[key].context;
		} else {
			Debug.Log ( "あれーテキストキーないよー:" +  key);
			return "";
		}
	}

	/// <summary>
	/// イベント発生タッチ回数ゲット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public int GetEventTouchCount(string key)
	{
		if (event_list_dic.ContainsKey(key)){
			return null != event_list_dic[key].touch_count ? event_list_dic[key].touch_count : 0;
		} else {
			Debug.Log ( "あれーキーないよー:" +  key);
			return 0;
		}
	}

	/// <summary>
	/// イベント発生シーン取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string[] GetEventScene(string key)
	{
		if (event_list_dic.ContainsKey(key)){
			return event_list_dic[key].scene_array;
		} else {
			Debug.LogError ( "あれーキーないよー:" +  key + ": prefab出しっぱ気をつけてー");
			return null;
		}
	}

	/// <summary>
	/// 全フラグ初期化
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void AllInit ()
	{
		savedata.scene = "SCENE_OP";
		InitFlgData();
		InitItemData();
		InitAchievementData();
		InitReadFileData();
		SetItem = "";
		MailNo = 0;
		DiagnosisPlace = "";
		XRayCount = 0;
		HospitalDoorCount = 0;
		DeskBoxCount = 0;

		dataManager.SaveData();
	}

	public void InitFlgData ()
	{
		List<string> keyList = new List<string>(savedata.event_flg_data.Keys);
		foreach(string key in keyList){
			savedata.event_flg_data [key] = false;
		}
	}

	public void InitItemData ()
	{
		List<string> keyList = new List<string>(savedata.has_item_data.Keys);
		foreach(string key in keyList){
			savedata.has_item_data [key] = false;
		}

		List<string> keyList_2 = new List<string>(savedata.has_archive_data.Keys);
		foreach(string key in keyList_2){
			savedata.has_archive_data [key] = false;
		}
	}

	public void InitAchievementData ()
	{
		List<string> keyList = new List<string>(savedata.is_achievement.Keys);
		foreach(string key in keyList){
			savedata.is_achievement [key] = false;
		}
	}
		
	public void InitReadFileData ()
	{
		List<string> keyList = new List<string>(savedata.read_file.Keys);
		foreach(string key in keyList){
			savedata.read_file [key] = false;
		}
	}

	public void InitFreeMagatamaData ()
	{
		savedata.free_magatama = 0;
		dataManager.SaveData();
	}

	/// <summary>
	/// イベント発生フラグ条件（なし）取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetStartEventNotFlg(string key)
	{
		if (event_list_dic.ContainsKey(key)){
			return null != event_list_dic[key].limit_flg_nothing ? event_list_dic[key].limit_flg_nothing : "";
		} else {
			Debug.Log ( "あれーキーないよー:" +  key);
			return "";
		}
	}

	/// <summary>
	/// イベント発生フラグ条件（あり）取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetStartEventFlg(string key)
	{
		if (event_list_dic.ContainsKey(key)){
			return null != event_list_dic[key].limit_flg ? event_list_dic[key].limit_flg : "";
		} else {
			Debug.Log ( "あれーキーないよー:" +  key);
			return "";
		}
	}

	/// <summary>
	/// アイテムセット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void SetItemGet(string item_name)
	{
		if(savedata.has_item_data.ContainsKey(item_name)){
			savedata.has_item_data[item_name] = true;
			dataManager.SaveData();
		} else {
			Debug.LogError("アイテムフラグない:"+item_name);
		}
	}
		

	public void DeleteItem(string item_name)
	{
		if(savedata.has_item_data.ContainsKey(item_name)){
			savedata.has_item_data[item_name] = false;
			dataManager.SaveData();
		} else {
			Debug.LogError("アイテムフラグない:"+item_name);
		}
	}


	public bool GetItem(string item_name)
	{
		if(savedata.has_item_data.ContainsKey(item_name)){
			return savedata.has_item_data[item_name];
		}

		return false;
	}

	public Dictionary<string, bool> GetItemDic()
	{
		return savedata.has_item_data;
	}

	/// <summary>
	/// アイテム説明
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetItemDetailContext(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].item_content;
		} else {
			Debug.LogError("アイテムない:"+item_name);
			return "";
		}
	}

	/// <summary>
	/// アイテム名前
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetItemDetailName(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].item_name;
		} else {
			Debug.LogError("アイテムない:"+item_name);
			return "";
		}
	}

	/// <summary>
	/// アイテムイベント
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string[] GetItemUseEvent(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].use_event_array;
		} else {
			Debug.Log("アイテムない:"+item_name);
			return null;
		}
	}

	/// <summary>
	/// 組み合わせることのできるアイテムを取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetMatchItemKey(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].match_item;
		} else {
			Debug.Log("アイテムない:"+item_name);
			return "";
		}
	}

	/// <summary>
	/// 組み合わせたアイテム名を取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetMatchedItemName(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].matched_item_name;
		} else {
			Debug.Log("アイテムない:"+item_name);
			return "";
		}
	}

	/// <summary>
	/// 組み合わせた後消えるか
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool isDeleteMatchedItem(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].isDeleteInMatch;
		} else {
			Debug.Log("アイテムない:"+item_name);
			return false;
		}
	}

	/// <summary>
	/// アイテム合成したときに発生するイベント
	/// </summary>
	/// <remarks>
	/// </remarks>
	public string GetMatchItemEvent(string item_name)
	{
		if(item_data_dic.ContainsKey(item_name)){
			return item_data_dic[item_name].match_event;
		} else {
			Debug.LogError("アイテムフラグない:"+item_name);
		}
		return "";
	}


	/// <summary>
	/// アーカイブセット
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void SetArchiveGet(string archive_name)
	{
		if(archive_data_dic.ContainsKey(archive_name)){
			savedata.has_archive_data[archive_name] = true;
		} else {
			Debug.LogError("アーカイブフラグない:"+archive_name);
		}
	}

	public Dictionary<string, bool> GetHasArchiveDic()
	{
		return savedata.has_archive_data;
	}

	public bool GetArchive(string archive_name)
	{
		if(savedata.has_archive_data.ContainsKey(archive_name)){
			return savedata.has_archive_data[archive_name];
		}

		return false;
	}

	public Dictionary<string, ArchiveData.Param> GetArchiveDic()
	{
		return archive_data_dic;
	}

	/// <summary>
	/// ヒントデータ取得
	/// </summary>
	/// <remarks>
	/// </remarks>
	public Dictionary<string, HintData.Param> GetHintDic()
	{
		return hint_dic;
	}

	public String GetHintTitle(string no)
	{
		if(hint_dic.ContainsKey(no)){
			return hint_dic[no].title;
		} else {
			Debug.LogError("ヒントナンバーない:"+no);
			return "";
		}
	}

	public bool IsHintRelease(string no)
	{
		if(savedata.used_hint.ContainsKey(no)){
			return savedata.used_hint[no];
		} else {
			Debug.LogError("ヒントナンバーない:"+no);
			return false;
		}
	}

	public void SetHintRelease(string no)
	{
		if(savedata.used_hint.ContainsKey(no)){
			savedata.used_hint[no] = true;
			dataManager.SaveData();
		} else {
			Debug.LogError("ヒントナンバーない:"+no);
		}
	}

	public Dictionary<string, bool> SetHintReleaseDic()
	{
		return savedata.used_hint;
	}

	public string GetHintContent(string no)
	{
		if(hint_dic.ContainsKey(no)){
			return hint_dic[no].content;
		} else {
			Debug.LogError("ヒントナンバーない:"+no);
			return "";
		}
	}

	public string GetHintMagatama(string no)
	{
		if(hint_dic.ContainsKey(no)){
			return hint_dic[no].magatama_count;
		} else {
			Debug.LogError("ヒントナンバーない:"+no);
			return "";
		}
	}

	/// <summary>
	/// アーカイブ既読情報
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool ReadFile(string no)
	{
		if(savedata.read_file.ContainsKey(no)){
			return savedata.read_file[no];
		} else {
			Debug.LogError("アーカイブない:"+no);
			return false;
		}
	}

	public void SetReadFile(string no)
	{
		if(savedata.read_file.ContainsKey(no)){
			savedata.read_file[no] = true;
			dataManager.SaveData();
		} else {
			Debug.LogError("アーカイブない:"+no);
		}
	}

	/// <summary>
	/// 勾玉
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void AddFreeMagatama (int add_count)
	{
		savedata.free_magatama += add_count;
		dataManager.SaveData();
	}

	public int GetFreeMagatama ()
	{
		return savedata.free_magatama;
	}

	public int GetAllMagatama ()
	{
		return savedata.free_magatama+savedata.pay_magatama;
	}

	public void ReduceMagatama(int reduce_count)
	{
		if(savedata.free_magatama+savedata.pay_magatama < reduce_count){
			Debug.LogError("マガタマたりない:");
			return;
		}

		if(reduce_count <= savedata.free_magatama){
			//freeだけ減らす
			savedata.free_magatama -= reduce_count;
		} else {
			reduce_count -= savedata.free_magatama;
			savedata.free_magatama = 0;
			savedata.pay_magatama -= reduce_count;

			Debug.Log("pay_magatama:" + savedata.pay_magatama);
			// サーバーのマガタマの数減らす
			/*
			Client.GetInstance().SetMagatamaCount(
				(savedata.pay_magatama).ToString(),
				SetMagatamaCountSuccess);
				*/
		}
		dataManager.SaveData();
	}

	void SetMagatamaCountSuccess (IDictionary response)
	{
	
	}

	public void AddPayMagatama (int add_count)
	{
		savedata.pay_magatama += add_count;
		dataManager.SaveData();
	}

	public int GetPayMagatama ()
	{
		return savedata.pay_magatama;
	}

	public void SetPayMagatama (int magatama)
	{
		savedata.pay_magatama = magatama;
		dataManager.SaveData();
	}


	/// <summary>
	/// コンフィグ系
	/// </summary>
	/// <remarks>
	/// </remarks>
	public bool IsBGM
	{
		get {
			return savedata.isBGM;
		}
		set {
			savedata.isBGM = value;
			dataManager.SaveData();
		}
	}

	public bool IsSE
	{
		get {
			return savedata.isSE;
		}
		set {
			savedata.isSE = value;
			dataManager.SaveData();
		}
	}

	public bool IsVIBRATE
	{
		get {
			return savedata.isVIBRATE;
		}
		set {
			savedata.isVIBRATE = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// 達成率
	/// </summary>
	/// <remarks>
	/// </remarks>
	public void SetAchievement(string no)
	{
		if(savedata.is_achievement.ContainsKey(no)){
			if(! savedata.is_achievement[no]){
				/*
				analytics.LogEvent(new EventHitBuilder()
					.SetEventCategory("Achievement")
					.SetEventAction("Clear")
					.SetEventLabel("No:" + no.ToString())
					.SetEventValue(1));
					*/
			}

			savedata.is_achievement[no] = true;
			dataManager.SaveData();


		} else {
			Debug.LogError("達成率フラグない:"+no);
		}
	}

	public Dictionary<string, AchievementData.Param> GetAchievementDic()
	{
		return achievement_dic;
	}

	public bool IsAchievement(string no)
	{
		if(savedata.is_achievement.ContainsKey(no)){
			return savedata.is_achievement[no];
		} else {
			Debug.LogError("達成率フラグない:"+no);
			return false;
		}

	}

	public string GetAndroidAchievementID (string id)
	{
		if(android_achievement.ContainsKey(id)){
			return android_achievement[id];
		} else {
			Debug.LogError("Android実績IDがない:"+id);
			return "";
		}
	}

	/// <summary>
	/// メールナンバー
	/// </summary>
	/// <remarks>
	/// </remarks>
	public int MailNo
	{
		get {
			return this.savedata.mail_no;
		}
		set {
			if(value<5){
				this.savedata.mail_no = value;
			} else {
				this.savedata.mail_no = 0;
			}
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// 広告
	/// </summary>
	/// <remarks>
	/// </remarks>

	// 広告からもどってきたあとか 
	public bool IsAds
	{
		get {
			return this.isAds;
		}
		set {
			this.isAds = value;
		}
	}

	// 広告が回復しているか
	public bool IsDisplayAds
	{
		get {
			return this.savedata.isDisplayAds;
		}
		set {
			this.savedata.isDisplayAds = value;
		}
	}

	// 広告のポップアップをだすか
	public bool IsDisplayAdsPop
	{
		get {
			return this.isAdPop;
		}
		set {
			this.isAdPop = value;
		}
	}

	public DateTime ADsTime
	{
		get {
			return this.savedata.ads_time;
		}
		set {
			this.savedata.ads_time = value;
			dataManager.SaveData();
		}
	}

	public int ClearCount
	{
		get {
			return this.savedata.clear_count;
		}
		set {
			this.savedata.clear_count = value;
			dataManager.SaveData();
		}
	}

	public string DiagnosisPlace
	{
		get {
			return this.savedata.diagnosisPlace;
		}
		set {
			this.savedata.diagnosisPlace = value;
			dataManager.SaveData();
		}
	}

	void SendAnalytics (string flg_name)
	{
		if(flg_name.Equals("FLG_TUTORIAL_1")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(0);
		} else if(flg_name.Equals("FLG_GET_STAFFROOM_KEY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(1);
		} else if(flg_name.Equals("FLG_STAFF_ROOM_DOOR_OPEN")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(2);
		} else if(flg_name.Equals("FLG_STAFF_ROOM_MAP")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(3);
		} else if(flg_name.Equals("FLG_EXAMINATION_DOOR_OPEN")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(4);
		} else if(flg_name.Equals("FLG_EXAMINATION_GET_BLOOD_PACK")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(5);
		} else if(flg_name.Equals("FLG_HOSPITAL_ROOM_DOOR_OPEN")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(6);
		} else if(flg_name.Equals("FLG_HOSPITAL_ROOM_VASE_DESTORY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(7);
		} else if(flg_name.Equals("FLG_STAFF_ROOM_NURSE_CALL_MSG")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(8);
		} else if(flg_name.Equals("FLG_HOSPITAL_ROOM_GET_BATTERY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(9);
		} else if(flg_name.Equals("FLG_MORTUARY_LIGHT_ON")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(10);
		} else if(flg_name.Equals("FLG_LAMP_KEY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(11);
		} else if(flg_name.Equals("FLG_EXAMINATION_ARM_SUTURE")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(12);
		} else if(flg_name.Equals("FLG_GET_CAR_KEY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(13);
		} else if(flg_name.Equals("FLG_EXAMINATION_CLEAR_DESK_BOX_KEY")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(14);
		} else if(flg_name.Equals("FLG_MORTUARY_CLEAR_PUZZLE")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(15);
		} else if(flg_name.Equals("FLG_MORTUARY_HELP_FRIEND")){
			GameObject.Find("GAv3").GetComponent<AnalyticsManager>().SendEventFlg(16);
		}
	}

	/// <summary>
	/// 部屋ごとのシーンナンバー
	/// </summary>
	public string GetSceneNoPerRoom()
	{
		return scene_dic[savedata.scene].texture_name;
	}

	/// <summary>
	/// 視点固定の切り替え
	/// </summary>
	public bool IsGazeTouch
	{
		get {
			return this.savedata.isGazeTouch;
		}
		set {
			this.savedata.isGazeTouch = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// 視点固定のスピード
	/// </summary>
	public int GazeSpeed
	{
		get {
			return this.savedata.gazeSpeed;
		}
		set {
			this.savedata.gazeSpeed = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// X線操作回数
	/// </summary>
	public int XRayCount
	{
		get {
			return this.savedata.xRayCount;
		}
		set {
			this.savedata.xRayCount = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// 病室扉を訪れた回数
	/// </summary>
	public int HospitalDoorCount
	{
		get {
			return this.savedata.hospitalDoorCount;
		}
		set {
			this.savedata.hospitalDoorCount = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// ダイヤルロックに挑んだ回数
	/// </summary>
	public int DeskBoxCount
	{
		get {
			return this.savedata.deskBoxCount;
		}
		set {
			this.savedata.deskBoxCount = value;
			dataManager.SaveData();
		}
	}

	/// <summary>
	/// 達成率
	/// </summary>
	public int AchievementPoint
	{
		get {
			return this.achievementPoint;
		}
		set {
			this.achievementPoint = value;
		}
	}





}
