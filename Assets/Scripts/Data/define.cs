﻿using UnityEngine;
using System.Collections;

public class define : MonoBehaviour {

	// エクセル指定イベント
	public static string EventSetLimit360 = "SET_LIMIT_360";
	public static string EventSetLimit360Part = "SET_LIMIT_360_PART";
	public static string EventPlaySE = "PLAY_SE";
	public static string EventCreatePrefab = "CREATE_PREFAB";
	public static string EventDestroyPrefab = "DESTROY_PREFAB";
	public static string EventJumpEventNoWhenHasEventFlg = "JUMP_EVENT_NO_WHEN_HAS_EVENT_FLG";
	public static string EventActiveEventFlg = "ACTIVE_EVENT_FLG";
	public static string EventEndEvent = "END_EVENT";
	public static string EventFadeOut = "FADE_OUT";
	public static string EventFadeIn = "FADE_IN";
	public static string EventJumpEvent = "JUMP_EVENT";
	public static string EventJumpEventWhenUseItem = "JUMP_EVENT_WHEN_USE_ITEM";
	public static string EventDisplayMessage = "DISPLAY_MESSAGE";
	public static string EventGetItem = "GET_ITEM";
	public static string EventPlayBGM = "PLAY_BGM";
	public static string EventLoadScene = "LOAD_SCENE";
	public static string EventChange360Immediate = "CHANGE_360_IMMEDIATE";
	public static string EventChangeScene = "CHANGE_SCENE";
	public static string EventDisabledUIButton = "DISABLED_UI_BUTTON";
	public static string EventEnabledUIButton = "ENABLED_UI_BUTTON";
	public static string EventDelay = "DELAY";
	public static string EventDeleteItem = "DELETE_ITEM";
	public static string EventCreateSpatitalSE = "CREATE_SPATIAL_SE";
	public static string EventStopBGM = "STOP_BGM";
	public static string EventVibrate = "VIBRATE";
	public static string EventBackBtnEvent = "BACKBTN_EVENT";
	public static string EventChange360ViewFade = "CHANGE_360VIEW_FADE";
	public static string EventDisplayMapPop = "DISPLAY_MAP_POP";
	public static string EventDisabledReticle = "DISABLED_RETICLE";


	public static string EVE_CONTENT_FULL_IMAGE = "FULL_IMAGE";

	public static string EVE_DISPLAY_ITEM = "DISPLAY_ITEM";


	public static string EVE_END_EVENT = "END_EVENT";
	public static string EVE_VIEW_CHANGE = "VIEW_CHANGE";
	public static string EVE_SET_FLG = "SET_FLG";
	public static string EVE_CAMERA_ROTATE_Y = "CAMERA_ROTATE_Y";
	public static string EVE_BLACK_OUT = "BLACK_OUT";
	public static string EVE_BLACK_IN = "BLACK_IN";
	public static string EVE_CAMERA_ZOOM = "CAMERA_ZOOM";
	public static string EVE_CAMERA_ROTATE_ANIM_X = "CAMERA_ROTATE_ANIM_X";
	public static string EVE_CAMERA_ROTATE_ANIM_Y = "CAMERA_ROTATE_ANIM_Y";
	public static string EVE_VIBRATE = "VIBRATE";
	public static string EVE_CHECK_NOT_FLG = "CHECK_NOT_FLG";
	public static string EVE_CHANGE_SCENE = "CHANGE_SCENE";
	public static string EVE_GET_ARCHIVE = "GET_ARCHIVE";
	public static string EVE_EVENT_END_AND_BACKBTN = "EVENT_END_AND_BACKBTN";
	public static string EVE_CHECK_USE_ITEM = "CHECK_USE_ITEM";
	public static string EVE_CHECK_FULL_IMAGE = "CHECK_FULL_IMAGE";
	public static string EVE_CHECK_FLG = "CHECK_FLG";

	public static string EVE_UI_BUTTON_FALSE = "UI_BUTTON_FALSE";
	public static string EVE_UI_BUTTON_TRUE = "UI_BUTTON_TRUE";

	public static string EVE_NOT_FULL_IMAGE = "NOT_FULL_IMAGE";

	public static string EVE_SET_SPRITE = "SET_SPRITE";
	public static string EVE_RANDUM_EVENT = "RANDUM_EVENT";
	public static string EVE_CAMERA_SHAKE = "CAMERA_SHAKE";
	public static string EVE_UPDATE_UI = "UPDATE_UI";
	public static string EVE_IMPOSSIBLE_MAP = "IMPOSSIBLE_MAP";
	public static string EVE_PLAY_SE_REPEAT = "PLAY_SE_REPEAT";
	public static string EVE_STOP_SE = "STOP_SE";
	public static string EVE_RANDUM_OCCUR = "RANDUM_OCCUR";
	public static string EVE_PLAY_SE_ENVIROMENT = "PLAY_SE_ENVIROMENT";
	public static string EVE_STOP_SE_ENVIROMENT = "STOP_SE_ENVIROMENT";
	public static string EVE_GET_ACHIEVEMENT = "GET_ACHIEVEMENT";
	public static string EVE_REDUCE_MAGATAMA = "REDUCE_MAGATAMA";
	public static string EVE_SUBMIT_ACHIEVEMENT = "SUBMIT_ACHIEVEMENT";
	public static string EVE_SPECIAL_EVENT = "SPECIAL_EVENT";

	// シーンの名前
	public static string SceneNameGame = "Game";
	public static string SceneNameOPED = "OP_ED";

	// シーンオプション
	public static string SceneOptGame = "game";











}