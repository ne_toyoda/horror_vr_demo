﻿using UnityEngine;
using System.Collections;

public class GlobalObject : MonoBehaviour
{
    private static GlobalObject sharedInstance = null;
    public string mStringParam = null;
    private object mParam = null;
    private object[] mParams = null;

    public string StringParam {
        get {
            return mStringParam;
        }
		set {
			mStringParam = value;
        }
    }

    public object Param {
        get {
            return mParam;
        }
    }

    public object[] Params {
        get {
            return mParams;
        }
    }

    public static GlobalObject getInstance ()
    {
        return sharedInstance;
    }

    public static void LoadLevelWithString (string levelName, string stringParam)
    {
		getInstance().mStringParam = stringParam;
        Application.LoadLevel(levelName);
    }

    public static void LoadLevelWithObject (string levelName, object theParam)
    {
        getInstance().mParam = theParam;
        Application.LoadLevel(levelName);
    }

    public static void LoadLevelWithParams (string levelName, params object[] theParams)
    {
        getInstance().mParams = theParams;
        Application.LoadLevel(levelName);
    }

    public void Awake ()
    {
        if (sharedInstance == null) {
            Debug.Log("Awake GlobalObject");

            sharedInstance = this;
            DontDestroyOnLoad(gameObject);
        }

        Debug.Log ("StringParam = " + getInstance().StringParam);
    }
}