﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using LitJson;

public class SaveDataManager : MonoBehaviour {

	private static readonly string EncryptKey = "c6eahbq9sjuawhvdr9kvhpsm5qv393ga";
	private static readonly int EncryptPasswordCount = 16;
	private static readonly string PasswordChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static readonly int PasswordCharsLength = PasswordChars.Length;
	private static readonly string SavePath = Application.persistentDataPath + "/save.bytes";

	void Start()
	{
		//SavePath = Application.persistentDataPath + "/save.bytes";
	}

	/// <summary>
	/// AES暗号化(Base64形式)
	/// </summary>
	public static void EncryptAesBase64(string json, out string iv, out string base64)
	{
		byte[] src = Encoding.UTF8.GetBytes(json);
		byte[] dst;
		EncryptAes(src, out iv, out dst);
		base64 = Convert.ToBase64String(dst);
	}

	/// <summary>
	/// AES複合化(Base64形式)
	/// </summary>
	public static void DecryptAesBase64(string base64, string iv, out string json)
	{
		byte[] src = Convert.FromBase64String(base64);
		byte[] dst;
		DecryptAes(src, iv, out dst);
		json = Encoding.UTF8.GetString(dst).Trim('\0');
	}

	/// <summary>
	/// AES暗号化
	/// </summary>
	public static void EncryptAes(byte[] src, out string iv, out byte[] dst)
	{
		iv = CreatePassword(EncryptPasswordCount);
		dst = null;
		using (RijndaelManaged rijndael = new RijndaelManaged())
		{
			rijndael.Padding = PaddingMode.PKCS7;
			rijndael.Mode = CipherMode.CBC;
			rijndael.KeySize = 256;
			rijndael.BlockSize = 128;

			byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
			byte[] vec = Encoding.UTF8.GetBytes(iv);

			using (ICryptoTransform encryptor = rijndael.CreateEncryptor(key, vec))
			using (MemoryStream ms = new MemoryStream())
			using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
			{
				cs.Write(src, 0, src.Length);
				cs.FlushFinalBlock();
				dst = ms.ToArray();
			}
		}
	}

	public void SaveData ()
	{
		///// 保存 /////////////////////////////////////
		{
			// セーブデータ作成
			SaveData obj = GetComponent<GameData>().savedata;
			//Debug.Log("[Encrypt]Id:" + obj.Get);
			//Debug.Log("[Encrypt]Name:" + obj.Name);

			// 暗号化
			string json = JsonMapper.ToJson(obj);
			string iv;
			string base64;
			EncryptAesBase64(json, out iv, out base64);
			Debug.Log("[Encrypt]json:" + json);
			Debug.Log("[Encrypt]base64:" + base64);

			// 保存
			byte[] ivBytes = Encoding.UTF8.GetBytes(iv);
			byte[] base64Bytes = Encoding.UTF8.GetBytes(base64);
			using (FileStream fs = new FileStream(SavePath, FileMode.Create, FileAccess.Write))
			using (BinaryWriter bw = new BinaryWriter(fs))
			{
				bw.Write(ivBytes.Length);
				bw.Write(ivBytes);

				bw.Write(base64Bytes.Length);
				bw.Write(base64Bytes);
			}
		}
	}

	public SaveData LoadData ()
	{
		///// 読み込み /////////////////////////////////////
		{
			// 読み込み
			byte[] ivBytes = null;
			byte[] base64Bytes = null;
			if ( ! System.IO.File.Exists(SavePath)){
				Debug.Log("初回でっす");
				SaveData data = new SaveData();
				return data;
			}

			Debug.Log("SavePath:" + SavePath);

			using (FileStream fs = new FileStream(SavePath, FileMode.Open, FileAccess.Read))
			using (BinaryReader br = new BinaryReader(fs))
			{
				int length = br.ReadInt32();
				ivBytes = br.ReadBytes(length);

				length = br.ReadInt32();
				base64Bytes = br.ReadBytes(length);
			}

			// 複合化
			string json;
			string iv = Encoding.UTF8.GetString(ivBytes);
			string base64 = Encoding.UTF8.GetString(base64Bytes);
			DecryptAesBase64(base64, iv, out json);
			Debug.Log("[Decrypt]json:" + json);

			// セーブデータ復元
			return JsonMapper.ToObject<SaveData>(json);
			//Debug.Log("[Decrypt]Id:" + obj.Id);
			//Debug.Log("[Decrypt]Name:" + obj.Name);
		}
	}

	/// <summary>
	/// AES複合化
	/// </summary>
	public static void DecryptAes(byte[] src, string iv, out byte[] dst)
	{
		dst = new byte[src.Length];
		using (RijndaelManaged rijndael = new RijndaelManaged())
		{
			rijndael.Padding = PaddingMode.PKCS7;
			rijndael.Mode = CipherMode.CBC;
			rijndael.KeySize = 256;
			rijndael.BlockSize = 128;

			byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
			byte[] vec = Encoding.UTF8.GetBytes(iv);

			using (ICryptoTransform decryptor = rijndael.CreateDecryptor(key, vec))
			using (MemoryStream ms = new MemoryStream(src))
			using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
			{
				cs.Read(dst, 0, dst.Length);
			}
		}
	}

	/// <summary>
	/// パスワード生成
	/// </summary>
	/// <param name="count">文字列数</param>
	/// <returns>パスワード</returns>
	public static string CreatePassword(int count)
	{
		StringBuilder sb = new StringBuilder(count);
		for (int i = count - 1; i >= 0; i--)
		{
			char c = PasswordChars[UnityEngine.Random.Range(0, PasswordCharsLength)];
			sb.Append(c);
		}
		return sb.ToString();
	}
}
