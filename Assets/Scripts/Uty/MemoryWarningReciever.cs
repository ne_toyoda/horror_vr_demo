﻿using UnityEngine;
using System.Collections;

public class MemoryWarningReciever : MonoBehaviour {

	public void DidReceiveMemoryWarning (string message)
	{
		Debug.Log ("メモリやばいってよ:" + message);
		Debug.Log ("GCするよ");
		System.GC.Collect ();
		Debug.Log ("使ってなさそうなアセットを開放するよ");
		Resources.UnloadUnusedAssets ();
	}
}
